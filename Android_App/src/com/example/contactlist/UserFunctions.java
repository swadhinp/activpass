
package com.example.contactlist;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.example.contactlist.QuizActivity.Question;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

public class UserFunctions extends AsyncTask<Object,Void,JSONObject> {
	
	private JSONParser jsonParser;
	
	private static String questionURL = "http://10.5.18.202/activPass_core/code_base/server.php";
	
	private static String login_tag = "login";
	private static String register_tag = "register";
	private static String question_tag = "question";
	private static String answer_tag = "answer";
	private Context context;
	// constructor
	public UserFunctions(Context c){
		context = c;
		jsonParser = new JSONParser();
	}
	
	protected JSONObject doInBackground(Object... params) {
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
		String username = sharedPrefs.getString("username", "null");
		
		ConnectivityManager conMgr =  (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) 
		{
			if(username.equalsIgnoreCase("null"))
			{
				return login((String)params[0]);
			}
			else
			{
				if(params.length==0)
					return getQuestions(username);
				else
					return submitAnswers((List<Question>)params[0],username);
		    }
		}
        else
        {
        	return null;
        }
		}
	
	/**
	 * function make Login Request
	 * @param email
	 * @param password
	 * */
	
	public JSONObject login(String username){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "login"));
		params.add(new BasicNameValuePair("username", username));
		JSONObject json = jsonParser.getJSONFromUrl(questionURL, params);
		return json;
	}
	
	public static void trimCache(Context context) {
	    try {
	       File dir = context.getCacheDir();
	       if (dir != null && dir.isDirectory()) {
	          deleteDir(dir);
	       }
	    } catch (Exception e) {
	       // TODO: handle exception
	    }
	 }
	
	public static boolean deleteDir(File dir) {
	    if (dir != null && dir.isDirectory()) {
	       String[] children = dir.list();
	       for (int i = 0; i < children.length; i++) {
	          boolean success = deleteDir(new File(dir, children[i]));
	          if (!success) {
	             return false;
	          }
	       }
	    }

	    // The directory is now empty so delete it
	    return dir.delete();
	 }
	
	public JSONObject submitAnswers(List<Question> questions,String username){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		String question = "", answer = "";
		for(int i=0;i<questions.size();i++){
			question += questions.get(i).question + ";";
			for(int j=0;j<questions.get(i).response.length;j++)
				answer += questions.get(i).response[j] + ",";
			answer += ";";
			}
		
		params.add(new BasicNameValuePair("tag", answer_tag));
		params.add(new BasicNameValuePair("username", username));
		params.add(new BasicNameValuePair("questions",question));
		params.add(new BasicNameValuePair("answers",answer));
		JSONObject json = jsonParser.getJSONFromUrl(questionURL, params);
		return json;
	}
	
	public JSONObject getQuestions(String username){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", question_tag));
		params.add(new BasicNameValuePair("username", username));
		
		JSONObject json = jsonParser.getJSONFromUrl(questionURL, params);
		return json;
	}

}
