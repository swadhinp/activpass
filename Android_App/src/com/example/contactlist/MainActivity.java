package com.example.contactlist;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;


import android.net.*;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("SimpleDateFormat")
public class MainActivity extends Activity {

	//protected static final Uri SMS_INBOX_CONTENT_URI = null;



	private void showQuestions(){		
		Intent dashboard = new Intent(getApplicationContext(), QuizActivity.class);
		
		// Close all views before launching Dashboard
		dashboard.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(dashboard);
	}
	
	private void collectData()
	{
		Intent i= new Intent(this, LogService.class);
		this.startService(i); 
	}
	
	Timer myTimer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		System.out.println("Hello1" );
//		final Button Retrieve = (Button) findViewById(R.id.submit);
//        Retrieve.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//            	retrieve();
//            }  
//        });
        
        final Button Quiz = (Button) findViewById(R.id.button2);
        Quiz.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	showQuestions();
            }  
        });
        
        final Button Collect = (Button) findViewById(R.id.button3);
        Collect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	collectData();
            }  
        });
        
        final Button Logout = (Button) findViewById(R.id.logout);
        Logout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(v.getContext());
			    SharedPreferences.Editor spe = prefs.edit();
			    spe.putString("username", "null");
			    spe.commit(); 
			    
			    Intent login = new Intent(getApplicationContext(), LoginActivity.class);
	        	login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        	startActivity(login);
	        	// Closing dashboard screen
	        	finish();  
            }  
        });
            
        try
		{
			Log.d("ContactService","Trying to send files");
			//Intent i= new Intent(this, LogService.class);
			//this.startService(i); 
			myTimer = new Timer();
			LoggingActivity log = new LoggingActivity(this,""+1);
			myTimer.schedule(log, 1000, 30*60*1000);			
		}
		catch(Exception ex)
		{
			Log.d("ContactService","Unable to send files");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public File fileLocation(String timestamp) {
		
		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
		    // We can read and write the media
		    mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
		    // We can only read the media
		    mExternalStorageAvailable = true;
		    mExternalStorageWriteable = false;
		} else {
		    mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
		//Log.d(LOG_TAG,Environment.getExternalStorageState()+Boolean.toString(mExternalStorageAvailable) + Boolean.toString(mExternalStorageWriteable));
		if (mExternalStorageAvailable && mExternalStorageWriteable) {
			
			//int fNo = app_preferences.getInt("fileNo",1);
			return new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/Contacts/"+timestamp);
			
		}
		else {
			//No external Storage;
			return null;
		}
	}

}
