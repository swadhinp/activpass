package com.example.contactlist;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.example.contactlist.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


public class QuizActivity extends Activity{

	private static String KEY_SUCCESS = "success";
	private static String KEY_ERROR = "error";
	private static String KEY_ERROR_MSG = "error_msg";
	int questionType = -1;
	int questionNum = -1;
		class Question{
		public	String question;
		public int type;  // 1:MCQ, 2:MCQ_Multiple, 3:Binary, 4:Text
		public String[] options;
		public String[] answer;
		public String[] response;
	}
	List<Question> questions;
	int getQuestions()
	{
		questions = new ArrayList<Question>();
		UserFunctions userFunction = new UserFunctions(this);
		Log.d("Button", "Questions");
		JSONObject json;
		try {
			
			json = userFunction.execute().get();
		// check for login response
		
			if (json!=null && json.getString(KEY_SUCCESS) != null) 
			{
				JSONArray response = json.getJSONArray("questions");
				int num = response.length();
				Log.d("Button",""+num);
				for(int i=0;i<num;i++)
				{
					JSONObject obj = response.getJSONObject(i);
					Question q = new Question();
					q.question = obj.getString("question");
					String type = obj.getString("type");
					if(type.contains("ADD_YN"))
						q.type = 3;
					else if(type.contains("UNI_MCQ"))
						q.type = 2;
					else if(type.contains("UNI_TX"))
						q.type = 4;
					if(q.type==1 || q.type==2)
					{
						q.options = obj.getString("options").split(";");
					}
					//q.answer = obj.getString("answer").split(";");
					questions.add(q);
				}
				return num;
			}
			else
			{
				return -1;
			}
		}
		catch(Exception e)
		{
			return -1;
		}
	}
	
	void submit()
	{
		Button submit = (Button)findViewById(R.id.submit);
		boolean correct = false;
		try{
			switch(questionType){
			case 1:
			{
				RadioGroup r = (RadioGroup)findViewById(R.id.radioGroup1);
				View radioButton = r.findViewById(r.getCheckedRadioButtonId());
				int idx = r.indexOfChild(radioButton);
				Question q = questions.get(questionNum);
				q.response = new String[1];
				q.response[0] = String.copyValueOf(q.options[idx].toCharArray());
				questions.set(questionNum, q);
				correct = true;
				break;
			}
			
			case 3:
			{
				RadioGroup r = (RadioGroup)findViewById(R.id.radioGroup1);
				View radioButton = r.findViewById(r.getCheckedRadioButtonId());
				int idx = r.indexOfChild(radioButton);
				Question q = questions.get(questionNum);
				q.response = new String[1];
				if(idx==0)
					q.response[0] = "Yes";
				else 
					q.response[0] = "No";
				questions.set(questionNum, q);
				correct = true;
				break;
			}
			case 2:
			{
				CheckBox[] chk = new CheckBox[4];
				chk[0] = (CheckBox)findViewById(R.id.checkBox1);
				chk[1] = (CheckBox)findViewById(R.id.checkBox2);
				chk[2] = (CheckBox)findViewById(R.id.checkBox3);
				chk[3] = (CheckBox)findViewById(R.id.checkBox4);
				Question q = questions.get(questionNum);
				q.response = new String[4];
				for(int i=0;i<4;i++)
				{
					if(chk[i].isChecked())
						q.response[i] = String.copyValueOf(q.options[i].toCharArray());
					else
						q.response[i] = null;
				}
				questions.set(questionNum, q);
				correct = true;
				break;
			}
			
			case 4:
			{
				EditText e = (EditText)findViewById(R.id.editText1);
				Question q = questions.get(questionNum);
				q.response = new String[1];
				q.response[0] = e.getText().toString();
				questions.set(questionNum, q);
				correct = true;
				break;
			}
			
			default:
			{
				break;
			}
			
			}
		}
		catch(Exception ex)
		{
			correct = false;
		}
		if(correct)
		{
			submit.setEnabled(false);
			Button next = (Button)findViewById(R.id.next);
			next.setEnabled(true);
		}
		else
		{
			Toast.makeText(this, "Select an option", Toast.LENGTH_SHORT).show();
		}
	}
	
	void showQuestion(int index)
	{
		if(index >= questions.size())
		{
			sendAnswers();
			return;
		}
		Question q = questions.get(index);
		TextView question;
		Button submit;
		Button next;
		questionType = q.type;
		questionNum = index;
		switch(q.type)
		{
		case 1: 
			{
				setContentView(R.layout.quiz_layout_mcq);
				question = (TextView) findViewById(R.id.question);
				question.setText(q.question);
				RadioButton r1 = (RadioButton) findViewById(R.id.radio0);
				r1.setText(q.options[0]);
				RadioButton r2 = (RadioButton) findViewById(R.id.radio1);
				r2.setText(q.options[1]);
				RadioButton r3 = (RadioButton) findViewById(R.id.radio2);
				r3.setText(q.options[2]);
				RadioButton r4 = (RadioButton) findViewById(R.id.radio3);
				r4.setText(q.options[3]);
				break;
			}	
		case 2:
		{
			setContentView(R.layout.quiz_layout_multiple);
			question = (TextView) findViewById(R.id.question);
			question.setText(q.question);
			CheckBox r1 = (CheckBox) findViewById(R.id.checkBox1);
			r1.setText(q.options[0]);
			CheckBox r2 = (CheckBox) findViewById(R.id.checkBox2);
			r2.setText(q.options[1]);
			CheckBox r3 = (CheckBox) findViewById(R.id.checkBox3);
			r3.setText(q.options[2]);
			CheckBox r4 = (CheckBox) findViewById(R.id.checkBox4);
			r4.setText(q.options[3]);
			break;
		}
		case 3:
		{
			setContentView(R.layout.quiz_layout_binary);
			question = (TextView) findViewById(R.id.question);
			question.setText(q.question);	
			break;
		}
		case 4:
		{
			setContentView(R.layout.quiz_layout_text);
			question = (TextView) findViewById(R.id.question);
			question.setText(q.question);	
			break;
			
		}
			
		default: break;
		}
		submit = (Button) findViewById(R.id.submit);
		next = (Button)  findViewById(R.id.next);
		submit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				submit();
			}
		});
		next.setEnabled(false);
		next.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showQuestion(questionNum+1);
			}
		});
	}
	
	private void sendAnswers() {
		// TODO Auto-generated method stub
		UserFunctions userFunction = new UserFunctions(this);
		JSONObject json; 
		try{
			json = userFunction.execute(questions).get();
			Toast.makeText(this, "Answers Submitted",Toast.LENGTH_LONG).show();
			
			Intent dashboard = new Intent(getApplicationContext(), MainActivity.class);
			dashboard.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(dashboard);
		}
		catch(Exception ex)
		{
			Toast.makeText(this, "No Network Connection. Unable to submit answers.",Toast.LENGTH_LONG).show();
		}
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		
		
		if(getQuestions()!=-1)
		{
			showQuestion(0);
		}
		else
		{
			Toast.makeText(this, "No Questions to show. Please check your connection.", Toast.LENGTH_LONG).show();
		}
	}
}
