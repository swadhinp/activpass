/**
 * Author: Ravi Tamada
 * URL: www.androidhive.info
 * twitter: http://twitter.com/ravitamada
 * */
package com.example.contactlist;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import org.apache.http.cookie.Cookie;
import org.json.JSONException;
import org.json.JSONObject;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.preference.PreferenceManager;
import android.content.SharedPreferences;

public class LoginActivity extends Activity {
	Button btnLogin;
	Button btnLinkToRegister;
	EditText inputEmail;
	EditText inputPassword;
	TextView loginErrorMsg;

	// JSON Response node names
	private static String KEY_SUCCESS = "success";
	private static String KEY_ERROR = "error";
	private static String KEY_ERROR_MSG = "error_msg";
	private static String KEY_UID = "uid";
	private static String KEY_NAME = "name";
	private static String KEY_EMAIL = "phone";
	private static String KEY_CREATED_AT = "created_at";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
	    String username = prefs.getString("username", "null");
		 if(!username.equalsIgnoreCase("null")){
			 	
	        	Intent login = new Intent(getApplicationContext(), MainActivity.class);
	        	login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        	startActivity(login);
	        	// Closing dashboard screen
	        	finish();        	
	        }
		 else{ 	
		setContentView(R.layout.login);

		// Importing all assets like buttons, text fields
		inputEmail = (EditText) findViewById(R.id.loginEmail);
		btnLogin = (Button) findViewById(R.id.btnLogin);
		loginErrorMsg = (TextView) findViewById(R.id.login_error);

		// Login button Click Event
		btnLogin.setOnClickListener(new View.OnClickListener() {

			public void onClick(View view) {
				String email = inputEmail.getText().toString();
				Log.d("Button", "Login");
				try {
					
					
					if (email != null) {
							// Uncomment Later
							UserFunctions userFunction = new UserFunctions(getApplicationContext());
							JSONObject json = userFunction.execute(email).get();
							Log.d("JSON", json.getString("login"));
							if(json!=null && json.getString("login").equals("success"))
							//if(json!=null)	
							{
								SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(view.getContext());
							    SharedPreferences.Editor spe = prefs.edit();
							    spe.putString("username", email);
							    spe.commit(); 
							
								Intent dashboard = new Intent(getApplicationContext(), MainActivity.class);
								
								// Close all views before launching Dashboard
								dashboard.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								startActivity(dashboard);
								
								// Close Login Screen
								finish();
							}
							//Uncomment Later
							else
							{
								Toast.makeText(getApplicationContext(), "Please give your registered username or Contact 09477092046", Toast.LENGTH_LONG).show();
							}
						}
					else{
							// Error in login
							loginErrorMsg.setText("Please enter a username");
						}
				} catch (Exception e) {
					e.printStackTrace();
				}
			
			}
		});
	
	}
	}
}
