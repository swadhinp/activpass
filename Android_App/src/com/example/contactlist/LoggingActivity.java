package com.example.contactlist;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.TimerTask;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

public class LoggingActivity extends TimerTask {

	Context c;
	public LoggingActivity(Context c,String uid)
	{
		this.c = c;
	}
	
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try
    	{
			Intent i= new Intent(c, LogService.class);
			c.startService(i); 
    	}
    	catch (Exception ex)
    	{
    		Log.e("File Uploader111", "error: " + ex.getMessage(), ex);
			
    	}
	}

}
