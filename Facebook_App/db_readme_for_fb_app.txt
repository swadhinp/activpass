Connection info string:
   "dbname=dc2ikdj9dlc16h host=ec2-107-22-163-140.compute-1.amazonaws.com port=5432 user=pgrhfegtaldfit password=YQBGzFJFosBH1ct2bdiBeBqIze sslmode=require"
Connection URL:
	postgres://pgrhfegtaldfit:YQBGzFJFosBH1ct2bdiBeBqIze@ec2-107-22-163-140.compute-1.amazonaws.com:5432/dc2ikdj9dlc16h

Postgresql Environment Variable:
HEROKU_POSTGRESQL_NAVY_URL

Command to transfer local database:

heroku pg:transfer -f postgres://cnerg:cnerg@localhost/fb_db -t postgres://pgrhfegtaldfit:YQBGzFJFosBH1ct2bdiBeBqIze@ec2-107-22-163-140.compute-1.amazonaws.com:5432/dc2ikdj9dlc16h


Commands to get data from facebook app:
heroku pgbackups:capture HEROKU_POSTGRESQL_NAVY_URL --expire

curl -o latest.dump `heroku pgbackups:url`
pg_restore --verbose --clean --no-acl --no-owner -h localhost -U cnerg -d fb_app_db latest.dump // after createdb fb_app_db
pg_restore --verbose --clean --no-acl --no-owner -h localhost -U cnerg -d fb_db latest.dump // after createdb fb_db


password for user cnerg is cnerg


destroying database in heroku
heroku pg:reset HEROKU_POSTGRESQL_NAVY_URL


dump to a file
pg_dump fb_db >fb_db_dump
COPY admin_page_table TO STDOUT (DELIMITER '|');
COPY friend_table TO '/home/cnerg/social_activity/calm-plains-2859/test_friend_table.txt' (DELIMITER '|');
OR
\copy friend_table TO /home/cnerg/friend.csv CSV DELIMITER '|'

Restoring database:
Drop all tables
psql dbname < infile


PSQL Commands:
---------------
psql -> \c db_name ( Connect ) -> \dt ( shows tables ) -> \q (quit)



git commit -am "comments"
git push heroku



CREATE TABLE message_table (
user_id varchar(30),
user_name varchar(100),
sent_received integer,
body varchar(300),
message_id varchar(100),
created_time varchar(100),
PRIMARY KEY(user_id,message_id)
);

CREATE TABLE admin_page_table (
user_id varchar(30),
name varchar(100),
page_id varchar(30),
PRIMARY KEY(user_id,page_id)
);


CREATE TABLE group_table (
user_id varchar(30),
group_name varchar(100),
group_id varchar(30),
privacy varchar(30),
PRIMARY KEY(user_id,group_id)
);

CREATE TABLE user_profile (
user_id varchar(30) PRIMARY KEY,
user_name varchar(100)
);


CREATE TABLE friend_table (
user_id varchar(30),
friend_id varchar(30),
friend_username varchar(50),
friend_name varchar(100),
PRIMARY KEY(user_id,friend_id)
);
