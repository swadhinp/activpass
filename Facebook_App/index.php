<?php

/**
 * This sample app is provided to kickstart your experience using Facebook's
 * resources for developers.  This sample app provides examples of several
 * key concepts, including authentication, the Graph API, and FQL (Facebook
 * Query Language). Please visit the docs at 'developers.facebook.com/docs'
 * to learn more about the resources available to you
 */

// Provides access to app specific values such as your app id and app secret.
// Defined in 'AppInfo.php'
require_once('AppInfo.php');

// Enforce https on production

if (substr(AppInfo::getUrl(), 0, 8) != 'https://' && $_SERVER['REMOTE_ADDR'] != '127.0.0.1') 
{
  header('Location: https://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
  exit();
}

// This provides access to helper functions defined in 'utils.php'
require_once('utils.php');


/*****************************************************************************
 *
 * The content below provides examples of how to fetch Facebook data using the
 * Graph API and FQL.  It uses the helper functions defined in 'utils.php' to
 * do so.  You should change this section so that it prepares all of the
 * information that you want to display to the user.
 *
 ****************************************************************************/

require_once('sdk/src/facebook.php');

$app_namespace = 'activity_recognition';
$app_url = 'https://apps.facebook.com/' . $app_namespace . '/';
$scope = 'email,publish_actions,user_groups,user_likes, user_photos,user_subscriptions,manage_friendlists,manage_pages,read_friendlists,read_mailbox,read_page_mailboxes';
//$scope = 'email,publish_actions,user_about_me,user_actions.books,user_actions.music,user_actions.news,user_actions.video,user_activities,user_birthday,user_education_history,user_events,user_games_activity,user_groups,user_hometown,user_interests,user_likes, user_location,user_notes,user_photos,user_questions,user_relationship_details,user_relationships,user_religion_politics,user_status,user_subscriptions,user_videos,user_website,user_work_history,ads_management,create_event,create_note,export_stream,friends_online_presence,manage_friendlists,manage_notifications,manage_pages,photo_upload,publish_stream,read_friendlists,read_insights,read_mailbox,read_page_mailboxes,read_requests,read_stream,rsvp_event,share_item,sms,status_update,user_online_presence,video_upload,xmpp_login';
 
$facebook = new Facebook(array(
  'appId'  => AppInfo::appID(),
  'secret' => AppInfo::appSecret(),
  'sharedSession' => true,
  'trustForwarded' => true,
));

$user_id = $facebook->getUser();

if (!$user_id) {
        $loginUrl = $facebook->getLoginUrl(array(
        'scope' => $scope,
        'redirect_uri' => $app_url,
        ));

        print('<script> top.location.href=\'' . $loginUrl . '\'</script>');
}

if ($user_id) {
  try {
    // Fetch the viewer's basic information
    $basic = $facebook->api('/me');
  } catch (FacebookApiException $e) {
    // If the call fails we check if we still have a user. The user will be
    // cleared if the error is because of an invalid accesstoken
    //if (!$facebook->getUser()) {
      //header('Location: '. AppInfo::getUrl($_SERVER['REQUEST_URI']));
      exit();
    //}
  }

  // This fetches some things that you like . 'limit=*" only returns * values.
  // To see the format of the data you are retrieving, use the "Graph API
  // Explorer" which is at https://developers.facebook.com/tools/explorer/
  $likes = idx($facebook->api('/me/likes?limit=16'), 'data', array());
  
  // This fetches 4 of your friends.
  $friends = idx($facebook->api('/me/friends?limit=16'), 'data', array());

  // And this returns 16 of your photos.
  $photos = idx($facebook->api('/me/photos?limit=16'), 'data', array());

  // Here is an example of a FQL call that fetches all of your friends that are
  // using this app
  $app_using_friends = $facebook->api(array(
    'method' => 'fql.query',
    'query' => 'SELECT uid, name FROM user WHERE uid IN(SELECT uid2 FROM friend WHERE uid1 = me()) AND is_app_user = 1'
  ));
  
$my_friends = $facebook->api(array(
    'method' => 'fql.query',
    'query' => 'SELECT uid, username, name FROM user WHERE uid IN(SELECT uid2 FROM friend WHERE uid1 = me())'
  ));

    $my_groups = $facebook->api(array(
    'method' => 'fql.query',
    'query' => 'SELECT name, gid, description, privacy FROM group WHERE gid IN (SELECT gid FROM group_member WHERE uid = me())'
));
 /*     $messages = $facebook->api(array(
    	'method' => 'fql.query',
    	'query' => 'SELECT thread_id, message_id, author_id, created_time, body FROM message WHERE thread_id IN (SELECT thread_id FROM thread WHERE folder_id = 0) ORDER BY created_time ASC LIMIT 100'
));*/

$multiqueries = array(
    'messages' => 'SELECT message_id, thread_id, author_id, body, created_time, attachment, viewer_id FROM message WHERE thread_id IN (SELECT thread_id FROM thread WHERE folder_id = 0) ORDER BY created_time DESC LIMIT 100',
    'users' => 'SELECT id, name FROM profile WHERE id IN (SELECT author_id FROM #messages)',
);

$param = array(
    'method'   => 'fql.multiquery',
    'queries'  => $multiqueries
);

try {
    $fqlresults = $facebook->api($param);
} catch (FacebookApiException $e) {
    echo "EXCEPTION {$e->getMessage()}\n";
}



$multiqueries_pages = array(
    'query1' => 'SELECT page_id FROM page_admin WHERE uid = me()',
    'query2' => 'SELECT page_id, name, page_url FROM page WHERE page_id IN (SELECT page_id FROM #query1)',
);

$param_pages = array(
    'method'   => 'fql.multiquery',
    'queries'  => $multiqueries_pages
);

try {
    $fqlresults_pages = $facebook->api($param_pages);
} catch (FacebookApiException $e) {
    echo "EXCEPTION {$e->getMessage()}\n";
}

}

// Fetch the basic info of the app that they are using
$app_info = $facebook->api('/'. AppInfo::appID());

$app_name = idx($app_info, 'name', '');

?>
<!DOCTYPE html>
<html xmlns:fb="http://ogp.me/ns/fb#" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=yes" />

    <title><?php echo he($app_name); ?></title>
    <link rel="stylesheet" href="stylesheets/screen.css" media="Screen" type="text/css" />
    <link rel="stylesheet" href="stylesheets/mobile.css" media="handheld, only screen and (max-width: 480px), only screen and (max-device-width: 480px)" type="text/css" />

    <!--[if IEMobile]>
    <link rel="stylesheet" href="mobile.css" media="screen" type="text/css"  />
    <![endif]-->

    <!-- These are Open Graph tags.  They add meta data to your  -->
    <!-- site that facebook uses when your content is shared     -->
    <!-- over facebook.  You should fill these tags in with      -->
    <!-- your data.  To learn more about Open Graph, visit       -->
    <!-- 'https://developers.facebook.com/docs/opengraph/'       -->
    <meta property="og:title" content="<?php echo he($app_name); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo AppInfo::getUrl(); ?>" />
    <meta property="og:image" content="<?php echo AppInfo::getUrl('/logo.png'); ?>" />
    <meta property="og:site_name" content="<?php echo he($app_name); ?>" />
    <meta property="og:description" content="My first app" />
    <meta property="fb:app_id" content="<?php echo AppInfo::appID(); ?>" />

    <script type="text/javascript" src="/javascript/jquery-1.7.1.min.js"></script>

    <script type="text/javascript">
      function logResponse(response) {
        if (console && console.log) {
          console.log('The response was', response);
        }
      }

      $(function(){
        // Set up so we handle click on the buttons
        $('#postToWall').click(function() {
          FB.ui(
            {
              method : 'feed',
              link   : $(this).attr('data-url')
            },
            function (response) {
              // If response is null the user canceled the dialog
              if (response != null) {
                logResponse(response);
              }
            }
          );
        });

        $('#sendToFriends').click(function() {
          FB.ui(
            {
              method : 'send',
              link   : $(this).attr('data-url')
            },
            function (response) {
              // If response is null the user canceled the dialog
              if (response != null) {
                logResponse(response);
              }
            }
          );
        });

        $('#sendRequest').click(function() {
          FB.ui(
            {
              method  : 'apprequests',
              message : $(this).attr('data-message')
            },
            function (response) {
              // If response is null the user canceled the dialog
              if (response != null) {
                logResponse(response);
              }
            }
          );
        });
      });
    </script>

    <!--[if IE]>
      <script type="text/javascript">
        var tags = ['header', 'section'];
        while(tags.length)
          document.createElement(tags.pop());
      </script>
    <![endif]-->
  </head>
  <body>
    <div id="fb-root"></div>
    <script type="text/javascript">
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '<?php echo AppInfo::appID(); ?>', // App ID
          channelUrl : '//<?php echo $_SERVER["HTTP_HOST"]; ?>/channel.html', // Channel File
          status     : true, // check login status
          cookie     : true, // enable cookies to allow the server to access the session
          xfbml      : true // parse XFBML
        });

        // Listen to the auth.login which will be called when the user logs in
        // using the Login button
        FB.Event.subscribe('auth.login', function(response) {
          // We want to reload the page now so PHP can read the cookie that the
          // Javascript SDK sat. But we don't want to use
          // window.location.reload() because if this is in a canvas there was a
          // post made to this page and a reload will trigger a message to the
          // user asking if they want to send data again.
          window.location = window.location;
        });

        FB.Canvas.setAutoGrow();
      };

      // Load the SDK Asynchronously
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>

    <header class="clearfix">
      <?php if (isset($basic)) { ?>
      <p id="picture" style="background-image: url(https://graph.facebook.com/<?php echo he($user_id); ?>/picture?type=normal)"></p>

      <div>
        <h1>Welcome!!! <strong><?php echo he(idx($basic, 'name')); 
	//echo "<br>".$_SERVER['REMOTE_ADDR']."<br>". $_SERVER['HTTP_HOST'] ."<br>". $_SERVER['REQUEST_URI'];

	 ?></strong></h1>
        <p class="tagline">
          This is your app
          <a href="<?php echo he(idx($app_info, 'link'));?>" target="_top"><?php echo he($app_name); ?></a>
        </p>

        <div id="share-app">
          <p>Share your app:</p>
          <ul>
            <li>
              <a href="#" class="facebook-button" id="postToWall" data-url="<?php echo AppInfo::getUrl(); ?>">
                <span class="plus">Post to Wall</span>
              </a>
            </li>
            <li>
              <a href="#" class="facebook-button speech-bubble" id="sendToFriends" data-url="<?php echo AppInfo::getUrl(); ?>">
                <span class="speech-bubble">Send Message</span>
              </a>
            </li>
            <li>
              <a href="#" class="facebook-button apprequests" id="sendRequest" data-message="Test this awesome app">
                <span class="apprequests">Send Requests</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <?php } else { ?>
      <div>
        <h1>Welcome</h1>
        <div class="fb-login-button" data-scope="user_likes,user_photos"></div>
      </div>
      <?php } ?>
    </header>

<!--    <section id="get-started">
      <p>Welcome to Sourav Dandapat's Facebook app, running on <span>heroku</span>!</p>
      <a href="https://devcenter.heroku.com/articles/facebook" target="_top" class="button">Learn How to Edit This App</a>
    </section> -->

    <?php
	# This function reads your DATABASE_URL configuration automatically set by Heroku
	# the return value is a string that will work with pg_connect
	/*function pg_connection_string() {
  		return "dbname=da9b3kibc4loli host=ec2-54-227-255-156.compute-1.amazonaws.com port=5432 user=qwrtyuyfzeelnl password=Lara_ecf8s5Yl-ePQq62Luyh4b sslmode=require";
	}
 
	# Establish db connection
	$db = pg_connect(pg_connection_string());
	if (!$db) {
   		echo "Database connection error."
   		exit;
	}*/



	
	$dbconn = pg_connect("host=ec2-107-22-163-140.compute-1.amazonaws.com port=5432 dbname=dc2ikdj9dlc16h user=pgrhfegtaldfit password=YQBGzFJFosBH1ct2bdiBeBqIze sslmode=require options='--client_encoding=UTF8'") or die('Could not connect: ' . pg_last_error()); 
	
	$mysqltime = date ("Y-m-d", $phptime);
	$messages1234 = $fqlresults[0]['fql_result_set'];
	$users1234 = $fqlresults[1]['fql_result_set'];
	//print_r($my_groups);
	$query1_result = $fqlresults_pages[0]['fql_result_set'];
	$query2_result = $fqlresults_pages[1]['fql_result_set'];

//	print_r($query2_result);

	//Creating User_Profile_Table
	$user_profile_name = idx($basic,'name');
	$user_profile_res = pg_query($dbconn, "INSERT INTO user_profile (user_id,user_name) VALUES ('$user_id','$user_profile_name')");
	if (!$user_profile_res) { 
    		pg_query($dbconn, "ROLLBACK"); 
		$user_profile_roll = "ROLL-PROFILE";
	} else { 
    		pg_query($dbconn, "COMMIT"); 
		$user_profile_roll = "COMM-PROFILE";
	}

	//Creating Friend_Table
	foreach ($my_friends as $my_friend){
		$friend_uid = idx($my_friend,'uid');
		$friend_name = idx($my_friend,'name');
		$friend_username = idx($my_friend,'username');
		$friend_res = pg_query($dbconn, "INSERT INTO friend_table (user_id,friend_id,friend_username,friend_name) VALUES ('$user_id','$friend_uid','$friend_username','$friend_name')");
		if (!$friend_res) { 
    			pg_query($dbconn, "ROLLBACK"); 
			$friend_roll = "ROLL-FRIEND";
		} else { 
    			pg_query($dbconn, "COMMIT"); 
			$friend_roll = "COMM-FRIEND";
		}
	}


	//Creating Group_Table
	foreach ($my_groups as $group){
		$group_name = idx($group,'name');
		$group_privacy = idx($group,'privacy');
		$group_id = idx($group,'gid');
		$group_res = pg_query($dbconn, "INSERT INTO group_table (user_id,group_name,group_id,privacy) VALUES ('$user_id','$group_name','$group_id','$group_privacy')"); 
		if (!$group_res) { 
    			pg_query($dbconn, "ROLLBACK"); 
			$group_roll = "ROLL-GROUP";
		} else { 
    			pg_query($dbconn, "COMMIT"); 
			$group_roll = "COMM-GROUP";
		}
	}
	
	
	//Creating Page_Table
	foreach ($query2_result as $page) {
		$page_id = idx($page, 'page_id');
        	$page_name = idx($page, 'name');
        	$page_url = idx($page, 'page_url');
		$page_res = pg_query($dbconn, "INSERT INTO admin_page_table (user_id,name,page_id) VALUES ('$user_id','$page_name','$page_id')"); 

	      // Verify and end the transaction as appropriate. 
		if (!$page_res) { 
    			pg_query($dbconn, "ROLLBACK"); 
			$page_roll = "ROLL-PAGE";
		} else { 
    			pg_query($dbconn, "COMMIT"); 
			$page_roll = "COMM-PAGE";
		}
	}

	//Creating Message_Table
	foreach ($messages1234 as $message) {
		$user_id2 = idx($message, 'author_id');
        	$message_identifier = idx($message, 'message_id');
		$time = idx($message, 'created_time');
		$thread_id = idx($message, 'created_time');
		$body = idx($message, 'body');
		$viewer_id = idx($message, 'viewer_id');
		foreach ($users1234 as $user) {
			$user_id1 = idx($user, 'id');
			if($user_id1 == $user_id2)
				$user_name = idx($user, 'name');
		}
		if($user_id == $user_id2){
			$sent_received = 1;
		}
		else {
			$sent_received = 0;
		}
		$message_res = pg_query($dbconn, "INSERT INTO message_table (user_id,user_name,sent_received,body,message_id,created_time) VALUES ('$user_id2','$user_name','$sent_received','$body','$message_identifier','$time')"); 

		if (!$message_res) { 
    			pg_query($dbconn, "ROLLBACK"); 
			$message_roll = "ROLL-MESSAGE";
		} else { 
    			pg_query($dbconn, "COMMIT"); 
			$message_roll = "COMM-MESSAGE";
		}
	}
	 
	$reqcount = pg_query($dbconn, "SELECT * FROM message_table");
	$result = pg_num_rows($reqcount);
    ?>
     
    <?php
      if ($user_id) {
    ?>

    <section id="samples" class="clearfix">
	
<!--      <h1>Examples of the Facebook Graph API -->
<?php //echo he($user_id); echo("    "); echo he($message_body); echo he($user_name); echo("    "); echo he($message_body);echo("    "); echo he($message_identifier); echo("    "); echo he($time); echo("    "); echo he($roll); echo(" "); echo he($roll1); echo(" "); echo he($roll2);?> 
<!--</h1> -->

      <div class="list inline">
        <h3>A few of your friends</h3>
        <ul class="friends">
          <?php
            $i = 0;
            foreach ($friends as $friend) {
              // Extract the pieces of info we need from the requests above
              $id = idx($friend, 'id');
              $name = idx($friend, 'name');
              $class = ($i++ % 4 === 0) ? 'first-column' : '';
          ?>
          <li class="<?php echo $class; ?>">
            <a href="https://www.facebook.com/<?php echo he($id); ?>" target="_top">
              <img src="https://graph.facebook.com/<?php echo he($id) ?>/picture?type=square" alt="<?php echo he($name); ?>">
              <?php echo he($name); ?>
            </a>
          </li>
          <?php
            }
          ?>
        </ul>
      </div>

      <div class="list inline">
        <h3>Recent photos</h3>
        <ul class="photos">
          <?php
            $i = 0;
            foreach ($photos as $photo) {
              // Extract the pieces of info we need from the requests above
              $id = idx($photo, 'id');
              $picture = idx($photo, 'picture');
              $link = idx($photo, 'link');

              $class = ($i++ % 4 === 0) ? 'first-column' : '';
          ?>
          <li style="background-image: url(<?php echo he($picture); ?>);" class="<?php echo $class; ?>">
            <a href="<?php echo he($link); ?>" target="_top"></a>
          </li>
          <?php
            }
          ?>
        </ul>
      </div>

      <div class="list">
        <h3>Things you like</h3>
        <ul class="things">
          <?php
            foreach ($likes as $like) {
              // Extract the pieces of info we need from the requests above
              $id = idx($like, 'id');
              $item = idx($like, 'name');

              // This display's the object that the user liked as a link to
              // that object's page.
          ?>
          <li>
            <a href="https://www.facebook.com/<?php echo he($id); ?>" target="_top">
              <img src="https://graph.facebook.com/<?php echo he($id) ?>/picture?type=square" alt="<?php echo he($item); ?>">
              <?php echo he($item); ?>
            </a>
          </li>
          <?php
            }
          ?>
        </ul>
      </div>

      <div class="list">
        <h3>Friends using this app</h3>
        <ul class="friends">
          <?php
            foreach ($app_using_friends as $auf) {
              // Extract the pieces of info we need from the requests above
              $id = idx($auf, 'uid');
              $name = idx($auf, 'name');
          ?>
          <li>
            <a href="https://www.facebook.com/<?php echo he($id); ?>" target="_top">
              <img src="https://graph.facebook.com/<?php echo he($id) ?>/picture?type=square" alt="<?php echo he($name); ?>">
              <?php echo he($name); ?>
            </a>
          </li>
          <?php
            }
          ?>
        </ul>
      </div>
    </section>

    <?php
      }
    ?>

<!--    <section id="guides" class="clearfix">
      <h1>Learn More About Heroku &amp; Facebook Apps</h1>
      <ul>
        <li>
          <a href="https://www.heroku.com/?utm_source=facebook&utm_medium=app&utm_campaign=fb_integration" target="_top" class="icon heroku">Heroku</a>
          <p>Learn more about <a href="https://www.heroku.com/?utm_source=facebook&utm_medium=app&utm_campaign=fb_integration" target="_top">Heroku</a>, or read developer docs in the Heroku <a href="https://devcenter.heroku.com/" target="_top">Dev Center</a>.</p>
        </li>
        <li>
          <a href="https://developers.facebook.com/docs/guides/web/" target="_top" class="icon websites">Websites</a>
          <p>
            Drive growth and engagement on your site with
            Facebook Login and Social Plugins.
          </p>
        </li>
        <li>
          <a href="https://developers.facebook.com/docs/guides/mobile/" target="_top" class="icon mobile-apps">Mobile Apps</a>
          <p>
            Integrate with our core experience by building apps
            that operate within Facebook.
          </p>
        </li>
        <li>
          <a href="https://developers.facebook.com/docs/guides/canvas/" target="_top" class="icon apps-on-facebook">Apps on Facebook</a>
          <p>Let users find and connect to their friends in mobile apps and games.</p>
        </li>
      </ul>
    </section> -->
  </body>
</html>
