#!/usr/bin/python
import json
import sys
import random
import os
import re
from random import choice
from math import sqrt
from math import ceil


if len(sys.argv) != 3:
	print "\nUsage: <Program_name> <Response> <User_name>\n"
	exit(0)

#print "master"
response = str(sys.argv[1])

user_name = str(sys.argv[2])

exp_read_handle          = open( "/home/cnerg/Activpass/Server/www/activPass_core/response_base/next_experiment_number.txt",'r')
experiment_number = int((exp_read_handle.readline()).strip())
exp_read_handle.close()

asked_question_file_name = user_name + "_asked_questions.log.backup" 
qs_read_handle          = open( "/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + asked_question_file_name,'r')
qs_read_content         = qs_read_handle.readlines()
qs_read_handle.close()

def hamdist(str1, str2):
        diffs = 0
        for ch1, ch2 in zip(str1, str2):
                if ch1 != ch2:
                        diffs += 1
	if diffs*3 <= len(str1):
        	return True

def check_for_correct_name( ans_str, cor_str ):
	#possible_name_list = cor_str.strip().split(';')
	#approx_name_list = []
	#for nm in possible_name_list:
	#	approx_name_list.append(nm.strip().split(' ')[0].lower())

	act_name_list = ans_str.lower().strip().split(' ')
	cor_name_list = cor_str.lower().strip().split(' ')

	##print act_name_list
	##print cor_name_list

	for elem in cor_name_list:
		for elem2 in act_name_list:
			if hamdist(elem,elem2):
				return "RIGHT"
	
	return "WRONG"

def check_for_mcq_correct ( ans_str, cor_str ):
	##print "Cor ", cor_str
	##print "Ans ", ans_str
	option_list = cor_str.lower().strip().rstrip(',').rstrip(';').split(';')
	ans_list = ans_str.lower().strip().rstrip(',').split(',')
	
	##print option_list
	##print ans_list

	if len(option_list) >=1 and len(ans_list) == 0:
		return "WRONG"
	
	double_len = len(ans_list)*2

	if len(option_list) >= len(ans_list) and double_len >= len(option_list):
		for i in range( len(ans_list) ):
			if ans_list[i].lower() in  option_list:
				continue
			else:
				return "WRONG"

		return "RIGHT"
	else:
		return "WRONG"
	
	#return "CORRECT"

mcq_flag = 0
qs_asked = ""			
i=0
right = 0

ans = response.strip().split(";")
num_qs = len(ans)
#print ans
#respons = ans
#print ans

response_file_name = user_name + "_question_answer_response.log" 
ans_handle = open("/home/cnerg/Activpass/Server/www/activPass_core/response_base/" + response_file_name,'a')

i=0

for line in qs_read_content:
	#print line
	line_s = line.strip().split("|")
	type1 = line_s[2]

	mcq_flag = 0
	if ( re.search( r"MCQ", type1)):
		mcq_flag = 1

	actual_ans = ""
	if mcq_flag == 0:
		actual_ans = line_s[4]
		if ( not ( re.search( r"TX", type1 ) ) ):
			if ( re.search( r"YN", type1 ) ):
				ans_n = "X"
				if ans[i] == "Yes,":
					ans_n="Y"
				else:
					ans_n="N"
        		if ans_n.lower() == actual_ans.lower():
                		result = "RIGHT"
				ans_handle.write( str(experiment_number) + "|" + line.strip() + "|" + ans[i] + "|" + result +"\n" )
				right+=1
        		else:
                		result = "WRONG"
				ans_handle.write( str(experiment_number) + "|" + line.strip() + "|" + ans[i] + "|" + result + "\n")
		else:
			result = check_for_correct_name( ans[i], actual_ans )
			ans_handle.write( str(experiment_number) + "|" + line.strip() + "|" + ans[i] + "|" + result + "\n")
			
	else:
		actual_ans = line_s[5]
		result = check_for_mcq_correct( ans[i], actual_ans )	
		ans_handle.write( str(experiment_number) + "|" + line.strip() + "|" + ans[i] + "|" + result + "\n")

	i=i+1

ans_handle.close()

exp_read_handle   = open( "/home/cnerg/Activpass/Server/www/activPass_core/response_base/next_experiment_number.txt",'w')
experiment_number = experiment_number + 1
exp_read_handle.write( str(experiment_number) )
exp_read_handle.close()


#os.system("rm -rf "+v)
#print right
s={}
s["success"]=right
print json.dumps(s)
