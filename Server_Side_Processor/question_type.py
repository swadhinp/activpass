#!/usr/bin/python

import sys
import random
import os
import re
from random import choice
from math import sqrt
from math import ceil


if len(sys.argv) != 2:
        print "\nUsage: <Program_name> <User_Name>\n"
        exit(0)

user_name = str(sys.argv[1])

user_question_file_name = "/home/cnerg/Activpass/Server/www/activPass_core/question_base/" +user_name + "_all_question_answer.log"
user_question_type_file_name = "/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + user_name + "_all_question_type.log"


qs_ans_read_handle          = open(user_question_file_name, 'r')
qs_ans_read_content         = qs_ans_read_handle.readlines()
qs_ans_read_handle.close()


cmd = "touch "+user_question_type_file_name
os.system(cmd);

qs_ans_read_handle          = open(user_question_type_file_name, 'r')
qs_type_read_content         = qs_ans_read_handle.readlines()
qs_ans_read_handle.close()


qs_ans_read_handle          = open(user_question_type_file_name, 'a')

question_type_freq_dict = {}
available_question_type_dict = {}


for line in qs_ans_read_content:
	s=line.strip().split('|')[0:3]
	s_mod = str(s[0])+"|"+str(s[1])+"|"+str(s[2])
	if s_mod not in question_type_freq_dict:
		question_type_freq_dict[s_mod] = 1
	else:
		question_type_freq_dict[s_mod] = question_type_freq_dict[s_mod] + 1

for line in qs_type_read_content:
	s=line.strip().split('|')[0:3]
	s_mod = str(s[0])+"|"+str(s[1])+"|"+str(s[2])
	if s_mod not in available_question_type_dict:
		available_question_type_dict[s_mod] = 1


print question_type_freq_dict
for key1 in question_type_freq_dict:
	if key1 not in available_question_type_dict:
		new_type = key1+"\n" 
		print new_type 
		qs_ans_read_handle.write(new_type)
	#	cmd_str = "echo "+new_type+" >>"+user_question_type_file_name
	#	os.system(cmd_str)
qs_ans_read_handle.close()
