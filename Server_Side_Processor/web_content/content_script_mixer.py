#!/usr/bin/python
import sys
import random
import os
import re
from random import choice
from math import sqrt
from math import ceil
from os import listdir
from os.path import isfile, join

if len(sys.argv) != 3:
        #print "\nUsage: <Program_name> <Parsed_Content_Script_Files_Directory_Path> <Final Content Script File Name>\n"
        exit(0)

dir_path = str(sys.argv[1])

onlyfiles = [ f for f in listdir(dir_path) if isfile(join(dir_path,f)) ]
##print onlyfiles

content_script_all_write_handle = open(sys.argv[2],'w')

for f_name in onlyfiles:

	user_name = f_name.strip().split('_')[0]
	f_path = dir_path + "/" + f_name

	history_read_handle          = open(f_path,'r')
	history_read_content         = history_read_handle.readlines()
	history_read_handle.close()

	for line in history_read_content:
		content_script_all_write_handle.write(user_name + "||" + line)

content_script_all_write_handle.close()	
print "success1"
