#!/usr/bin/python
import sys
import random
import os
import re
from random import choice
from random import sample
from math import sqrt
from math import ceil
import time
from datetime import date


if len(sys.argv) != 4:
        print "\nUsage: <Program_name> <All_users_Mixed_Content_Script_File> <Parsed_Web_History_File> <User_Name>\n"
        exit(0)

#print "script file = " + sys.argv[1]
#print "web file = " + sys.argv[2]
#print "user name = " + sys.argv[3]
content_script_read_handle           = open(sys.argv[1],'r')
content_script_read_content          = content_script_read_handle.readlines()
content_script_read_handle.close()

web_history_read_handle              = open(sys.argv[2],'r')
web_history_read_content             = web_history_read_handle.readlines()
web_history_read_handle.close()

user_name 	= str(sys.argv[3])

#Sorting Needs to be done after date issue fixed
qs_ans_out_file_name             = user_name + "_question_answer_web_content.log"
qs_ans_out_file                  = open( "/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + qs_ans_out_file_name, 'w');

#Putting the files info into appropriate data structures

content_script_dict ={}
content_script_dict_other ={}
content_script_all = {}
elem_dict = {}
#Question Generation from Content Script

#Yes_No Type Questions
for line in content_script_read_content:
	line_lst = line.strip().split("||")
	#print line_lst
	try:
		if len(line_lst) == 3 :
			if line_lst[0] == user_name:
				content_script_dict[line_lst[1]] = line_lst[2]
			else:
				content_script_dict_other[line_lst[1]] = line_lst[2]
			content_script_all[line_lst[1]] = line_lst[2]
	except IndexError:
		print "Content script Index Error"

#print "11111111111"
#print content_script_dict
front_profile_str = "CONTENT_SCRIPT|POSITIVE|ADD_YN|Have you visited and looked at today/this week the web-page about "

len_content_script_dict = len(content_script_dict)
nlen = int(len_content_script_dict)/4
i=0
for elem in content_script_dict:
	if elem not in elem_dict:
		elem_dict[elem] =1
		try:
			elem.decode('ascii')
		except UnicodeDecodeError:
			continue	
		line_str = front_profile_str + str(elem).replace('|','-') + " ? ( Y/N )|Y" + "\n"
		qs_ans_out_file.write(line_str)
		i = i+1
		if i == nlen :
			break

front_profile_str = "CONTENT_SCRIPT|NEGATIVE|ADD_YN|Have you visited and looked at today/this week the web-page about "

len_content_script_dict_other = len(content_script_dict_other)
nlen_other = int(len_content_script_dict_other)/4
i=0
for elem in content_script_dict_other:
	if elem not in content_script_dict:
		if elem not in elem_dict:
			elem_dict[elem]=1
			try:
				elem.decode('ascii')
			except UnicodeDecodeError:
				continue	
			line_str = front_profile_str + str(elem).replace('|','-') + " ? ( Y/N )|N" + "\n"
			qs_ans_out_file.write(line_str)
			i=i+1
			if i == nlen_other:
				break


#print content_script_dict_other

#MCQs regarding have you visited Qs for content script
content_script_dict_list = []
content_script_dict_other_list = []

for key in content_script_dict:
	content_script_dict_list.append(key)

for key in content_script_dict_other:
	content_script_dict_other_list.append(key)

mcq_ans_str = ""
def make_mcq_for_content_script ( num, opt ):
	global mcq_ans_str
	#print "333333"
	print "content_script_dict = "
	print content_script_dict
	print "content_script_dict_other = "
	print content_script_dict_other
	if num == 1 :
		if len(content_script_dict) > 0:
			while 1:
				elem = content_script_dict_list[random.randint(0,len(content_script_dict)-1)]
				try:
                                	elem.decode('ascii')
					return elem
                        	except UnicodeDecodeError:
                                	continue
		else:
			return "agn"
	else :
		if len(content_script_dict_other) > 0:
			while 1:
				elem = content_script_dict_other_list[random.randint(0,len(content_script_dict_other)-1)]
				try:
                                	elem.decode('ascii')
					return elem
                        	except UnicodeDecodeError:
                                	continue
		else:
			return "agn"

def have_some_mcq_for_content_script ( front_str ):

	global mcq_ans_str
	mcq_ans_str = ""
	q_str =""
	ret_val = ""
	for i in [ "A", "B", "C", "D"]:
		while True:
			rand_num = random.randint(1,2)
			ret_val = make_mcq_for_content_script(rand_num,i)
			if ret_val != "agn":
				if ret_val not in elem_dict:
					if rand_num == 1:
						mcq_ans_str = mcq_ans_str + i + ";"
					elem_dict[ret_val] =1
					break
		#print rand_num
		q_str = q_str + i + ".  "  + ret_val.replace('|', '-') + ";;"
	#print q_str
	line_str = front_str + q_str + "|" + mcq_ans_str + "\n"
	#print "44444"
	return line_str

#MCQ based Questions on Content Script History
front_profile_str = "CONTENT_SCRIPT|POS_NEG|UNI_MCQ|Please click the option(s) of the pages you visited today/this week (skip question if all options are wrong) :|" 

p = len(content_script_all)
no_of_con_qu =0
if p > 4:
	no_of_con_qu = p/8
	if no_of_con_qu < 1 :
		no_of_con_qu =1
	elif no_of_con_qu > 5:
		no_of_con_qu = 5
for i in range(no_of_con_qu): #No of MCQs
	qs_ans_out_file.write(have_some_mcq_for_content_script(front_profile_str))

#Question Generation from Web History

#Putting Web history into a dictionary
web_history_dict = {}
todays_date = date.today()
for line in web_history_read_content:
	#print "hiiii"
     try:
	line_m = line.strip().split("||")
	web_browsing_date = date(int(line_m[2].split('/')[3]),int(line_m[2].split('/')[2]), int(line_m[2].split('/')[1]))
	date_diff = (todays_date - web_browsing_date).days
	if date_diff <= 3:	
		if line_m[0] not in web_history_dict:
			web_history_dict[line_m[0]] = [line_m[1] ]
		else:
			web_history_dict[line_m[0]].append(line_m[1])
     except:
	x=1 
#Sorting web history list according to time and visit count
#sorted_based_on_time_list_descending = sorted(history_dict.items(), key=lambda x: x[1], reverse = True)

#print "web_history_dict"	
#print web_history_dict	
web_temp_dict = {}
for elem in web_history_dict:
	#print elem
	q_count = 0
	if elem == user_name:
		#front_profile_web_line_str = "WEB_DATA|POSITIVE|ADD_YN| Have you looked into and visited "
		front_profile_web_line_str = "WEB_DATA|POSITIVE|ADD_YN| Have you looked into and read about "
		end_web_line_str = " today/this week ? (Y/N) "
		
		#sampl_size = min( [ len(web_history_dict[elem]), 20 ] )
		#uniq_random_list = sample(xrange(len(web_history_dict[elem])), sampl_size)
		total_user_web_history_size = len(web_history_dict[elem])
		no_web_question = int(total_user_web_history_size)/4
		i=0

		#print uniq_random_list
		while i< no_web_question :
			str2=web_history_dict[elem][random.randint(0,len(web_history_dict[elem])-1)]
			if str2 not in elem_dict:
				elem_dict[str2]=1
				str1 = str(str2).replace('|',' ')

				if str1 not in web_temp_dict:
					web_temp_dict[str1]=1

					if str1 != "":
						qs_ans_out_file.write( front_profile_web_line_str + str1 + end_web_line_str + "|" + "Y\n" )
						q_count += 1
						i = i+1

	else:
		#front_profile_web_line_str = "WEB_DATA|NEGATIVE|ADD_YN| Have you looked into and browsed "
		front_profile_web_line_str = "WEB_DATA|NEGATIVE|ADD_YN| Have you looked into and read about "
		end_web_line_str = " today/this week ? (Y/N) "
		#rand_shuff_list = random.shuffle(web_history_dict[elem])
		#print "web_length", len(web_history_dict[elem])
		#samp_val = min( [ (len(web_history_dict[elem]) - 1), 4 ] )
		#uniq_random_list = sample(xrange(len(web_history_dict[elem])-1), samp_val)
		total_other_user_web_history_size = len(web_history_dict[elem])
		no_other_web_question = int(total_other_user_web_history_size)/4
		i=0
		while i< no_other_web_question :
			str2=web_history_dict[elem][random.randint(0,len(web_history_dict[elem])-1)]
			if str2 not in elem_dict:
                                elem_dict[str2]=1
				str1 = str(str2).replace('|',' ')
			

				if str1 not in web_temp_dict:
					web_temp_dict[str1]=1
					
					if ((user_name not in web_history_dict or str2 not in web_history_dict[user_name]) and (str1 != "")):
						qs_ans_out_file.write( front_profile_web_line_str + str1 + end_web_line_str + "|" + "N\n" )
						q_count += 1
						i=i+1

	

print "33333333"
web_history_dict_list = []
for elem in web_history_dict:
	web_history_dict_list.append(elem)
#print web_history_dict
if user_name in web_history_dict:
	own_user_visit_url_list = web_history_dict[user_name] 
else:
	own_user_visit_url_list = []
all_users_visit_url_list = []

for elem in web_history_dict:
	for lm in web_history_dict[elem]:
		if lm not in all_users_visit_url_list:
			all_users_visit_url_list.append(lm)

#print own_user_visit_url_list
#print all_users_visit_url_list
def check_url_list( url_str) :
	if url_str in own_user_visit_url_list:
		return "T"
	else:
		return "F"
def have_some_mcq_for_web ( end_str, front_str ):

	mcq_other_list = []
	mcq_ans_str = ""
	q_str = ""
	question_left = 4
	if len(elem_dict) < len(own_user_visit_url_list):
		question_left = 3
		while 1:
			elem = own_user_visit_url_list[random.randint(0,len(own_user_visit_url_list)-1)] 
			elem_new = str(elem) + ";T"
			if elem not in elem_dict:
				elem_dict[elem] = 1
				break
	#print "elem12 ="
	#print elem

	#print "all_users_visit_url_list ="
	#print all_users_visit_url_list	
	#uniq_random_list = sample(xrange(len(all_users_visit_url_list)-1), 3)
        #print uniq_random_list
	i=1
	while i<= question_left:
		while 1:
			elem11 = all_users_visit_url_list[random.randint(0,len(all_users_visit_url_list)-1)]
			res = check_url_list(elem11)
			new_element = str(elem11) + ";" + str(res)
			if elem11 not in elem_dict:
                        	elem_dict[elem11] = 1
                        	break
		new_element = new_element.replace('|', '-')
        	mcq_other_list.append( new_element)
		i=i+1
	if question_left == 3:
       		elem = elem.replace('|', '-') 
		mcq_other_list.append(elem_new)
        random.shuffle(mcq_other_list)
	#print "mcq other list"
        #print mcq_other_list

        j = 0
        for i in [ "A", "B", "C", "D"]:
                cor_option = mcq_other_list[j].strip().split(';')[1]

                if cor_option == "T":
                        mcq_ans_str = mcq_ans_str + i + ","

                q_str = q_str + i + ".  " + mcq_other_list[j].split(';')[0] + ";;"
                j = j + 1

        #print q_str
        line_str = front_str + end_str + q_str + "|" + mcq_ans_str + "\n"
        return line_str


#MCQ based Questions on Profile Visit History
print "len(web_history_dict_list) =" + str(len(web_history_dict_list))
print "len(own_user_visit_url_list = " + str(len(own_user_visit_url_list))
print "len(all_users_visit_url_list) = " + str(len(all_users_visit_url_list))
if ((len(web_history_dict_list) > 0) and (len(own_user_visit_url_list) > 0)): 
	#print "len(all_users_visit_url_list) = " + str(len(all_users_visit_url_list))
	if( (len(all_users_visit_url_list)-1) > 3 ):
		front_profile_str = "WEB_DATA|POS_NEG|UNI_MCQ|Please click the options of the links you visited "
		no_of_possible_q = len(all_users_visit_url_list)/8
		if no_of_possible_q > 0 and no_of_possible_q < 8 :
			no_qu = no_of_possible_q
		elif no_of_possible_q > 0:
			no_qu = 8
		else:
			no_of_possible_q =0
		print "no_of_possible_q = " + str(no_of_possible_q)
		if no_of_possible_q > 0:
			for i in range(no_qu): #No of MCQs
				qs_ans_out_file.write(have_some_mcq_for_web(" today/this week in comma separated way :|", front_profile_str))

qs_ans_out_file.close()
