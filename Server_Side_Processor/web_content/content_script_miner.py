#!/usr/bin/python
import sys
import random
import os
import re
import urllib2
import httplib
import codecs
import time
import types

from random import choice
from math import sqrt
from math import ceil
from os import path
from bs4 import BeautifulSoup


if len(sys.argv) != 3:
        print "\nUsage: <Program_name> <Content_Script_History_File> <User_Name>\n"
        exit(0)
#print "history file " + sys.argv[1]
#print "user name " + sys.argv[2]
if os.path.exists(sys.argv[1]): 
	history_read_handle          = open(sys.argv[1],'r')
	history_read_content         = history_read_handle.readlines()
	history_read_handle.close()

	file_content_dict = {}

	user_name = str(sys.argv[2])

	#Read the content script history file and get the texts and urls
	for line in history_read_content:

		if re.search(r"MAGIC$", line):
			continue;
		if line == "\n":
			continue;
		if( re.search(r"^ref=tn_tnmn", line)):
			continue;
		#if( re.search(r"^bit.ly", line)):
		#	continue;
	
		line_m = line.strip().split(':')
		len_m = len(line_m)
	
		line_str = line_m[0]
		for i in range( len(line_m) - 2):
			line_str = line_str + ":" + line_m[i+1]

		file_content_dict[ line_str ] = line_m[-1:]

	#print file_content_dict

	def character_count(word):
		count = 0
		for c in word:
			count = count + 1
	
		return count

	#Filtering Different Extreme Cases
	filtered_content_dict = {}

	for key in file_content_dict:

		#Removing Special Occurrences
		if ( re.search(r"Photos$", key) or re.search(r"Albums$", key) or re.search(r"Profile Pictures", key) or re.search(r"Recently Added", key) or re.search(r"Mutual Friends$", key) or re.search(r"Mutual$", key) or re.search(r"span", key) or re.search(r"comment", key) or re.search(r"about a minute ago", key) or re.search(r"poke", key) ):
                	#print key
			continue

		#Storing Special String
		if ( re.search(r"^Photos of", key.lstrip()) ):
			filtered_content_dict[key.lstrip().rstrip()] = file_content_dict[key]
			continue

		#Removing Small Character Sequences
		ch_c = character_count(key.lstrip().rstrip())
		if int(ch_c) < 15 :
			continue

		#URL storing
		if ( re.search(r"^http", key.lstrip()) ):
			filtered_content_dict[key.lstrip().rstrip()] = file_content_dict[key]
			continue

		#Div Element
		if ( re.search(r"^HTMLDivElement" , key.lstrip() ) ):
			if ( re.search(r"<" , key ) and re.search(r">" , key ) and re.search(r"=" , key ) and re.search(r"//" , key )):
				continue;
			else:
				l_str = key.strip().split(':')[1].lstrip().rstrip()
				filtered_content_dict[l_str] = file_content_dict[key]
                		continue
		#For Others
		filtered_content_dict[key.lstrip()] = file_content_dict[key]

	facebook_content_dict = {}
	other_url_dict = {}
	text_content_dict = {}

	for key in filtered_content_dict:

		#URL Processeing
		if( re.search(r"^http", key)):
			if( re.search(r"www.facebook.com", key)):
				if ( re.search(r"\.php", key)):
					continue
				elif ( re.search(r"\.[0-9]+\.", key)):
					continue
				else:
					facebook_content_dict[key] = filtered_content_dict[key]
			else: # For other URL
				other_url_dict[key] = filtered_content_dict[key]

		elif ( re.search(r"[0-9]+\.[A-Z0-9a-z]+", key) ): # Some Noise Processing
			#print key
			continue 
		else: #Not URL
			if key not in text_content_dict:
				text_content_dict[key] = [ filtered_content_dict[key], 1 ]
			else:
				text_content_dict[key] = [ filtered_content_dict[key], int(text_content_dict[key][1]) + 1 ]

		#print key, ":" , filtered_content_dict[key]

	#print facebook_content_dict
	#print other_url_dict
	#print text_content_dict

	other_url_semantics_dict = {}
	user_dict = {}

	#Process URLS
	#1 Remove URLS containing facebook.com
	#2 Handle URLS of the form http[s]://[www.]XX.YY[/] separately
	#3 Send the rest URLS to the next step

	#Use GetLinkInfo on the URLS of other_url_dict
	count = 0
	#print "other url dict"
	#print other_url_dict
	for url_str in other_url_dict:
		#http://www.getlinkinfo.com/info?link=http%3A%2F%2Fyoutube.com%2Fwatch?v=bvunndTmY4cZ&x=70&y=18
		new_url_str = "http://www.getlinkinfo.com/info?link=" + url_str.replace(":","%3A").replace("/","%2F") + "&x=70&y=18"
		#print new_url_str
		count += 1

		#Getting The page containing the semantics of the URL
		#print "Content: "+url_str
		url1 = url_str.split(':')
		url2 = url1[0]+":"+url1[1]
		#print url2
		time_hour = url_str.strip().split(' @ ')[1].split(':')[0]
                time_date = url_str.strip().split(' @ ')[0].split(':')[-1]
                time_in_milliseconds_from_epoch = time_hour+"/"+time_date
		try:
                        usock = urllib2.urlopen(url2)
                except:
                        continue
		data = usock.read()
                usock.close()
                tmp_url_data = open("tmp_url.txt",'w')
                tmp_url_data.write(data)
                tmp_url_data.close()
                tmp_url_data = open("tmp_url.txt",'r')
                content = tmp_url_data.readlines()
                nl2 = ""
                for line1 in content:
                        if re.search(r"<title",line1):
                                nl1=line1.strip().split('</title>')[0]
                                nl2=nl1.split('<title')[1]
                                if re.search(r'>',nl2):
                                        nl2=nl2.split('>')[1]
                tmp_url_data.close()
                command_str = "rm tmp_url.txt"
                os.system(command_str)	
		if (nl2 == "Facebook") :
                        continue
                elif (nl2 == "Gmail") :
                        continue
                elif (nl2 == "YouTube") :
                        continue
                elif (nl2 == "Twitter") :
                        continue

                else:
                        if (nl2 != ""):
                                if nl2 not in user_dict:
                                        user_dict[nl2] = time_in_milliseconds_from_epoch
                                else:
                                        time = user_dict[nl2]
                                        if time_in_milliseconds_from_epoch > time:
                                                user_dict[nl2] = time_in_milliseconds_from_epoch

	history_write_handle          = open( "/home/cnerg/Activpass/Server/www/activPass_core/data_base/web_content_data/processed/content_script/" + user_name + "_content_script_for_fb_question_feeder.txt",'w')
	for key in user_dict:
        	str1= key+"||"+user_dict[key]+"\n"
        	history_write_handle.write(str1)

	history_write_handle.close()
	#print "Success"


"""
		try:
			temp_page = urllib2.urlopen(new_url_str)
			main_page = temp_page.read()
	
			#File Writing
			dir_path_name = "./page_dir/"

			try:
				os.makedirs(dir_path_name)
			except OSError:
				if not os.path.isdir(dir_path_name):
					raise
		
			fname = dir_path_name + "/url_" + str(url_str).strip().split("/")[2] + ".txt"
			fhandle = open(fname, 'w')
			fhandle.write(main_page)
			fhandle.close()
			os.system("dos2unix " + fname)
			#introduce delay        
			time.sleep(0.0001)

		except urllib2.URLError,error:
			print "While Retrieving Page " + str(new_url_str) + ", got URL Error."
		except httplib.BadStatusLine:
			print "Could not fetch %s due to BAD STATUS" % new_url_str

		soup_page = BeautifulSoup(main_page)
	
		#print soup_page.find(text="Title").findNext('dd').contents[0]
		#print soup_page.find("dt", { "class" : "link-title" }).findNext('dd').find('b').contents[0]
		if isinstance(soup_page.find("dt", { "class" : "link-title" }), types.NoneType ) == True:
			format_semantic_str = "(none)"
		else:
			format_semantic_str = soup_page.find("dt", { "class" : "link-title" }).findNext('dd').contents[0]
		#print format_semantic_str

		if ( str(format_semantic_str) == "(none)" ) :
			other_url_semantics_dict[url_str] = other_url_dict[url_str]
		else:
			other_url_semantics_dict[str(format_semantic_str).replace("<b>","").replace("</b>","")] = other_url_dict[url_str]

	#print other_url_semantics_dict
	#print text_content_dict

	sorted_based_on_time_other_url_descending = sorted(other_url_semantics_dict.items(), key=lambda x: x[1], reverse = True)
	sorted_based_on_time_text_content_descending = sorted(text_content_dict.items(), key=lambda x: x[1][0], reverse = True)

	#print sorted_based_on_time_other_url_descending
	#print sorted_based_on_time_text_content_descending

	#Now form a Question Feeder file from it

	for item in sorted_based_on_time_other_url_descending:
		#print item
		line_fields = line.strip().split(':')
		line_str = line_fields[0]+line_fields[1] + "||" + item[1][0] + "|| 1\n"
		#print line_str
		history_write_handle.write(line_str)

	for item in sorted_based_on_time_text_content_descending:
		print item
		line_str = item[0] + "||" + str(item[1][0][0]) + "||" + str(item[1][1]) + "\n"
		history_write_handle.write(line_str)

		#print item[0]
		#print item[1][0]	
	#print filtered_content_dict
        """


