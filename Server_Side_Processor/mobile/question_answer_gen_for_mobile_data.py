#!/usr/bin/python
import sys
import random
import os
import re
from random import choice
from math import sqrt
from math import ceil
from os import listdir
from os.path import isfile, join
from random import sample
import urllib2
import httplib
import codecs
import time
import types
from datetime import datetime

from bs4 import BeautifulSoup

if len(sys.argv) != 5:
        print "\nUsage: <Program_name> <Mobile_Data_Dir_Path> <Other_Configuration_File> <Date> <User_Name>\n"
        exit(0)

dir_path   = str(sys.argv[1])
today_date = str(sys.argv[3])
user_name  = str(sys.argv[4])

audio_read_content = []
video_read_content = []
raw_contact_read_content = []
contact_read_content = []
call_read_content = []
image_read_content = []
sms_inbox_read_content = []
sms_sent_read_content = []

#Other Generated Files
call_outlier_read_content = []
frequent_read_content = []
config_read_content = []

config_read_handle          		= open( str(sys.argv[2]),'r')
config_read_content           		= config_read_handle.readlines()
config_read_handle.close()

in_thres = 0
out_thres = 0

for line in config_read_content:
	l_str_list = line.strip().split(':')

	if l_str_list[0] == "incoming":
		in_thres = int(l_str_list[1])
	elif l_str_list[0] == "outgoing":
		out_thres = int(l_str_list[1])

onlyfiles = [ f for f in listdir(dir_path) if isfile(join(dir_path,f)) ]
#print onlyfiles

for f_name in onlyfiles:

        file_type = f_name.strip().split('-')[0]
        f_path = dir_path + "/" + f_name
#	print file_type

	if re.search("audio", str(file_type) ):
        	audio_read_handle          		= open(f_path,'r')
        	audio_read_content           		= audio_read_handle.readlines()
		#print audio_read_content
        	audio_read_handle.close()

	elif re.search("video", str(file_type) ):
        	history_read_handle          		= open(f_path,'r')
        	video_read_content           		= history_read_handle.readlines()
        	history_read_handle.close()
	
	elif re.search("raw_contact", str(file_type) ):
        	history_read_handle                	= open(f_path,'r')
        	raw_contact_read_content           	= history_read_handle.readlines()
        	history_read_handle.close()

	elif re.search("contact", str(file_type) ):
        	history_read_handle            		= open(f_path,'r')
        	contact_read_content           		= history_read_handle.readlines()
        	history_read_handle.close()

	elif re.search("call", str(file_type) ):
		b = os.path.getsize(f_path)
		if b == 0:
			sys.exit()
        	history_read_handle                	= open(f_path,'r')
        	call_read_content           		= history_read_handle.readlines()
        	history_read_handle.close()

	elif re.search("image", str(file_type) ):
        	history_read_handle                	= open(f_path,'r')
        	image_read_content           		= history_read_handle.readlines()
        	history_read_handle.close()

	elif re.search("sms_inbox", str(file_type) ):
        	history_read_handle                	= open(f_path,'r')
        	sms_inbox_read_content           	= history_read_handle.readlines()
        	history_read_handle.close()

	elif re.search("sms_sent", str(file_type) ):
		#print "sms_sent", file_type
        	history_read_handle                	= open(f_path,'r')
        	sms_sent_read_content           	= history_read_handle.readlines()
        	history_read_handle.close()

	elif re.search("outlier", str(file_type) ):
		#print "sms_sent", file_type
        	history_read_handle                	= open(f_path,'r')
        	call_outlier_read_content           	= history_read_handle.readlines()
        	history_read_handle.close()

	elif re.search("frequent", str(file_type) ):
		#print "sms_sent", file_type
        	history_read_handle                	= open(f_path,'r')
        	frequent_read_content           	= history_read_handle.readlines()
        	history_read_handle.close()

	else:
		print "File Type = " + str(file_type)
		print "Something Terribly Wrong : Please check your Code .."

print "hi"
#Putting the files info into appropriate data structures
audio_info_dict = {}
ringtone_audio_dict = {}
ringtone_audio_title = ""
ringtone_audio_date = ""

#print audio_read_content
for line in audio_read_content:
	l_str_lst = line.strip().split(';')
	#print l_str_lst	
	audio_title = l_str_lst[0].strip().split(':')[-1]
	
	#print l_str_lst[1]	
	date_added_0 = l_str_lst[1].strip().split(':')[0] #date some issue
	if date_added_0 == "date_added":
		date_added = l_str_lst[1].strip().split(':')[1] #date some issue
		audio_info_dict[ audio_title ] =  date_added 
	else:
		date_added = l_str_lst[2].strip().split(':')[1] #date some issue
                audio_info_dict[ audio_title ] =  date_added

	if re.search("ring_tone", l_str_lst[-1] ):
		ringtone_audio_title = audio_title
		ringtone_audio_date = date_added
		ringtone_audio_dict[ringtone_audio_title] = date_added
		del audio_info_dict[audio_title]  #Ringtone song deleted from dictionary

#for key in audio_info_dict:
#	print key

#Sorting Needs to be done after date issue fixed
qs_ans_out_file_name             = user_name + "_question_answer_mobile.log"
qs_ans_out_file                  = open( "/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + qs_ans_out_file_name, 'w');

#Audio Data based Questions

#Question Generation from Audio Data
#Have you visited Qs
def have_you_added( time_str ):

	q_count = 0
        for elem in audio_info_dict: #It should be reverse sorted list according to time
                qs_ans_out_file.write("AUDIO_DATA|POSITIVE|ADD_YN_T|" + "Have you added music titled " + elem + " " + time_str + " ? (Y/N)" + "|" + "Y" + "\n");
		q_count += 1

		if q_count >= 5:
			break

	q_count = 0
	for elem in audio_info_dict:
		qs_ans_out_file.write("AUDIO_DATA|POSITIVE|ADD_YN_D|" + "Have you added music titled " + elem + " at " + audio_info_dict[elem].strip().split('_')[0] + " ? (Y/N)" + "|" + "Y" + "\n");
		
		q_count += 1
		if q_count >= 10: #Ten Questions from actual date
			break	

	#NEGATIVE Questions
        #for elem in others_audio_info_dict:
        #       qs_ans_out_file.write("AUDIO_DATA|NEGATIVE|ADD_YN|" + "Have you added music titled  " + elem + " " + time_str + "'s profile ? (Y/N)" + "|" + "Y" + "\n");      

#have_you_added("today/this week")

def ret_key_at_pos( pos_n, a_dict ):
	
	count = 1

	for key in a_dict:
		if count == pos_n:
			return key

		count = count + 1
		if count > pos_n:
			break
	return "NAN"

#Single MCQ
def have_some_mcq ( time_str, front_str ):

	global ringtone_audio_title        
        mcq_ans_str = ""
        q_str =""
	r = random.randint(0,1)
	#print "r=" + str(r)
	if(len(ringtone_audio_dict) == 1 and r == 0):	
		uniq_random_list = sample(xrange(len(audio_info_dict)-1), 3)
		mcq_list = []

		for i in uniq_random_list:
			ret_val = ret_key_at_pos( i, audio_info_dict )
			mcq_list.append(ret_val)

		mcq_list.append(ringtone_audio_title)
	else:
		uniq_random_list = sample(xrange(len(audio_info_dict)-1), 4)
		mcq_list = []

		for i in uniq_random_list:
			ret_val = ret_key_at_pos( i, audio_info_dict )
			mcq_list.append(ret_val)
		
	random.shuffle(mcq_list)

	j = 0
        for i in [ "A", "B", "C", "D"]:

		if ringtone_audio_title == mcq_list[j]:
			mcq_ans_str = i

                q_str = q_str + i + ".  " + mcq_list[j] + ";;"
		j = j + 1
        
	#print q_str
        line_str = front_str + q_str + "|" + mcq_ans_str + "\n"
        return line_str

#MCQ based Questions on Profile Visit History
front_profile_str = "AUDIO_DATA|POS_NEG|UNI_MCQ_R|Please write the option of your Ringtone (Please don't write any option if all are wrong):|" 
if (len(audio_info_dict) >= 5):
	for i in range(5): #No of Ringtone MCQs
        	qs_ans_out_file.write(have_some_mcq("today/this week", front_profile_str))

#Multi MCQ

def give_some_hint_in_text( ans_txt ):
	txt_len = 0
	for ch in ans_txt:
		txt_len = txt_len + 1
	
	uniq_random_list = sorted(sample(xrange(txt_len), int(txt_len*2/3)))

	new_hint_text = ""
	cnt = 0
	j = 0
	new_ans_txt = ""

	for ch in ans_txt:
		new_ch = ''
		if cnt == uniq_random_list[j] :
			new_ch = "*"
			j = j + 1
		else:
			new_ch = ch
		
		new_ans_txt = new_ans_txt + new_ch
		cnt = cnt+1
		
		if j >= len(uniq_random_list) :
			break

	return new_ans_txt

#Text Based Question
if (len(audio_info_dict) >= 5) and (len(ringtone_audio_dict) == 1):
	qs_ans_out_file.write( "AUDIO_DATA|POSITIVE|UNI_TX_R|Please write the song name of your Ringtone: |" + ringtone_audio_title + "\n")
	qs_ans_out_file.write( "AUDIO_DATA|POSITIVE|UNI_TX_H_R|Please write the song name of your Ringtone: (Hint-> " + give_some_hint_in_text( ringtone_audio_title ) + ") |" + ringtone_audio_title + "\n")

contact_name_number_dict = {}
contact_number_name_dict = {}
contact_info_dict = {}

def check_show_name( sh_list ):
	for elem in sh_list:
		if re.search(r"\w+@\w+\.\w+", str(elem)):
			if re.search("gmail", str(elem) ) :
				return "Google"
			elif re.search("facebook", str(elem) ) :
				return "Facebook"
			else:
				return "Other"

		elif re.search(r"http", str(elem)):
			if re.search("google", str(elem) ) :
				return "Google"
			elif re.search("facebook", str(elem) ) :
				return "Facebook"
			else:
				return "Other"

		elif str(elem).replace('+','').isalnum():
			return "Phone"

	return "Other"
			
			
for line in contact_read_content:
	l_str_list = line.strip().split(';')
	
	contact_name = ""
	contact_number = ""
	email_contact = ""
	job_contact_number = ""
	show_name_list = []
	contact_source = "No"

	for elem in l_str_list:
		if re.search( "nm:", elem ) :
			contact_name = elem.strip().split(':')[1]
		elif re.search( "hm:", elem ) or re.search( "pm", elem ) or re.search( "wm", elem ):
			#print contact_number
			contact_number = elem.strip().split(':')[1]
		elif re.search( "he:", elem ) or re.search( "oe", elem ) or re.search( "we", elem ):
			email_contact = elem.strip().split(':')[1]
		elif re.search( "jt:", elem ):
			job_contact_number = elem.strip().split(':')[1]
		elif re.search( "on:", elem ):
			#print elem
			if elem != "":
				show_name_list.append(elem.strip().split(':')[1])

	contact_source = check_show_name(show_name_list)
	
	if contact_number != "" and contact_name != "" and contact_source == "Phone" :
		contact_name_number_dict[ contact_name ] = contact_number
		contact_number_name_dict[ contact_number ] = contact_name

	if contact_name not in contact_info_dict:
		 contact_info_dict[ contact_name ] = [ str(contact_number), str(email_contact), str(job_contact_number), show_name_list, str(contact_source) ]
	else:
		#print "Interestingly Here", contact_name
		contact_info_dict[ contact_name ].append( [ str(contact_number), str(email_contact), str(job_contact_number), show_name_list, str(contact_source) ] ) 


#print contact_name_number_dict
#print contact_number_name_dict
#print contact_info_dict
incoming_call_dict = {}
outgoing_call_dict = {}
missed_call_dict = {}
rejected_call_dict = {}
all_call_info_dict = {}
call_total_freq_time_dict = {}

#Universal_Info
first_caller_of_the_day = ""
last_caller_of_the_day = ""

#most_frequent_callers_of_the_day = []
#most_rejected_callers_of_the_day = []
most_frequent_callers_of_the_week = {}
most_rejected_callers_of_the_week = {}
most_frequent_callers_of_the_week_list = []
most_rejected_callers_of_the_week_list = []

#Duration Based
highest_duration_incoming_call_week = 0
highest_duration_incoming_caller_week = ""
highest_duration_outgoing_call_week = 0
highest_duration_outgoing_caller_week = ""
highest_number_incoming_caller_week = ""
highest_number_outgoing_caller_week = ""

highest_duration_incoming_call_day = 0
highest_duration_incoming_caller_day = ""
highest_duration_outgoing_call_day = 0
highest_duration_outgoing_caller_day = ""
highest_number_incoming_caller_day = ""
highest_number_outgoing_caller_day = ""

#total_number_of_calls_of_the_day = ""
number_of_rejected_calls_of_the_day = 0
#number_of_outgoing_calls_of_the_day = ""
#last_call_duration = ""
#total_call_duration_of_the_day = ""
last_call_receive_time = ""
first_call_receive_time = ""

freq_call_info_dict = {}

for line in frequent_read_content:
	#print "line   =  " + line
	l_str_lst = line.strip().split(';')
	
	call_name = ""
        call_date = ""
        call_type = ""
        call_duration = ""
	
	for elem in l_str_lst:
		if re.search("op:", elem) :	
			call_name = elem.strip().split(':')[1]
		elif re.search("st:", elem) :
			call_date = elem.strip().split(':')[1]
		elif re.search("duration:", elem) :
			call_duration = elem.strip().split(':')[1]
		elif re.search("ct:", elem) :
			call_type = elem.strip().split(':')[1]
			if str(call_type) != "":
				call_type = elem.strip().split(':')[1]
			else:
				call_type = "Rejected"
		else:
			continue

	if call_name not in freq_call_info_dict:
		freq_call_info_dict[ call_name ] = [ call_date, int(call_duration), [call_type], 0]
	else:
		freq_call_info_dict[ call_name ][1] = int(freq_call_info_dict[ call_name ][1]) + int(call_duration)
		freq_call_info_dict[ call_name ][2].append(call_type)
		freq_call_info_dict[ call_name ][3] = int(freq_call_info_dict[ call_name ][3]) + 1

	#print "call_type =" + call_type

	if ( call_type == "Incoming" or call_type == "Outgoing" ) :
		if ( call_name not in most_frequent_callers_of_the_week ) :
			most_frequent_callers_of_the_week[ call_name ] = 1
		else:
			most_frequent_callers_of_the_week[ call_name ] = int(most_frequent_callers_of_the_week[ call_name ]) + 1
	
	if ( call_type == "Rejected" ) :
		if (call_name not in most_rejected_callers_of_the_week):
			most_rejected_callers_of_the_week[ call_name ] = 1
		else:
			most_rejected_callers_of_the_week[ call_name ] = int(most_rejected_callers_of_the_week[ call_name ]) + 1
	

#print freq_call_info_dict
#print most_frequent_callers_of_the_week
#print most_rejected_callers_of_the_week
#print most_frequent_callers_of_the_week_list
#print most_rejected_callers_of_the_week_list

most_frequent_callers_of_the_week_list = sorted(most_frequent_callers_of_the_week.items(), key=lambda x: x[1], reverse = True)
most_rejected_callers_of_the_week_list = sorted(most_rejected_callers_of_the_week.items(), key=lambda x: x[1], reverse = True)

incoming_call_name_dict = {}
outgoing_call_name_dict = {}

incoming_call_name_dict_day = {}
outgoing_call_name_dict_day = {}

caller_name_time_dict_day = {}
all_call_time_caller_dict = {}

def modify_date_format( date_str ):
	date_m = date_str.strip().split('_')[0]
	time_m = date_str.strip().split('_')[1]

	date_day = date_m.split('/')[0]
	date_month = date_m.split('/')[1]
	date_year = date_m.split('/')[2]

	new_date = str(date_year) + "/" + str(date_month) + "/" + str(date_day) + "_" + str(time_m)
	#print new_date
	return new_date

for line in call_read_content:
	l_str_lst = line.strip().split(';')

	call_name = ""
	call_number = ""
	call_date = ""
	call_type = ""
	call_duration = ""
	call_act_date = ""


	for elem in l_str_lst:
		if not(re.search("null", elem)) and re.search("num:", elem) :
			call_number = elem.strip().split(':')[1]
		elif re.search("name:", elem) :	
			call_name = elem.strip().split(':')[1]
		elif re.search("date:", elem) :
			call_date = elem.strip().split(':')[1]
			call_act_date = call_date.strip().split('_')[0]
		elif re.search("duration:", elem) :
			call_duration = elem.strip().split(':')[1]
		elif re.search("callType:", elem) :
			call_type = elem.strip().split(':')[1]
			if str(call_type) != "":
				call_type = elem.strip().split(':')[1]
			else:
				call_type = "Rejected"
		else:
			continue
	
	#print call_act_date
	#Initial Putting
	if call_name not in all_call_info_dict:
		all_call_time_caller_dict[ modify_date_format(call_date) ] = call_name

		if call_name != '':
			call_total_freq_time_dict[ call_name ] = [ 1 , call_duration ]
			all_call_info_dict[ call_name ] = [ [ call_name, call_number, call_date, call_duration] ]
	else:
		all_call_info_dict[ call_name ].append( [ call_name, call_number, call_date, call_duration] )
		call_total_freq_time_dict[ call_name ][0] = int(call_total_freq_time_dict[ call_name ][0]) + 1
		call_total_freq_time_dict[ call_name ][1] = int(call_total_freq_time_dict[ call_name ][1]) + int(call_duration)
		all_call_time_caller_dict[ modify_date_format(call_date) ] = call_name
	#print "today_date = "+today_date + "Call_act_date = " + call_act_date

	if ( str(today_date) == str(call_act_date) ):
#		print "Populating  caller_name_time_dict_day"
		if call_date.strip().split('_')[1] not in caller_name_time_dict_day:
			caller_name_time_dict_day[call_date.strip().split('_')[1]] = call_name
	#Other putting
	#print "call_type =" + str(call_type)
	if call_type == "Missed":
		dict_key = str(call_name) + "_" + str(call_date)

		if dict_key not in missed_call_dict:
			if call_name != '':
				missed_call_dict[ dict_key ] = [ call_name, call_number, call_date ]
		else:
			print "M:Why_o_Why"

	elif call_type == "Incoming":
		dict_key = str(call_name) + "_" + str(call_date)

		if dict_key not in incoming_call_dict:
			if call_name != '':
				if int(call_duration) >= highest_duration_incoming_call_week:
					highest_duration_incoming_call_week   = int(call_duration)
					highest_duration_incoming_caller_week = str(call_name)

				if (int(call_duration) >= highest_duration_incoming_call_day) and ( str(today_date) == str(call_act_date) ):
					highest_duration_incoming_call_day   = int(call_duration)
					highest_duration_incoming_caller_day = str(call_name)

				incoming_call_dict[ dict_key ] = [ call_name, call_number, call_date, call_duration ]
		else:
			print "I:Why_o_Why"

		if str(call_name) not in incoming_call_name_dict:
			incoming_call_name_dict[ call_name ] = 1
		else:
			incoming_call_name_dict[ call_name ] += 1
	
		if ( str(today_date) == str(call_act_date) ):
			if str(call_name) not in incoming_call_name_dict_day:
				incoming_call_name_dict_day[ call_name ] = 1
			else:
				incoming_call_name_dict_day[ call_name ] += 1

	elif call_type == "Outgoing":
		dict_key = str(call_name) + "_" + str(call_date)

		if dict_key not in outgoing_call_dict:
			if call_name != '':
				if int(call_duration) >= highest_duration_outgoing_call_week:
					highest_duration_outgoing_call_week   = int(call_duration)
					highest_duration_outgoing_caller_week = str(call_name)

				if (int(call_duration) >= highest_duration_outgoing_call_day) and ( str(today_date) == str(call_act_date) ):
					highest_duration_outgoing_call_day   = int(call_duration)
					highest_duration_outgoing_caller_day = str(call_name)

				outgoing_call_dict[ dict_key ] = [ call_name, call_number, call_date, call_duration ]
		else:
			print "O:Why_o_Why"
		
		if str(call_name) not in outgoing_call_name_dict:
			outgoing_call_name_dict[ call_name ] = 1
		else:
			outgoing_call_name_dict[ call_name ] += 1
		
		if ( str(today_date) == str(call_act_date) ):
			if str(call_name) not in outgoing_call_name_dict_day:
				outgoing_call_name_dict_day[ call_name ] = 1
			else:
				outgoing_call_name_dict_day[ call_name ] += 1


	elif call_type == "":
		dict_key = str(call_name) + "_" + str(call_date)

		if dict_key not in rejected_call_dict:
			if call_name != '':
				rejected_call_dict[ dict_key ] = [ call_name, call_number, call_date, call_duration ]
				number_of_rejected_calls_of_the_day += 1
		#else:
		#	print "O:Why_o_Why"

	elif call_type == "Rejected":
		dict_key = str(call_name) + "_" + str(call_date)

		if dict_key not in rejected_call_dict:
			if call_name != '':
				rejected_call_dict[ dict_key ] = [ call_name, call_number, call_date, call_duration ]
				number_of_rejected_calls_of_the_day += 1
	else:
		#print call_type
		print "Danger : Not allowed here"

call_timewise_sorted_list = sorted(all_call_time_caller_dict)
#print call_timewise_sorted_list
 
def create_time_range_other( call_time_str ):
	hr_time = call_time_str.strip().split('/')[0]
	
	am_pm = ""
	ac_tm = ""

	if int(hr_time) > 12 :
		am_pm = " pm"
		ac_tm = int(hr_time) - 12
	else:
		am_pm = " am"
		ac_tm = int(hr_time)

	return str(ac_tm) + str(am_pm)

incoming_call_name_week_list = sorted(incoming_call_name_dict.items(), key=lambda x: x[1], reverse = True)
outgoing_call_name_week_list = sorted(incoming_call_name_dict.items(), key=lambda x: x[1], reverse = True)
incoming_call_name_day_list = sorted(incoming_call_name_dict_day.items(), key=lambda x: x[1], reverse = True)
outgoing_call_name_day_list = sorted(outgoing_call_name_dict_day.items(), key=lambda x: x[1], reverse = True)
caller_name_time_dict_day_list = sorted(caller_name_time_dict_day.items())

#print caller_name_time_dict_day

#if ( len(caller_name_time_dict_day_list) > 0 ) :
#print caller_name_time_dict_day_list
if len(caller_name_time_dict_day_list) > 0:
	first_call_receive_time = create_time_range_other(caller_name_time_dict_day_list[0][0])
	last_call_receive_time = create_time_range_other(caller_name_time_dict_day_list[-1][0])

	#print caller_name_time_dict_day_list
	first_caller_of_the_day = caller_name_time_dict_day_list[0][1]
	last_caller_of_the_day = caller_name_time_dict_day_list[-1][1]

#print first_call_receive_time, first_caller_of_the_day, last_call_receive_time, last_caller_of_the_day
#print incoming_call_name_list
#print outgoing_call_name_list
#print incoming_call_name_dict_day
#print outgoing_call_name_dict_day

highest_number_incoming_caller_week = incoming_call_name_week_list[0][0]
highest_number_outgoing_caller_week = outgoing_call_name_week_list[0][0]
if ( len(incoming_call_name_day_list) > 0 ):
	highest_number_incoming_caller_day = incoming_call_name_day_list[0][0]
if ( len(highest_number_outgoing_caller_day) > 0):
	highest_number_outgoing_caller_day = outgoing_call_name_day_list[0][0]

#print "***********************************"
#print highest_duration_incoming_call_week
#print highest_duration_incoming_caller_week
#print highest_duration_outgoing_call_week
#print highest_duration_outgoing_caller_week
#print highest_number_incoming_caller_week
#print highest_number_outgoing_caller_week
#print "***********************************"
#print highest_duration_incoming_call_day
#print highest_duration_incoming_caller_day
#print highest_duration_outgoing_call_day
#print highest_duration_outgoing_caller_day
#print highest_number_incoming_caller_day
#print highest_number_outgoing_caller_day
#print "***********************************"
#print missed_call_dict
#print incoming_call_dict
#print outgoing_call_dict
#print rejected_call_dict
#print call_total_freq_time_dict

outlier_call_dict = {}

call_related_questions_dict = {}
call_related_negative_questions_dict = {}

for line in call_outlier_read_content:
	
	l_str_list = line.strip().split(';')

	out_person = ""
	call_type = ""
	call_time = ""
	call_duration = ""

	#print l_str_list
	for elem in l_str_list:
		if re.search("op:", elem) :
			out_person = elem.strip().split(':')[1]
		elif re.search("ct:", elem) :	
			call_type = elem.strip().split(':')[1]
			if str(call_type) != "":
				call_type = elem.strip().split(':')[1]
			else:
				call_type = "Rejected"
		elif re.search("st:", elem) :
			call_time = elem.strip().split(':')[1]
		elif re.search("duration:", elem) :
			call_duration = elem.strip().split(':')[1]
	
	outlier_call_dict [ out_person ] = [ str(call_type), str(call_time), str(call_duration) ]	

#print outlier_call_dict

def create_time_range( call_time_str ):
	hr_time = call_time_str.strip().split('/')[0]
	
	am_pm = ""
	ac_tm = ""

	if int(hr_time) > 12 :
		am_pm = " pm"
		ac_tm = int(hr_time) - 12
	else:
		am_pm = " am"
		ac_tm = int(hr_time)

	return str(ac_tm) + str(am_pm) + " +- 1hr "

def rand_am_pm( nm ):
	if int(nm) == 1:
		return "am"
	else:
		return "pm"

def rand_call( nm):
	if int(nm) == 1:
		return "called "
	elif int(nm) == 2:
		return "received a call from "
	else:
		return "rejected a call from "
		
def create_rand_time_range ( call_time_str ):
	hr_time = call_time_str.strip().split('/')[0]

	ac_tm = int(hr_time)

	if int(hr_time) > 12 :
		ac_tm = int(hr_time) - 12

	ac_tm = ac_tm + random.randint(3,7)

	if int(ac_tm) > 12 :
                ac_tm = int(ac_tm) - 12

	#print hr_time,ac_tm

	return str(ac_tm) + " " + str( rand_am_pm(random.randint(1,2)) ) + " +- 1hr "


def check_time_diff( date1_str, date2_str ):

        d1 = datetime.strptime( date1_str , "%Y/%m/%d_%H/%M/%S")
        d2 = datetime.strptime( date2_str , "%Y/%m/%d_%H/%M/%S")

        if ( abs((d1-d2).seconds) >= 3600 ): #1 Hour gap for remembering purpose
                return True
        else:
                return False

def is_this_call_isolated( call_time_str ):
	new_date = modify_date_format( call_time_str )
	indx =  call_timewise_sorted_list.index( new_date ) 
	
	indx_prev = 0
	indx_nxt = 0

	if indx != 0 and indx != (len(call_timewise_sorted_list) - 1) :
		indx_prev = indx - 1
		indx_nxt = indx + 1
		
		if check_time_diff( call_timewise_sorted_list[indx_prev] , call_timewise_sorted_list[indx] ) and check_time_diff( call_timewise_sorted_list[indx_nxt] , call_timewise_sorted_list[indx] ):
			return True
	elif indx == 0:
		if check_time_diff( call_timewise_sorted_list[1] , call_timewise_sorted_list[0] ):
			return True
	else:
		indx_prev = indx - 1
		if check_time_diff( call_timewise_sorted_list[indx_prev] , call_timewise_sorted_list[indx] ):
			return True
	return False


def convert_to_am_pm( tm_str ):
	if tm_str < 12:
		return str(tm_str) + " am"
	else:
		return str( tm_str - 12 ) + " pm"

size_of_outlier_call_dict = len(outlier_call_dict)
no_of_questions_from_have_you_called = int(size_of_outlier_call_dict)/3
if no_of_questions_from_have_you_called < 1 and size_of_outlier_call_dict >=1:
	no_of_questions_from_have_you_called =1

#print "outlier_call_dict ="
#print outlier_call_dict
#print "contact_name_number_dict"
#print contact_name_number_dict
def have_you_called( time_str ):

	i=0
        for elem in outlier_call_dict: #It should be reverse sorted list according to time
		if elem in contact_name_number_dict and elem not in call_related_questions_dict:
			call_related_questions_dict[elem] = 1
			
			#print "mobile questions: elem = " + str(elem)
			date_m = outlier_call_dict[elem][1].strip().split('_')[0]
			date_hr = outlier_call_dict[elem][1].strip().split('_')[1].split('/')[0]
			date_am_pm = convert_to_am_pm( int(date_hr) )

			if outlier_call_dict[elem][0] == "Outgoing":
				#if is_this_call_isolated( outlier_call_dict[elem][1] ) and  int(outlier_call_dict[elem][2]) > int(out_thres) :				
				if int(outlier_call_dict[elem][2]) > int(out_thres) :				
					r = random.randint(0,1)
					if (r == 0):
                				qs_ans_out_file.write("CALL_DATA|POSITIVE|ADD_YN_C_P|" + "Have you called " + elem + " at around " + date_am_pm + " on " + str(date_m) +" ? (Y/N)" + "|" + "Y" + "\n");
					#print "Hint question generation " + str(elem)
					else:
                				qs_ans_out_file.write("CALL_DATA|POSITIVE|UNI_TX_H_C_P|" + "Whom did you call at around " + date_am_pm + " on " + str(date_m) + " ? Hint: (" + give_some_hint_in_text(elem) + " ) |" + str(elem) + "\n");
			elif outlier_call_dict[elem][0] == "Incoming":
				if is_this_call_isolated( outlier_call_dict[elem][1] ) and  int(outlier_call_dict[elem][2]) > int(in_thres) :				
					r = random.randint(0,1)
					if (r == 0):
                				qs_ans_out_file.write("CALL_DATA|POSITIVE|ADD_YN_R_P|" + "Have you received a call from " + elem + " at around " + date_am_pm + " on " + str(date_m) + " ? (Y/N)" + "|" + "Y" + "\n");
					else:
                				qs_ans_out_file.write("CALL_DATA|POSITIVE|UNI_TX_H_C_P|" + "From Whom did you receive call at around " + date_am_pm + " on " + str(date_m) + " ? Hint: (" + give_some_hint_in_text(elem) + " ) |" + str(elem) + "\n");
			elif outlier_call_dict[elem][0] == "Rejected":
                		qs_ans_out_file.write("CALL_DATA|POSITIVE|ADD_YN_J_P|" + "Have you rejected a call from " + elem + " at around " + date_am_pm + " on " + str(date_m) + " ? (Y/N)" + "|" + "Y" + "\n");
			i=i+1
			if i > int(no_of_questions_from_have_you_called)/2:
				break
        
	q_count = 0
        for elem in outlier_call_dict:
		if elem in contact_name_number_dict and elem not in call_related_questions_dict:
			call_related_questions_dict[elem] = 1

			if outlier_call_dict[elem][0] == "Outgoing":
				r = 0
				if is_this_call_isolated( outlier_call_dict[elem][1] ) and  int(outlier_call_dict[elem][2]) > int(out_thres) :				
                			qs_ans_out_file.write("CALL_DATA|POSITIVE|ADD_YN_C_T|" + "Have you called " + elem + " on " + outlier_call_dict[elem][1].strip().split('_')[0] + " at around " + create_time_range(outlier_call_dict[elem][1].strip().split('_')[1]) + " ? (Y/N)" + "|" + "Y" + "\n");
					r = 1
				if r == 0:
                			qs_ans_out_file.write("CALL_DATA|NEGATIVE|ADD_YN_C_T|" + "Have you called " + elem + " on " + outlier_call_dict[elem][1].strip().split('_')[0] + " at around " + create_rand_time_range(outlier_call_dict[elem][1].strip().split('_')[1]) + " ? (Y/N)" + "|" + "N" + "\n");


			elif outlier_call_dict[elem][0] == "Incoming":
				r = 0
				if is_this_call_isolated( outlier_call_dict[elem][1] ) and  int(outlier_call_dict[elem][2]) > int(in_thres) :				
                			qs_ans_out_file.write("CALL_DATA|POSITIVE|ADD_YN_R_T|" + "Have you received a call from " + elem + " on " + outlier_call_dict[elem][1].strip().split('_')[0] + " at around " + create_time_range(outlier_call_dict[elem][1].strip().split('_')[1]) + " ? (Y/N)" + "|" + "Y" + "\n");
					r = 1
				if r == 0:
                			qs_ans_out_file.write("CALL_DATA|NEGATIVE|ADD_YN_R_T|" + "Have you received a call from " + elem + " on " + outlier_call_dict[elem][1].strip().split('_')[0] + " at around " + create_rand_time_range(outlier_call_dict[elem][1].strip().split('_')[1]) + " ? (Y/N)" + "|" + "N" + "\n");


			elif outlier_call_dict[elem][0] == "Rejected":
				r = random.randint(0,1)
				if r == 0:
                			qs_ans_out_file.write("CALL_DATA|POSITIVE|ADD_YN_J_T|" + "Have you rejected a call from " + elem + " on " + outlier_call_dict[elem][1].strip().split('_')[0] + " at around " + create_time_range(outlier_call_dict[elem][1].strip().split('_')[1]) + " ? (Y/N)" + "|" + "Y" + "\n");
				else:
                			qs_ans_out_file.write("CALL_DATA|NEGATIVE|ADD_YN_J_T|" + "Have you rejected a call from " + elem + " on " + outlier_call_dict[elem][1].strip().split('_')[0] + " at around " + create_rand_time_range(outlier_call_dict[elem][1].strip().split('_')[1]) + " ? (Y/N)" + "|" + "N" + "\n");
                	q_count += 1
                	if q_count >= 20: #Eight Questions from actual date
                        	break
			i=i+1
			if i == no_of_questions_from_have_you_called:
				break

        #NEGATIVE Questions
	q_count = 0
        for elem in contact_name_number_dict:
		if elem not in all_call_info_dict and elem not in call_related_negative_questions_dict:
			r = random.randint(0,1)
			if r == 0:
				qs_ans_out_file.write("CALL_DATA|NEGATIVE|ADD_YN_C_P|" + "Have you " + rand_call( random.randint(1,3) ) + elem + " " + time_str + " ? (Y/N)" + "|" + "N" + "\n");
			else:
				qs_ans_out_file.write("CALL_DATA|NEGATIVE|ADD_YN_C_T|" + "Have you " + rand_call( random.randint(1,3) ) + elem + " on today/this week" + " at around " + str(random.randint(1,12)) + rand_am_pm( random.randint(1,2) ) + " +- 1hr ? (Y/N)" + "|" + "N" + "\n");
			q_count += 1
			
			if q_count > 10:
				break

have_you_called("today/this week")

caller_list = []

for elem in caller_name_time_dict_day:
	caller_list.append(caller_name_time_dict_day[elem])

contact_name_list = []

for elem in contact_name_number_dict:
	contact_name_list.append(elem)

outlier_call_list = []
for elem in outlier_call_dict:
	outlier_call_list.append(elem)

def give_random_caller_today( call_dict ):
	rand_num = random.randint(1, len(call_dict)-1 )
	return caller_list[ rand_num ]

def give_random_caller_time( call_dict ): #For today call dict
	rand_num = 1
	run_count = 0
	
	if( len(call_dict) == 0 ):
		return "unknown"

	while True:
		rand_num = random.randint(1, len(call_dict)-1 )
		run_count = run_count + 1
		if caller_list[ rand_num ] in contact_name_list:
			break
		if run_count >= 20:
			return "unknown"
	return caller_list[ rand_num ]

mcq_ans_str = ""
mcq_call_question_dict = {}

def check_list( rand_list_len ):
	ans_list = []
	i = 0
	while i < rand_list_len:
		if contact_name_list[i] in caller_list:
			if contact_name_list[i] not in mcq_call_question_dict:
				mcq_call_question_dict[contact_name_list[i]] = 1
				ans_list.append(contact_name_list[i] + ";T")
				i = i+1
			else:
				continue
		else:
			if contact_name_list[i] not in mcq_call_question_dict:
				mcq_call_question_dict[contact_name_list[i]] = 1
				ans_list.append(contact_name_list[i] + ";F")
				i = i+1
			else:
				continue
	return ans_list
	
def have_some_mcq_for_call_data ( time_str, front_str ):
	
        mcq_ans_str = ""
        q_str =""

	cl_nm = give_random_caller_today( caller_name_time_dict_day )
	uniq_random_list = []

	if cl_nm != "unknown":
		while 1:
			correct_name = give_random_caller_today( caller_name_time_dict_day ) + ";T"
			if correct_name not in mcq_call_question_dict:
				mcq_call_question_dict[correct_name] = 1
				break
		#uniq_random_list = sample(xrange(len(contact_name_list)-1), 3)	
		rand_list_len = 3
	else:
		#uniq_random_list = sample(xrange(len(contact_name_list)-1), 4)	
		rand_list_len = 4

        #uniq_random_list = sample(xrange(len(contact_name_list)-1), 3)
	#print uniq_random_list
	mcq_other_list = check_list( rand_list_len )
	mcq_other_list.append(correct_name)
	#print mcq_other_list
        random.shuffle(mcq_other_list)
	#print mcq_other_list

        j = 0
        for i in [ "A", "B", "C", "D"]:
		cor_option = mcq_other_list[j].strip().split(';')[1]

                if cor_option == "T":
                        mcq_ans_str = mcq_ans_str + i + ","

                q_str = q_str + i + ".  " + mcq_other_list[j].split(';')[0] + ";;"
                j = j + 1

        #print q_str
        line_str = front_str + q_str + "|" + mcq_ans_str + "\n"
        return line_str

#print all_call_info_dict
def check_call_in_range( date, time_start, time_end, caller ):
	#print caller

	if caller in all_call_info_dict:
		for lst in all_call_info_dict[caller]:
			#print lst
			date1 = lst[2].strip().split('_')[0]
			time1 = lst[2].strip().split('_')[1]
			time_hr = int(time1.strip().split('/')[0])

			if date1 == date:
				if ( time_start <= time_hr ) and  ( time_end >= time_hr ):
					return "T"
		return "F"
	else:
		return "F"

#print outlier_call_dict
def have_some_mcq_for_call_time ( time_str, front_str ):

        mcq_ans_str = ""
        q_str =""
	#unkown
	correct_hr = ""
	correct_dt = ""
	
        correct_name = give_random_caller_time( caller_name_time_dict_day ) 

	prev_correct_name = correct_name

	if correct_name != "unknown" and correct_name not in mcq_call_question_dict:
		mcq_call_question_dict[correct_name] = 1
		for elem in caller_name_time_dict_day:
			if caller_name_time_dict_day[elem] == correct_name:
				correct_hr = str(elem).strip().split('/')[0]
				break
		#correct_hr = int(caller_name_time_dict_day[correct_name][1].strip().split('_')[1].strip().split('/')[0])
		correct_dt = today_date
	else:
		while 1:
			correct_name = contact_name_list[ random.randint(0,len(contact_name_list)-1) ]
			if correct_name not in mcq_call_question_dict:
				mcq_call_question_dict[correct_name] = 1
				correct_hr = random.randint(1,12)
				correct_dt = today_date 
				break

	range_1 = ""
	range_2 = ""
	range_3 = ""
	cor_str = ""

	if correct_hr < 12 :
		range_1 = str ( convert_to_am_pm(int(correct_hr) + 2) ) + " to " + str ( convert_to_am_pm(int(correct_hr) + 4) )
		range_1 = range_1 + ";" + check_call_in_range( correct_dt, int(correct_hr) + 2, int(correct_hr) + 4, correct_name )
		range_2 = str ( convert_to_am_pm(int(correct_hr) + 5) ) + " to " + str ( convert_to_am_pm(int(correct_hr) + 7) )
		range_2 = range_2 + ";" + check_call_in_range( correct_dt, int(correct_hr) + 5, int(correct_hr) + 7, correct_name )
		range_3 = str ( convert_to_am_pm(int(correct_hr) + 8) ) + " to " + str ( convert_to_am_pm(int(correct_hr) + 10) )
		range_3 = range_3 + ";" + check_call_in_range( correct_dt, int(correct_hr) + 8, int(correct_hr) + 10, correct_name )
	else:
		range_1 = str ( convert_to_am_pm(int(correct_hr) - 4) ) + " to " + str ( convert_to_am_pm(int(correct_hr) - 2) )
		range_1 = range_1 + ";" + check_call_in_range( correct_dt, int(correct_hr) - 4, int(correct_hr) - 2, correct_name )
		range_2 = str ( convert_to_am_pm(int(correct_hr) - 7) ) + " to " + str ( convert_to_am_pm(int(correct_hr) - 5) )
		range_2 = range_2 + ";" + check_call_in_range( correct_dt, int(correct_hr) - 7, int(correct_hr) - 5, correct_name )
		range_3 = str ( convert_to_am_pm(int(correct_hr) - 10) ) + " to " + str ( convert_to_am_pm(int(correct_hr) - 8) )
		range_3 = range_3 + ";" + check_call_in_range( correct_dt, int(correct_hr) - 10, int(correct_hr) - 8, correct_name )
		
	if prev_correct_name != "unknown":
		if int(correct_hr) != 0:
			correct_range =  str( convert_to_am_pm(int(correct_hr) - 1 ) ) + " to " + str( convert_to_am_pm(int(correct_hr) + 1 ) )+ ";T"
		else:
			correct_range =  str( " 11 p.m." ) + " to " + str( " 1 a.m. ")+ ";T"
	else:
		if int(correct_hr) != 0:
			correct_range =  str( convert_to_am_pm(int(correct_hr) - 1 ) ) + " to " + str( convert_to_am_pm(int(correct_hr) + 1 ) )
		else:
			correct_range =  str( " 11 p.m." ) + " to " + str( " 1 a.m. ")

		correct_range = correct_range + ";" + check_call_in_range( correct_dt, int(correct_hr) - 1, int(correct_hr) + 1, correct_name )
	
        mcq_other_list = [ correct_range, range_1, range_2, range_3 ]
        #print mcq_other_list
        random.shuffle(mcq_other_list)
        #print mcq_other_list

        j = 0
        for i in [ "A", "B", "C", "D"]:
                cor_option = mcq_other_list[j].strip().split(';')[1]

                if cor_option == "T":
                        mcq_ans_str = mcq_ans_str + i + ","

                q_str = q_str + i + ".  " + mcq_other_list[j].split(';')[0] + ";;"
                j = j + 1

        #print q_str
        line_str = front_str + correct_name + " on " + str(correct_dt) + " ( Ex: A,B ) : |" + q_str + "|" + mcq_ans_str + "\n"
        return line_str


#MCQ based Questions on Profile Visit History
front_profile_str = "CALL_DATA|POS_NEG|UNI_MCQ_C_T|Please click the options corresponding to the correct time ranges of the following person you talked: "
if len(caller_name_time_dict_day) > 1:
	for i in range(5): #No of Time Range Based MCQs
        	qs_ans_out_file.write(have_some_mcq_for_call_time("today ( " + today_date + " )", front_profile_str))


#MCQ based Questions on Profile Visit History
front_profile_str = "CALL_DATA|POS_NEG|UNI_MCQ_C_P|Please click the options corresponding to the names of persons you talked with : |"

if len(caller_name_time_dict_day) > 1:
	for i in range(5): #No of Persons Based MCQs
        	qs_ans_out_file.write(have_some_mcq_for_call_data("today ( " + today_date + " )", front_profile_str))

#	qs_ans_out_file.write( "CALL_DATA|POSITIVE|UNI_TX_SC|Please write the first caller of today [ " + today_date + "] ( 12 am to 11:59 am ) : " + "|" + first_caller_of_the_day + "\n")
	qs_ans_out_file.write( "CALL_DATA|POSITIVE|UNI_TX_H_SC|Please write the first caller of today [ " + today_date + "] ( 12 am to 11:59 am ) : " + "(Hint-> " + give_some_hint_in_text( first_caller_of_the_day ) + ") |" + first_caller_of_the_day + "\n")

#print most_frequent_callers_of_the_week_list
#qs_ans_out_file.write( "CALL_DATA|POSITIVE|UNI_TX_SC|Please write the most frequent caller of this week : |" + most_frequent_callers_of_the_week_list[0][0] + "\n")
if (len(most_frequent_callers_of_the_week_list) > 0):
	qs_ans_out_file.write( "CALL_DATA|POSITIVE|UNI_TX_H_SC|Please write the most frequent caller of this week " + "(Hint-> " + give_some_hint_in_text( most_frequent_callers_of_the_week_list[0][0] ) + ") |" + most_frequent_callers_of_the_week_list[0][0] + "\n")

if ( len(highest_number_incoming_caller_day) > 0):
#	qs_ans_out_file.write( "CALL_DATA|POSITIVE|UNI_TX_SC|Please write the most frequent incoming caller of today [ " + today_date + "] : |" + highest_number_incoming_caller_day + "\n")
	qs_ans_out_file.write( "CALL_DATA|POSITIVE|UNI_TX_H_SC|Please write the most frequent incoming caller of today [ " + today_date + "] : " + "(Hint-> " + give_some_hint_in_text( highest_number_incoming_caller_day ) + ") |" + highest_number_incoming_caller_day + "\n")

if ( len(highest_number_outgoing_caller_day) > 0):
#	qs_ans_out_file.write( "CALL_DATA|POSITIVE|UNI_TX_SC|Please write the most frequent outgoing caller of today [ " + today_date + "] : |" + highest_number_outgoing_caller_day + "\n")
	qs_ans_out_file.write( "CALL_DATA|POSITIVE|UNI_TX_H_SC|Please write the most frequent outgoing caller of today [ " + today_date + "] : " + "(Hint-> " + give_some_hint_in_text( highest_number_outgoing_caller_day ) + ") |" + highest_number_outgoing_caller_day + "\n")

if (len(highest_duration_incoming_caller_day) > 0):
#	qs_ans_out_file.write( "CALL_DATA|POSITIVE|UNI_TX_SC|Please write the caller with whom you talked longest at a stretch(Incoming) today [ " + today_date + "] : |" + highest_duration_incoming_caller_day + "\n")
	qs_ans_out_file.write( "CALL_DATA|POSITIVE|UNI_TX_H_SC|Please write the caller with whom you talked longest at a stretch(Incoming) today [ " + today_date + "] : " + "(Hint-> " + give_some_hint_in_text( highest_duration_incoming_caller_day ) + ") |" + highest_duration_incoming_caller_day + "\n")

if (len(highest_duration_outgoing_caller_day) > 0):
#	qs_ans_out_file.write( "CALL_DATA|POSITIVE|UNI_TX_SC|Please write the caller with whom you talked longest at a stretch(Outgoing) today [ " + today_date + "] : |" + highest_duration_outgoing_caller_day + "\n")
	qs_ans_out_file.write( "CALL_DATA|POSITIVE|UNI_TX_H_SC|Please write the caller with whom you talked longest at a stretch(Outgoing) today [ " + today_date + "] : " + "(Hint-> " + give_some_hint_in_text( highest_duration_outgoing_caller_day ) + ") |" + highest_duration_outgoing_caller_day + "\n")


#SMS Question Generation

sms_inbox_dict = {}
sms_sent_dict = {}

flag = 0
line_prev = ""

def check_date( sms_date_time ):
	sms_date = sms_date_time.strip().split('_')[0]

	d1 = datetime.strptime(sms_date, "%d/%m/%Y")
	d2 = datetime.strptime(today_date, "%d/%m/%Y")

	if ( abs((d1-d2).days) <= 3 ):
		return True
	else:
		return False
	
for line in sms_sent_read_content:

	if line.count(';') > 4:
		continue
	if line.count(';') < 4:
		if flag == 1:
			line_prev = line_prev + line

			if line_prev.count(';') == 4:
				flag = 2
			else:
				continue
		else:
			line_prev = line
			flag = 1
			continue
	
	if flag == 2:
		line = line_prev
		flag = 0
		line_prev = ""

	l_str_lst = line.strip().split(';')

	#print l_str_lst
	date_time = l_str_lst[3].strip().split(':')[1]
	number = l_str_lst[4].strip().split(':')[1]

	if ( check_date( date_time ) == True ):
		new_number_list = []
		new_number_list.append(str(number) )

		if len(number) == 10:
			new_number_list.append( "+91" + str(number) )
			new_number_list.append( "0" + str(number) )
		elif len(number) == 11:
			new_number_list.append( str(number)[1:] )
			new_number_list.append( "+91" + str(number)[1:] )
		elif len(number) == 13:
			new_number_list.append( str(number)[3:] )
			new_number_list.append( "0" + str(number)[3:] )
		flag_num = -1
	
		for nm in new_number_list:
			if nm in contact_number_name_dict:
				flag_num = nm
				break

		if flag_num != -1:
			#print "sent",flag_num
			if contact_number_name_dict[flag_num] not in sms_sent_dict:
				sms_sent_dict[ contact_number_name_dict[flag_num] ] = [ date_time ]
			else:
				sms_sent_dict[ contact_number_name_dict[flag_num] ].append(date_time)

flag = 0
line_prev = ""

for line in sms_inbox_read_content:
	
	if line.count(';') > 4:
		continue
	if line.count(';') < 4:
		if flag == 1:
			line_prev = line_prev + line

			if line_prev.count(';') == 4:
				flag = 2
			else:
				continue
		else:
			line_prev = line
			flag = 1
			continue
	
	if flag == 2:
		line = line_prev
		flag = 0
		line_prev = ""
	
	l_str_lst = line.strip().split(';')
	#print l_str_lst

	date_time = l_str_lst[3].strip().split(':')[1]
	number = l_str_lst[4].strip().split(':')[1]

	if ( check_date( date_time ) == True ):
		new_number_list = []
	
		new_number_list.append(str(number) )
		
		if len(number) == 10:
			new_number_list.append( "+91" + str(number) )
			new_number_list.append( "0" + str(number) )
		elif len(number) == 11:
			new_number_list.append( str(number)[1:] )
			new_number_list.append( "+91" + str(number)[1:] )
		elif len(number) == 13:
			new_number_list.append( str(number)[3:] )
			new_number_list.append( "0" + str(number)[3:] )
		
		flag_num = -1
		
		for nm in new_number_list:
			if nm in contact_number_name_dict:
				flag_num = nm
				break
	
		if flag_num != -1:
			#print "in", flag_num
	                if contact_number_name_dict[flag_num] not in sms_inbox_dict:
	                        sms_inbox_dict[ contact_number_name_dict[flag_num] ] = [ date_time ]
	                else:
	                        sms_inbox_dict[ contact_number_name_dict[flag_num] ].append(date_time)
	
#print sms_sent_dict
#print sms_inbox_dict

def rand_sms( nm):
	if int(nm) == 1:
		return "sent"
	elif int(nm) == 2:
		return "received "

def create_rand_sms_time_range ( date_tm_list ):
	hour_list = []
	for elm in date_tm_list:
		hour_list.append( int(elm.strip().split('_')[1].split('/')[0]) )
	
	num_list = range(1,25)
	random.shuffle( num_list )

	for hr in num_list:
		x = int(hr)
		if ( (x-1) not in hour_list ) and ( x not in hour_list ) and ( (x+1) not in hour_list ) :
			#print x, convert_to_am_pm(x)
			new_x = convert_to_am_pm(x)
			return str(new_x) + " +- 1 hr"
		
	return str(0) + " am +- 1 hr"
sms_related_question_dict = {}
				
def have_you_smsed( time_str ):
	size_of_sms_sent_dict = len(sms_sent_dict)
	no_of_question_from_sent_dict = int(size_of_sms_sent_dict)/3
	i =0
        for elem in sms_sent_dict: #It should be reverse sorted list according to time
		if elem in contact_name_number_dict and elem not in sms_related_question_dict:
			sms_related_question_dict[elem] = 1
                	qs_ans_out_file.write("SMS_DATA|POSITIVE|ADD_YN_S_P|" + "Have you sent SMS to " + elem + " " + time_str + " ? (Y/N)" + "|" + "Y" + "\n");
			i=i+1
			if i == no_of_question_from_sent_dict:
				break
        
	size_of_sms_inbox_dict = len(sms_inbox_dict)
	no_of_question_from_inbox_dict = int(size_of_sms_inbox_dict)/3
	i =0
        for elem in sms_inbox_dict: #It should be reverse sorted list according to time
		if elem in contact_name_number_dict and elem not in sms_related_question_dict:
			sms_related_question_dict[elem] = 1
                	qs_ans_out_file.write("SMS_DATA|POSITIVE|ADD_YN_R_P|" + "Have you received SMS from " + elem + " " + time_str + " ? (Y/N)" + "|" + "Y" + "\n");
        		i=i+1
                        if i == no_of_question_from_inbox_dict:
                                break
	q_count = 0

	i =0
        for elem in sms_sent_dict: #It should be reverse sorted list according to time
		if elem in contact_name_number_dict and elem not in sms_related_question_dict:
			sms_related_question_dict[elem] = 1
			date_hr_list = sms_sent_dict[elem]
			sel_date_tm = date_hr_list[ random.randint(0,len(date_hr_list)-1) ]
		
			sel_date = sel_date_tm.strip().split('_')[0]
			sel_hr = sel_date_tm.strip().split('_')[1].split('/')[0]
			r = random.randint(0,1)
			if r == 0:
                		qs_ans_out_file.write("SMS_DATA|POSITIVE|ADD_YN_S_T|" + "Have you sent SMS to " + elem + " on " + sel_date + " at around " + create_time_range(sel_date_tm.strip().split('_')[1]) + " ? (Y/N)" + "|" + "Y" + "\n");
			else:
                		qs_ans_out_file.write("SMS_DATA|NEGATIVE|ADD_YN_S_T|" + "Have you sent SMS to " + elem + " on " + sel_date + " at around " + create_rand_sms_time_range(date_hr_list) + " ? (Y/N)" + "|" + "N" + "\n");
        
                	q_count += 1
                	if q_count >= 10: #Eight Questions from actual date
                        	break
			i=i+1
			if i == no_of_question_from_sent_dict:
				break

	q_count = 0
	i =0

        for elem in sms_inbox_dict: #It should be reverse sorted list according to time
		if elem in contact_name_number_dict and elem not in sms_related_question_dict:
			sms_related_question_dict[elem] = 1
			date_hr_list = sms_inbox_dict[elem]
			sel_date_tm = date_hr_list[ random.randint(0,len(date_hr_list)-1) ]
		
			sel_date = sel_date_tm.strip().split('_')[0]
			sel_hr = sel_date_tm.strip().split('_')[1].split('/')[0]
			if r == 0:
                		qs_ans_out_file.write("SMS_DATA|POSITIVE|ADD_YN_R_T|" + "Have you received SMS from " + elem + " on " + sel_date + " at around " + create_time_range(sel_date_tm.strip().split('_')[1]) + " ? (Y/N)" + "|" + "Y" + "\n");
			else:
                		qs_ans_out_file.write("SMS_DATA|NEGATIVE|ADD_YN_R_T|" + "Have you received SMS from " + elem + " on " + sel_date + " at around " + create_rand_sms_time_range(date_hr_list) + " ? (Y/N)" + "|" + "N" + "\n");
        
                	q_count += 1
                	if q_count >= 10: #Eight Questions from actual date
                        	break
			i=i+1
			if i == no_of_question_from_sent_dict:
				break

have_you_smsed("today/this week")

#SMS MCQ Create 

sms_sent_list = []
sms_recv_list = []

for elem in sms_sent_dict:
	sms_sent_list.append(elem)

for elem in sms_inbox_dict:
	sms_recv_list.append(elem)

def give_random_sms_sender_today( sms_dict ):
	count = 1
	while 1:
		rand_num = random.randint(0, len(sms_dict)-1 )
		if sms_sent_list[ rand_num ] not in sms_related_name_question_dict:
			sms_related_name_question_dict[sms_sent_list[ rand_num ]] =1 
			return sms_sent_list[ rand_num ]
		else:
                        count=count+1
                        if count ==20:
                                return "not exist"

def give_random_sms_recv_today( sms_dict ):
	#print len(sms_dict)
	count = 1
	while 1:
		rand_num = random.randint(0, len(sms_dict)-1 )
		if sms_recv_list[ rand_num ] not in sms_related_name_question_dict:
			sms_related_name_question_dict[sms_recv_list[ rand_num ]] = 1
			return sms_recv_list[ rand_num ]
		else:
			count=count+1
			if count ==20:
				return "not exist"

"""
def give_random_caller_time( call_dict ):
	rand_num = 1
	while True:
		rand_num = random.randint(1, len(call_dict)-1 )
		if outlier_call_list[ rand_num ] in contact_name_list:
			break
	return outlier_call_list[ rand_num ]
"""

mcq_ans_str = ""
sms_related_name_question_dict = {}
def check_sms_list( rand_list_len ):

	ans_list = []
	i = 0
	while i < rand_list_len:
		if contact_name_list[i] in sms_sent_list:
			if contact_name_list[i] not in sms_related_name_question_dict:
				ans_list.append(contact_name_list[i] + ";T")
				i=i+1
			else:
				continue
		else:
			if contact_name_list[i] not in sms_related_name_question_dict:
				ans_list.append(contact_name_list[i] + ";F")
				i=i+1
			else:
                                continue
	return ans_list

#print contact_name_number_dict	
#print contact_number_name_dict	

def have_some_mcq_for_sms_data ( time_str, front_str, sent_recv ):
	
        mcq_ans_str = ""
        q_str =""
	correct_name = ""

	if sent_recv == 0:
		while 1:
			if give_random_sms_sender_today( sms_sent_dict ) == "not exist":
				return ""
			correct_name = give_random_sms_sender_today( sms_sent_dict ) + ";T"
			if correct_name not in sms_related_name_question_dict:
				sms_related_name_question_dict[correct_name]=1
				break
	else:
		while 1:
			if give_random_sms_recv_today( sms_inbox_dict ) == "not exist":
				return ""
			correct_name = give_random_sms_recv_today( sms_inbox_dict ) + ";T"
			if correct_name not in sms_related_name_question_dict:
                                sms_related_name_question_dict[correct_name]=1
                                break

        uniq_random_list_len = 3
	#print uniq_random_list
	mcq_other_list = check_sms_list(uniq_random_list_len)
	mcq_other_list.append(correct_name)
	#print mcq_other_list
        random.shuffle(mcq_other_list)
	#print mcq_other_list

        j = 0
        for i in [ "A", "B", "C", "D"]:
		cor_option = mcq_other_list[j].strip().split(';')[1]

                if cor_option == "T":
                        mcq_ans_str = mcq_ans_str + i + ","

                q_str = q_str + i + ".  " + mcq_other_list[j].split(';')[0] + ";;"
                j = j + 1

        #print q_str
        line_str = front_str + q_str + "|" + mcq_ans_str + "\n"
	return line_str


front_profile_str = "SMS_DATA|POS_NEG|UNI_MCQ_S_P|Please click the options corresponding to the persons you Sent SMSes :|"

if len(sms_sent_dict) > 0:
	for i in range(5): #No of Ringtone MCQs
        	qs_ans_out_file.write(have_some_mcq_for_sms_data(" this week ", front_profile_str, 0))

front_profile_str = "SMS_DATA|POS_NEG|UNI_MCQ_R_P|Please click the options corresponding to the persons from whom you Received SMSes : |"

if len(sms_inbox_dict) > 0:
	for i in range(5): #No of Ringtone MCQs
        	qs_ans_out_file.write(have_some_mcq_for_sms_data(" this week ", front_profile_str, 1))

#print all_call_info_dict
def check_sms_in_range( date, time_start, time_end, caller, snd_rcv ):
	#print caller
	sent_dict = {}
	if snd_rcv == 0:
		sent_dict = sms_sent_dict
	else:
		sent_dict = sms_inbox_dict

	if caller in sent_dict:
		for elem in sent_dict[caller]:
			#print lst
			if elem not in sms_related_name_question_dict:
				sms_related_name_question_dict[elem] = 1
				date1 = elem.strip().split('_')[0]
				time1 = elem.strip().split('_')[1]
				time_hr = int(time1.strip().split('/')[0])

				if date1 == date:
					if ( time_start <= time_hr ) and  ( time_end >= time_hr ):
						return "T"
		return "F"
	else:
		return "F"

def have_some_mcq_for_sms_time ( time_str, front_str, sent_recv ):

        mcq_ans_str = ""
        q_str =""
	correct_name = ""

	if sent_recv == 0:
		correct_name = give_random_sms_sender_today( sms_sent_dict )
		if correct_name == "not exist":
			return ""
	else:
		correct_name = give_random_sms_recv_today( sms_inbox_dict )
		if correct_name == "not exist":
			return ""

	correct_hr = ""

	if sent_recv == 0: 
		correct_hr = int(sms_sent_dict[correct_name][0].strip().split('_')[1].strip().split('/')[0])
	else:
		correct_hr = int(sms_inbox_dict[correct_name][0].strip().split('_')[1].strip().split('/')[0])
	
	correct_dt = ""
	if sent_recv == 0: 
		correct_dt = sms_sent_dict[correct_name][0].strip().split('_')[0]
	else:
		correct_dt = sms_inbox_dict[correct_name][0].strip().split('_')[0]

	range_1 = ""
	range_2 = ""
	range_3 = ""
	cor_str = ""

	if correct_hr < 12 :
		range_1 = str ( convert_to_am_pm(int(correct_hr) + 2) ) + " to " + str ( convert_to_am_pm(int(correct_hr) + 4) )
		range_1 = range_1 + ";" + check_sms_in_range( correct_dt, int(correct_hr) + 2, int(correct_hr) + 4, correct_name, sent_recv )
		range_2 = str ( convert_to_am_pm(int(correct_hr) + 5) ) + " to " + str ( convert_to_am_pm(int(correct_hr) + 7) )
		range_2 = range_2 + ";" + check_sms_in_range( correct_dt, int(correct_hr) + 5, int(correct_hr) + 7, correct_name, sent_recv )
		range_3 = str ( convert_to_am_pm(int(correct_hr) + 8) ) + " to " + str ( convert_to_am_pm(int(correct_hr) + 10) )
		range_3 = range_3 + ";" + check_sms_in_range( correct_dt, int(correct_hr) + 8, int(correct_hr) + 10, correct_name, sent_recv )
	else:
		range_1 = str ( convert_to_am_pm(int(correct_hr) - 4) ) + " to " + str ( convert_to_am_pm(int(correct_hr) - 2) )
		range_1 = range_1 + ";" + check_sms_in_range( correct_dt, int(correct_hr) - 4, int(correct_hr) - 2, correct_name, sent_recv )
		range_2 = str ( convert_to_am_pm(int(correct_hr) - 7) ) + " to " + str ( convert_to_am_pm(int(correct_hr) - 5) )
		range_2 = range_2 + ";" + check_sms_in_range( correct_dt, int(correct_hr) - 7, int(correct_hr) - 5, correct_name, sent_recv )
		range_3 = str ( convert_to_am_pm(int(correct_hr) - 10) ) + " to " + str ( convert_to_am_pm(int(correct_hr) - 8) )
		range_3 = range_3 + ";" + check_sms_in_range( correct_dt, int(correct_hr) - 10, int(correct_hr) - 8, correct_name, sent_recv )
		
		
	correct_range =  str( convert_to_am_pm(int(correct_hr) - 1 ) ) + " to " + str( convert_to_am_pm(int(correct_hr) + 1 ) )+ ";T"
	
        mcq_other_list = [ correct_range, range_1, range_2, range_3 ]
        #print mcq_other_list
        random.shuffle(mcq_other_list)
        #print mcq_other_list

        j = 0
        for i in [ "A", "B", "C", "D"]:
                cor_option = mcq_other_list[j].strip().split(';')[1]

                if cor_option == "T":
                        mcq_ans_str = mcq_ans_str + i + ","

                q_str = q_str + i + ".  " + mcq_other_list[j].split(';')[0] + ";;"
                j = j + 1

        #print q_str
        line_str = front_str + correct_name + " on " + str(correct_dt) + " ( Ex: A,B ) : |" + q_str + "|" + mcq_ans_str + "\n"
        return line_str


#MCQ based Questions on Profile Visit History
front_profile_str = "SMS_DATA|POS_NEG|UNI_MCQ_S_T|Please click the options corresponding to the correct time ranges of the following person you sent SMS : "

if ( len(sms_sent_dict) > 0 ):
	for i in range(5): #No of Ringtone MCQs
		line_str = have_some_mcq_for_sms_time(" this week ", front_profile_str, 0)
		if line_str != "":
        		qs_ans_out_file.write(" this week " + str(line_str))
		else:
			break


front_profile_str = "SMS_DATA|POS_NEG|UNI_MCQ_S_P|Please click the options corresponding to the correct time ranges of the following person from whom you received SMS  : "

if ( len(sms_inbox_dict) > 0 ):
	for i in range(5): #No of Ringtone MCQs
		line_str = have_some_mcq_for_sms_time(" this week ", front_profile_str, 0)
                if line_str != "":
        		qs_ans_out_file.write(" this week " + str(line_str))
		else:
			break

qs_ans_out_file.close()

#encrypted_answer_file_name = "en_" + user_name + "_question_answer.log"
#command_str = "openssl des3 -salt -in " + qs_ans_out_file_name + " -out " + encrypted_answer_file_name + " -k swadhinpradhan"
#os.system(command_str)

