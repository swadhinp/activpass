import java.io.BufferedReader;
import java.io.BufferedWriter;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
//import java.util.LinkedList;



class Per_Call_Info{

	private String start_time;
	private int duration_in_seconds;
	private String with_whom;
	private String call_type;
	public Per_Call_Info next;
	private int frequency;
	private int periodicity;

	public Per_Call_Info(String st_time, int duration, String whom, String type){
		start_time=new String(st_time);
		duration_in_seconds=duration;
		with_whom=new String(whom);
		call_type=new String(type);
		next = null;
		frequency =1;
		periodicity = 1;
	}
	
	String get_start_time(){
		return start_time;
	}
	
	int get_duration(){
		return duration_in_seconds;
	}
	
	String get_other_party(){
		return with_whom;
	}
	
	String get_call_type(){
		return call_type;
	}
	
	void print_call_info(String fileName){
			try{
			  		// Create file 
				    System.out.println("final");
			  		FileWriter fstream = new FileWriter(fileName,true);
			  		BufferedWriter out = new BufferedWriter(fstream);
					if(!call_type.equals("Incoming") && !call_type.equals("Outgoing") && !call_type.equals("Missed"))
						call_type="Rejected";
			  		out.write("op:"+with_whom+";ct:"+call_type+";st:"+start_time+";duration:"+duration_in_seconds+"\n");
			  		//Close the output stream
			  		System.out.println("write");
			  		out.close();
			  }catch (Exception e){//Catch exception if any
				  	System.out.println("Error: " + e.getMessage());
			  }
		//System.out.println("op:"+with_whom+" ct:"+call_type+" st:"+start_time+" duration:"+duration_in_seconds+"\n");
	}
	
	int check_match(Per_Call_Info X){
		if(with_whom.equals(X.get_other_party()))
			return 1;
		else
			return 0;
	}
	
	void increment_frequency(){
		frequency++;
	}
	
	void increment_periodicity(){
		periodicity++;
	}
	
	int get_frequency(){
		return frequency;
	}
	
	int get_periodicity(){
		return periodicity;
	}
}

class Call_List{

	private Per_Call_Info first;
	private Per_Call_Info last;
	
	public Call_List(){
		first = null;
		last = null;
	}
	public void enqueue (Per_Call_Info X){
		if( first == null ){
			first=X;
			last = X;
		} else {
			last.next=X;
			last=X;
		}
	}
	
	public Per_Call_Info search_by_name(Per_Call_Info X){
		Per_Call_Info temp=first;
		
		while( null != temp ){
			if( X.get_other_party() == temp.get_other_party() )
				return X;
			else
				temp = temp.next;
		}
		return null;
	}
	
	int isEmpty(){
		if(first == null)
			return 1;
		else
			return 0;
	}
	
	Per_Call_Info getFirst(){
		return first;		
	}
	
	void print_call_list(String fileName){
		Per_Call_Info temp =  first;
		while( temp != null){
			  //System.out.println("Hello1");
			  System.out.println("h3");
			  temp.print_call_info(fileName);
			  temp=temp.next;
		}
	}
	
	void delete_frequent_and_periodic_call(int freq_threshold, int periodicity_threshold, String Deleted_Freq_File, String Deleted_Periodic_File, String Deleted_Freq_and_Periodic_File){
		Per_Call_Info temp=first,current=first;
		while(temp!=null){
			if(temp.get_frequency()>=freq_threshold || temp.get_periodicity()>=periodicity_threshold){
				
				if(temp==first){
					if(temp.get_frequency()>=freq_threshold && temp.get_periodicity()>=periodicity_threshold)
						temp.print_call_info(Deleted_Freq_and_Periodic_File);
					else if (temp.get_frequency()>=freq_threshold)
						temp.print_call_info(Deleted_Freq_File);
					else
						temp.print_call_info(Deleted_Periodic_File);
					first=first.next;
					temp=first;
					current=first;
				} else {
					current.next=temp.next;
					temp=temp.next;
				}
			} else {
				temp=temp.next;
				if(temp!=first)
					current=current.next;
			}
			//temp=null;
			
		}
	}
	
}

class Call_Set{
	private Call_List c_l;
	String name;
	public Call_Set side_link;
	public Call_Set down_link;
		
	public Call_Set(){
		c_l=new Call_List();
		name=null;
		side_link=null;
		down_link=null;
	}
	
	public Call_Set(String X){
		c_l=new Call_List();
		name=X;
		side_link=null;
		down_link=null;
	}
	
	public void add_call_info(Per_Call_Info X){
		c_l.enqueue(X);
	}
	
	public void add_side(Call_Set X){
		side_link=X;
	}
	
	public void add_side(String Z){
		Call_Set Y = new Call_Set(Z);
		side_link=Y;
	}
	
	public void add_down(Call_Set X){
		down_link=X;
	}
	
	public Call_List get_call_link(){
		return c_l;
	}
	
	public void add_down(String Z){
		Call_Set Y = new Call_Set(Z);
		down_link=Y;
	}
	
	public void print_call_set(String fileName){
		if(c_l!=null){
			System.out.println("h2");
			c_l.print_call_list(fileName);
		}
		
	}
	
	public void check_frequency_or_periodicty(Call_Set X, int dec){
		Call_List temp=X.c_l;
		Per_Call_Info temp_call = temp.getFirst();
		while(temp_call!=null){
			Per_Call_Info current=c_l.getFirst();
			while(current!=null){
				if(current.check_match(temp_call)==1){
					if(dec==1){//check for frequency
						temp_call.increment_frequency();
						current.increment_frequency();
					} else {
						temp_call.increment_periodicity();
						current.increment_periodicity();
					}
				}
				current=current.next;
					
			}
			temp_call=temp_call.next;
		}
		
	}
}

public class Activity_Parser {
	Call_Set c_s_head = new Call_Set("Head");
	
	String[] day = {"Sunday","Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
	String[] timeSlot = {"0to3","3to6","6to9","9to12","12to15","15to18","18to21","21to24"};
	
	public void build_double_dimensional_call_set(){
		Call_Set temp=c_s_head;
		for(int i=0;i<7;i++){
			temp.add_side(day[i]);
			temp=temp.side_link;
		}
		temp=c_s_head;
		for(int i=0;i<8;i++){
			temp.add_down(timeSlot[i]);
			temp=temp.down_link;
		}
		Call_Set timeSlotCurrent=c_s_head;
		for(int i=0;i<8;i++){
			timeSlotCurrent=timeSlotCurrent.down_link;
			Call_Set timeSlotCurrentTemp= timeSlotCurrent;
			Call_Set dayCurrent=c_s_head;
			for(int j=0;j<7;j++){
				dayCurrent=dayCurrent.side_link;
				Call_Set dayCurrentTemp=dayCurrent;
				Call_Set current=new Call_Set(timeSlotCurrent.name+dayCurrent.name);
				while(dayCurrentTemp.down_link!=null)
					dayCurrentTemp=dayCurrentTemp.down_link;
				while(timeSlotCurrentTemp.side_link!=null)
					timeSlotCurrentTemp=timeSlotCurrentTemp.side_link;
				dayCurrentTemp.add_down(current);
				timeSlotCurrentTemp.add_side(current);
				
			}
		}
		
	}
	
	public void measure_frequency_and_periodicity(){
		Call_Set side=c_s_head,down=c_s_head;
		side=side.side_link;
		while(side!=null){
			Call_Set down_current=side.down_link;
			while(down_current!=null){
				Call_Set down_next=down_current.down_link;
				while(down_next!=null){
					down_current.check_frequency_or_periodicty(down_next, 1);
					down_next=down_next.down_link;
				}
				down_current=down_current.down_link;
			}
			side=side.side_link;
		}
		
		down=down.down_link;
		while(down!=null){
			Call_Set side_current=down.side_link;
			while(side_current!=null){
				Call_Set side_next=side_current.side_link;
				while(side_next!=null){
					side_current.check_frequency_or_periodicty(side_next, 0);
					side_next=side_next.side_link;
				}
				side_current=side_current.side_link;
			}
			down=down.down_link;
		}
		
	}
	
	public void del_frequent_and_periodic_call(int freq_thresh, int period_thresh, String Deleted_Freq_File, String Deleted_Periodic_File, String Deleted_Freq_and_Periodic_File){
		Call_Set side=c_s_head;
		side=side.side_link;
		System.out.println("New1");
		while(side!=null){
			Call_Set down_current=side.down_link;
			while(down_current!=null){
				Call_List S=down_current.get_call_link();
				S.delete_frequent_and_periodic_call(freq_thresh, period_thresh, Deleted_Freq_File, Deleted_Periodic_File, Deleted_Freq_and_Periodic_File);
				down_current=down_current.down_link;
			}
			side=side.side_link;
		}
	}
	
	public void print_double_dimensional_call_set(String fileName){
		Call_Set timeSlotCurrent=c_s_head;
		for(int i=0;i<8;i++){
			timeSlotCurrent=timeSlotCurrent.down_link;
			Call_Set timeSlotCurrentTemp= timeSlotCurrent;
			//System.out.println("Hello");
			while (timeSlotCurrentTemp!=null){
				System.out.println("h1");
				timeSlotCurrentTemp.print_call_set(fileName);
				timeSlotCurrentTemp=timeSlotCurrentTemp.side_link;
			}
			
		}
	}
	
	public String find_time_slot(String X){
		String[] given_time = X.split("/");
		int[] given_time_int={Integer.parseInt(given_time[0]),Integer.parseInt(given_time[1]),Integer.parseInt(given_time[2])};
		String timeSlot = new String();
		int hour=given_time_int[0];
		if(hour==0){
			timeSlot="0:previous";
		} else {
			hour=hour-1;
			int slotIndex=hour/3;
			if(hour%3==0)
				timeSlot=""+slotIndex+":none";
			if(hour%3==1)
				timeSlot=""+slotIndex+":next";
			if(hour%3==2)
				timeSlot=""+slotIndex+":previous";
				
		}
		return timeSlot;
	}
	
	public String return_call_set(String X){
		Calendar cal = Calendar.getInstance();
    	cal.getTime();
    	int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
    	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy_HH/mm/ss");
    	String test1=sdf.format(cal.getTime());
    	String[] current_fields = test1.split("_");
    	String[] current_date =current_fields[0].split("/");
    	int[] current_date_int={Integer.parseInt(current_date[0]),Integer.parseInt(current_date[1]),Integer.parseInt(current_date[2])};
    	
    	
    	String[] given_fields = X.split("_");
    	String[] given_date =given_fields[0].split("/");
    	
    	int[] given_date_int={Integer.parseInt(given_date[0]),Integer.parseInt(given_date[1]),Integer.parseInt(given_date[2])};
    	
    	if(given_date_int[2]==current_date_int[2]){
    		if(given_date_int[1]==current_date_int[1]){
    			if(given_date_int[0]>current_date_int[0]){
    				return("null");
    			} else {
    				if(given_date_int[0]==current_date_int[0]){
    					//String day_of_week=day[dayOfWeek-1];
    					String timeSlot=find_time_slot(given_fields[1]);
    					return (""+dayOfWeek+":"+timeSlot);
    					
    				} else {
    					if((current_date_int[0]-given_date_int[0])<=6){
    						int diff=current_date_int[0]-given_date_int[0];
    						int given_day_of_week;
    						if(dayOfWeek>=diff){
    							given_day_of_week=dayOfWeek - diff;
    						} else {
    							given_day_of_week=dayOfWeek +7 - diff;
    						}
    						//String day_of_week=day[given_day_of_week-1];
    						String timeSlot=find_time_slot(given_fields[1]);
    						return (""+given_day_of_week+":"+timeSlot);
    					} else {
    						return("null");
    					}
    				}
    			}
    		} else {
    			if((current_date_int[1]-given_date_int[1])==1){
    				int leapyear=0;
    				if((given_date_int[2]%100)==0){
    					if((given_date_int[2]%400)==0)
    						leapyear=1;
    				} else {
    					if((given_date_int[2]%4)==0)
    						leapyear=1;
    				}
    				int diff=100;
    				if((given_date_int[1]==1)||(given_date_int[1]==3)||(given_date_int[1]==5)||(given_date_int[1]==7)||(given_date_int[1]==8)||(given_date_int[1]==10)||(given_date_int[1]==12)){
    					diff=31-given_date_int[0]+1+current_date_int[0];
    				}
    				if(given_date_int[1]==2){
    					if(leapyear==1)
    						diff=29-given_date_int[0]+1+current_date_int[0];
    					else
    						diff=28-given_date_int[0]+1+current_date_int[0];
    				}
    				if((given_date_int[1]==4)||(given_date_int[1]==6)||(given_date_int[1]==9)||(given_date_int[1]==11)){
    					diff=30-given_date_int[0]+1+current_date_int[0];
    				}
    				
    				if(diff<=6){
    					int given_day_of_week;
						if(dayOfWeek>diff){
							given_day_of_week=dayOfWeek - diff;
						} else {
							given_day_of_week=dayOfWeek +7 - diff;
						}
						//String day_of_week=day[given_day_of_week-1];
						String timeSlot=find_time_slot(given_fields[1]);
						return (""+given_day_of_week+":"+timeSlot);
    				} else {
    					return ("null");
    				}
    			} else {
    				return ("null");
    			}
    		}
    	} else {
    		if((current_date_int[0]-given_date_int[0])==1){
    			if(given_date_int[1]==12){
    				int diff=31-given_date_int[0]+1+current_date_int[0];
    				if(diff<=6){
    					int given_day_of_week;
						if(dayOfWeek>=diff){
							given_day_of_week=dayOfWeek - diff;
						} else {
							given_day_of_week=dayOfWeek +7 - diff;
						}
						//String day_of_week=day[given_day_of_week-1];
						String timeSlot=find_time_slot(given_fields[1]);
						return (""+given_day_of_week+":"+timeSlot);
    				} else {
    					return ("null");
    				}
    			} else {
    				return ("null");
    			}
    		} else {
    			return ("null");
    		}
    	}
    	//System.out.println(fields[0]+ "test"+fields[1]);
		
	}
	
	public Call_Set find_call_set(int slotIndex, int dayIndex){
		Call_Set down=c_s_head;
		for(int i=0;i<=slotIndex;i++)
			down=down.down_link;
		for(int i=0;i<dayIndex;i++)
			down=down.side_link;
		return down;
	}
	
	public void populate(String filename){
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			String line = null;
			StringBuilder sb = new StringBuilder();
		
			while ((line = br.readLine()) != null) {
				//System.out.println(line);

				String[] fields=line.split(";");
				String name = new String("");
				String num = new String("");
				String date = new String("");
				String type = new String("");
				int duration = 0;
				
				for(int i=0;i<fields.length;i++){
					String[] newarr=fields[i].split(":");
					if(newarr[0].equals("name")){
						if(newarr.length==2){
							name=new String(newarr[1]);
						}
					}
					if(newarr[0].equals("num")){
						if(newarr.length==2){
							num=new String(newarr[1]);
						}
					}
					if(newarr[0].equals("duration")){
						if(newarr.length==2){
							duration=Integer.parseInt(newarr[1]);
						}
					}
					if(newarr[0].equals("date")){
						if(newarr.length==2){
							date=new String(newarr[1]);
						}
					}
					if(newarr[0].equals("callType")){
						if(newarr.length==2){
							type=new String(newarr[1]);
						}
					}
				}
				if(name.equals(""))
					name=new String(num);
				Per_Call_Info pci=new Per_Call_Info(date,duration,name,type);
				String X=return_call_set(date);
				
				System.out.println(X);

				if(!X.equals("null")){
					String[] call_set_fields=X.split(":");
					
					int day_of_week=Integer.parseInt(call_set_fields[0]);
					int slotIndex=Integer.parseInt(call_set_fields[1]);
					String extra=call_set_fields[2];
					
					System.out.println(day_of_week);
					System.out.println(slotIndex);
					System.out.println(extra);

					Call_Set CS=find_call_set(slotIndex,day_of_week);
					CS.add_call_info(pci);
					if(!extra.equals("none")){
						if(extra.equals("next")){
							if(slotIndex!=7)
								slotIndex++;
							else{
								slotIndex=0;
								if(day_of_week!=7)
									day_of_week++;
								else
									day_of_week=1;
							}
							
						}
						if(extra.equals("previous")){
							if(slotIndex!=0)
								slotIndex--;
							else{
								slotIndex=7;
								if(day_of_week!=1)
									day_of_week--;
								else
									day_of_week=7;
							}
						}
						CS=find_call_set(slotIndex,day_of_week);
						CS.add_call_info(pci);
						
					}
				}
				
				
				sb.append(line);
				sb.append("\n");
			}
			//String text = sb.toString();
			//System.out.println(text);
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public static void main(String [] args) throws IOException{
		int length = args.length;
    		
		if (length < 7) {
    			System.out.println("\nCOMMAND: java Activity_Parser <IN: CallRecord_File> <IN: Frequency Threshold> <IN: Periodicity Threshold> <OUT: Deleted_Frequent_file> <OUT: Deleted_Periodic_file> <OUT: Deleted_Frequent_and_Periodic_file><OUT: CallOut_File>\n");
			System.exit(-1);
    		}
   		
		String call_file_name = args[0];
		int frequency = Integer.parseInt(args[1]);
		int periodicity = Integer.parseInt(args[2]);
		String deleted_frequent_file = args[3];
		String deleted_periodic_file = args[4];
		String deleted_frequent_and_periodic_file = args[5];
		String call_out_file_name = args[6];
		
		/*
		for (int i = 0; i < length; i++) {
			
    			System.out.println(args[i]);
   		}*/

		Activity_Parser ap = new Activity_Parser();
		ap.build_double_dimensional_call_set();
		//ap.populate("/home/swadhin/Desktop/Activity_CCS/call2013-4-13-1-41 AM.txt");
		ap.populate(call_file_name);
	//	ap.measure_frequency_and_periodicity();
	//	ap.del_frequent_and_periodic_call(frequency,periodicity,deleted_frequent_file,deleted_periodic_file,deleted_frequent_and_periodic_file);
		//ap.print_double_dimensional_call_set("/home/swadhin/workspace/out_call.txt");
		ap.print_double_dimensional_call_set(call_out_file_name);
		//ap.return_call_set("12/02/2013 03:02:01");
		
		
	}
	
}
