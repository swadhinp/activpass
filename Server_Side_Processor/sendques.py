import sys
import json
import random
import os
import re
from random import choice
from math import sqrt
from math import ceil
from os import listdir
from os.path import isfile, join
from random import sample
import urllib2
import httplib
import codecs
import time
import types
from datetime import datetime

if len(sys.argv) != 2:
        print "\nUsage: <Program_name> <IN: user_name>\n"
        exit(0)

username     = str(sys.argv[1])
asked_questions_file_name = username+"_asked_questions.log" 

ques_read_handle          = open( "/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + asked_questions_file_name, "r")
ques_read_content         = ques_read_handle.readlines()
ques_read_handle.close()

#os.system("rm -rf "+v)
questions={};
list1=[];

i=1
for line in ques_read_content:
    m={}
    line_s = line.strip().split("|")
    try:
	m["type"]=line_s[2]
	m["question"]=line_s[3]
	if re.search("MCQ", line_s[2] ):
		m["options"]=line_s[4]
        i+=1
	list1.append(m)
    except:
	x=1
questions["questions"] = list1;
questions["success"]="true"

print json.dumps(questions)
