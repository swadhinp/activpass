import sys
import os
import glob

os.chdir("/home/cnerg/Activpass/Server/www/activPass_core/response_base")

os.system("rm merged_responses/*")

#merge old and new responses
path = "/home/cnerg/Activpass/Server/www/activPass_core/response_base/response_1st_phase/*_question_answer_response.log"
files=glob.glob(path)
for file in files:     
    fname = str(file)
    name = fname.split('/')[-1]
    os.system("cp " + fname + " merged_responses/"+name )


path = "/home/cnerg/Activpass/Server/www/activPass_core/response_base/*_question_answer_response.log"
files=glob.glob(path)
for file in files:     
    fname = str(file)
    name = fname.split('/')[-1]
    if os.path.isfile("merged_responses/"+name):
	fout = open("merged_responses/"+name, 'a')
	fin = open(fname, 'r')
	for line in fin:
		fout.write(line)
	fin.close()
	fout.close()
    else:
	    os.system("cp " + fname + " merged_responses/"+name )
	
os.chdir("/home/cnerg/Activpass/Server/www/activPass_core/svm/")
command_str = "python ques_asked_to_others.py"
os.system(command_str)

