#!/usr/bin/python
import sys
import random
from random import sample
import os
from os import path
import re
from random import choice
from math import sqrt
from math import ceil
import time
from datetime import date


if len(sys.argv) != 5:
        print "\nUsage: <Program_name> <Parsed_Profile_Visit_Web_History_File> <Facebook_Data_Dir_Path> <Parsed_Web_History_File> <User_Name>\n"
        exit(0)
print "Question answer generation for facebook\n Parameters"
print "Parsed_Profile_Visit_Web_History_File =" + str(sys.argv[1])
print "Facebook_Data_Dir_Path = " + str(sys.argv[2])
print "Parsed_Web_History_File =" + str(sys.argv[3])
print "user name = " + str(sys.argv[4])


history_read_flag = 0
web_history_read_flag = 0
if os.path.exists(sys.argv[1]):
	b = os.path.getsize(sys.argv[1])
	if b != 0:
		history_read_handle          = open(sys.argv[1],'r')
		history_read_content         = history_read_handle.readlines()
		history_read_handle.close()
		history_read_flag = 1

fb_data_dir_path = str(sys.argv[2])

#onlyfiles = [ f for f in listdir(fb_data_dir_path) if isfile(join(fb_data_dir_path,f)) ]

fb_read_handle               	     = open(fb_data_dir_path + "/friend.csv",'r')
fb_read_content              	     = fb_read_handle.readlines()
fb_read_handle.close()

fb_read_handle               	     = open(fb_data_dir_path + "/message.csv",'r')
fb_message_read_content              = fb_read_handle.readlines()
fb_read_handle.close()

fb_read_handle               	     = open(fb_data_dir_path + "/page.csv",'r')
fb_page_read_content                 = fb_read_handle.readlines()
fb_read_handle.close()

fb_read_handle               	     = open(fb_data_dir_path + "/group.csv",'r')
fb_group_read_content                = fb_read_handle.readlines()
fb_read_handle.close()

fb_read_handle               	     = open(fb_data_dir_path + "/profile.csv",'r')
fb_profile_read_content              = fb_read_handle.readlines()
fb_read_handle.close()

if os.path.exists(sys.argv[3]):
	b = os.path.getsize(sys.argv[3])
	if b != 0:
		web_history_read_handle              = open(sys.argv[3],'r')
		web_history_read_content             = web_history_read_handle.readlines()
		web_history_read_handle.close()
		web_history_read_flag = 1

user_name 	= str(sys.argv[4])
#Putting the files info into appropriate data structures

fb_profile_dict = {}
fb_profile_id_dict = {}
fb_profile_name_dict = {}

def give_some_hint_in_text( ans_txt ):
        txt_len = 0
        for ch in ans_txt:
                txt_len = txt_len + 1

        uniq_random_list = sorted(sample(xrange(txt_len), int(txt_len*2/3)))

        new_hint_text = ""
        cnt = 0
        j = 0
        new_ans_txt = ""

        for ch in ans_txt:
                new_ch = ''
                if cnt == uniq_random_list[j] :
                        new_ch = "*"
                        j = j + 1
                else:
                        new_ch = ch

                new_ans_txt = new_ans_txt + new_ch
                cnt = cnt+1

                if j >= len(uniq_random_list) :
                        break

        return new_ans_txt


for line in fb_profile_read_content:
	l_str_lst = line.strip().split('|')
	try:
		fb_profile_dict[ l_str_lst[0] ] = l_str_lst[1]
		fb_profile_name_dict[ l_str_lst[1].strip().replace(' ', '').lower() ] = l_str_lst[1].strip()
		fb_profile_id_dict[ l_str_lst[1].strip().replace(' ', '').lower() ] = l_str_lst[0].strip()
	except IndexError:
		print "FB profile Index Error"


#Putting facebook friend list into a dictionary
if user_name not in fb_profile_id_dict:
	sys.exit()
user_key = fb_profile_id_dict[user_name]
friends_id_dict = {}

for line in fb_read_content:
	line_m = line.strip().split('|')
	try:
		if user_key == str(line_m[0].strip()) :
			friends_id_dict[line_m[2]] = line_m[3]
	except IndexError:
		print "FB friend list Index Error"
#print friends_id_dict

#Putting Web history into a dictionary
todays_date = date.today()
history_dict = {}
if history_read_flag == 1:
	#print history_read_content
	for line in history_read_content:
		line_m = line.strip().split('|')
		try:
			visit_date = date(int(line_m[2].split(' @ ')[0].split('/')[2]),int(line_m[2].split(' @ ')[0].split('/')[1]),int(line_m[2].split(' @ ')[0].split('/')[0]))
			date_diff = (todays_date - visit_date).days
			if date_diff <= 3:
				list_m = [ line_m[1], line_m[2] ]
				history_dict[line_m[0]] = list_m
		except IndexError:
			print "History Index Error"
	#Sorting web history list according to time and visit count
	sorted_based_on_visit_count_list_descending = sorted(history_dict.items(), key=lambda x: x[1][0], reverse = True)
	sorted_based_on_time_list_descending = sorted(history_dict.items(), key=lambda x: x[1][1], reverse = True)

	#Iterating through the listd to find visiting patterns
	friends_not_visited = []
	non_friends_visited = []
	friends_visited = []

	for key in history_dict:
		if key in friends_id_dict:
			friends_visited.append(key)
		else:
			non_friends_visited.append(key)

	for key in friends_id_dict:
		if key not in history_dict:
			friends_not_visited.append(key)

#print friends_visited
#print non_friends_visited

#print sorted_based_on_time_list_descending
#print sorted_based_on_visit_count_list
elem_friend_dict = {}
qs_ans_out_file_name 		 = "/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + str(user_name) + "_question_answer_facebook.log"
qs_ans_out_file                  = open( qs_ans_out_file_name, 'w');

#Question Generation from profile visit history
#Have you visited Qs
def have_you_visited( time_str ):
	no_of_friends_visited = len(friends_visited)
	no_of_questions_from_friends_visited = int(no_of_friends_visited)/4
	if no_of_questions_from_friends_visited == 0 and no_of_friends_visited >0:
		no_of_questions_from_friends_visited =1
	i =0
	while i < no_of_questions_from_friends_visited:
		for elem in friends_visited:
			if elem not in elem_friend_dict:
				elem_friend_dict[elem] = 1
				qs_ans_out_file.write("HISTORY_PROFILE|POSITIVE|ADD_YN|" + "Have you visited (in last 3 days) your friend " + friends_id_dict[elem] +  "'s profile ? (Y/N)" + "|" + "Y" + "\n");
				i = i+1
				break
	no_of_non_friends_visited = len(non_friends_visited)
        no_of_questions_from_non_friends_visited = int(no_of_non_friends_visited)/4
        if no_of_questions_from_non_friends_visited == 0 and no_of_non_friends_visited >0:
                no_of_questions_from_non_friends_visited =1
        i =0
	while i < no_of_questions_from_non_friends_visited:
		for elem in non_friends_visited:
			if elem not in elem_friend_dict:
				elem_friend_dict[elem] = 1
				qs_ans_out_file.write("HISTORY_PROFILE|POSITIVE|ADD_YN|" + "Have you visited (in last 3 days) " + elem + "'s profile ? (Y/N)" + "|" + "Y" + "\n");	
				i = i+1
				break
	no_of_friends_not_visited = len(friends_not_visited)
        no_of_questions_from_friends_not_visited = int(no_of_friends_not_visited)/4
        if no_of_questions_from_friends_not_visited == 0 and no_of_friends_not_visited >0:
                no_of_questions_from_friends_not_visited =1
	elif no_of_questions_from_friends_not_visited > 5:
		no_of_questions_from_friends_not_visited = 1
        i =0
	while i < no_of_questions_from_friends_not_visited:
		for elem in friends_not_visited: #for negative questions
			if elem not in elem_friend_dict:
                                elem_friend_dict[elem] = 1
				qs_ans_out_file.write("HISTORY_PROFILE|NEGATIVE|ADD_YN|" + "Have you visited your friend (in last 3 days) " + friends_id_dict[elem] + "'s profile ? (Y/N)" + "|" + "N" + "\n");
				i = i+1
                                break

if history_read_flag == 1:
	have_you_visited("today/this week")
#have_you_visited("this week")

mcq_ans_str = ""
#MCQs regarding have you visited Qs
def make_mcq ( num, opt ):
	global mcq_ans_str
	
	if num == 1 :
		if len(friends_visited) > 0:
			mcq_ans_str = mcq_ans_str + opt + ";"
			elem = friends_visited[random.randint(0,len(friends_visited)-1)]
			if elem not in elem_friend_dict:
                                elem_friend_dict[elem] = 1
				return friends_id_dict[elem]
			else:
				return "agn"
		else:
			return "agn"
	elif num == 2 :
		if len(non_friends_visited) > 0:
			mcq_ans_str = mcq_ans_str + opt + ";"
			elem = non_friends_visited[random.randint(0,len(non_friends_visited)-1)]
			if elem not in elem_friend_dict:
                                elem_friend_dict[elem] = 1
                                return elem
			else:
				return "agn"
		else:
			return "agn"
	else:
		if len(friends_not_visited) > 0:
			elem = friends_not_visited[random.randint(0,len(friends_not_visited)-1)]
			if elem not in elem_friend_dict:
                                elem_friend_dict[elem] = 1
                                return friends_id_dict[elem]
			else:
				return "agn"
		else:
			return "agn"

def process_string ( l_str ):
	return l_str.replace('0','').replace('1','').replace('2','').replace('3','').replace('4','').replace('5','').replace('6','').replace('7','').replace('8','').replace('9','').replace('.',' ')

def have_some_mcq ( time_str, front_str ):
	
	global mcq_ans_str
	mcq_ans_str = ""
	q_str =""
	ret_val = ""
	for i in [ "A", "B", "C", "D"]:
		while True:
			rand_num = random.randint(1,3)
			ret_val = make_mcq(rand_num,i).lower()
			if ret_val != "agn":
				break
		#print rand_num
		q_str = q_str + i + ".  " + process_string(ret_val) + ";"
	#print q_str
	line_str = front_str + q_str + "|" + mcq_ans_str + "\n"
	return line_str
'''
#MCQ based Questions on Profile Visit History
if history_read_flag == 1:
	front_profile_str = "HISTORY_PROFILE|POS_NEG|UNI_MCQ|Please click the options of the persons' profiles you visited (in last 3 days) :|" 
	for i in range(6): #No of MCQs
		qs_ans_out_file.write(have_some_mcq("", front_profile_str))

	ans_str = ""
	f_k = -1
	#print "sorted_based_on_visit_count_list_descending"
	#print sorted_based_on_visit_count_list_descending
	len_list = len(sorted_based_on_visit_count_list_descending)

	for k in range( len_list ):
		if sorted_based_on_visit_count_list_descending[k][0] in friends_id_dict:
			ans_str = friends_id_dict[sorted_based_on_visit_count_list_descending[k][0]]
			f_k = k
			break
	count = 1
	if f_k != -1:
		for k in range( len_list ):
			if ( (k + f_k + 1) < len_list ):
				if sorted_based_on_visit_count_list_descending[k+f_k+1][0] in friends_id_dict:
					ans_str = ans_str + ";" + friends_id_dict[sorted_based_on_visit_count_list_descending[k+f_k+1][0]]
					count = count + 1
					if count > 4:
						break
		#print ans_str
		front_profile_str = "HISTORY_PROFILE|POSITIVE|UNI_TX|Which friend did you visit most frequently today/this week ? (FIRST_NAME LAST_NAME) |" 
		qs_ans_out_file.write(front_profile_str + ans_str + "\n")
		front_profile_str = "HISTORY_PROFILE|POSITIVE|UNI_H_TX|Which friend did you visit most frequently today/this week ? (FIRST_NAME LAST_NAME) Hint->( " + give_some_hint_in_text( ans_str ) + " ) |" 
		qs_ans_out_file.write(front_profile_str + ans_str + "\n")

	ans_str = ""
	f_k = -1
	len_list = len(sorted_based_on_time_list_descending)

	for k in range( len_list ):
		if sorted_based_on_time_list_descending[k][0] in friends_id_dict:
			ans_str = friends_id_dict[sorted_based_on_time_list_descending[k][0]]
			f_k = k
			break
	count = 1
	if f_k != -1:
		for k in range( len_list ):
			if ( (k + f_k + 1) < len_list ):
				if sorted_based_on_time_list_descending[k+f_k+1][0] in friends_id_dict:
					ans_str = ans_str + ";" + friends_id_dict[sorted_based_on_time_list_descending[k+f_k+1][0]]
					count = count + 1
					if count > 4:
						break
		#print ans_str
		front_profile_str = "HISTORY_PROFILE|POSITIVE|UNI_TX|Which friend did you visit most recently today/this week ? (FIRST_NAME LAST_NAME) |" 
		qs_ans_out_file.write(front_profile_str + ans_str + "\n")
		front_profile_str = "HISTORY_PROFILE|POSITIVE|UNI_H_TX|Which friend did you visit most frequently today/this week ? (FIRST_NAME LAST_NAME) Hint->( " + give_some_hint_in_text( ans_str ) + " ) |" 
		qs_ans_out_file.write(front_profile_str + ans_str + "\n")
'''
#print history_dict
#Profile Visit Question generation Done

web_history_dict = {}
if web_history_read_flag == 1:
#Question Generation from Web History
	for line in web_history_read_content:
		line_lst = line.strip().split('|')
		try:
			web_history_dict[line_lst[0]] = [ line_lst[1], line_lst[2] ]
		except IndexError:
			print "Web history Index Error"

def create_web_history_related_qs ( f_str, e_str, elm ):
	work_items = web_history_dict[elem][0].strip().split(';')

	for item in work_items:
		if item != "":
			line_web_str = f_str + str(item) + e_str + "|" + "Y\n"
			qs_ans_out_file.write ( line_web_str )
	
if web_history_read_flag == 1:
	web_line_str = ""
	for elem in web_history_dict:
		if elem == user_name:
			front_profile_web_line_str = "HISTORY_WEB|POSITIVE|ADD_YN| Have you looked into and used "
			end_web_line_str = " today/this week in your own account ? (Y/N) "
			create_web_history_related_qs( front_profile_web_line_str, end_web_line_str, elem )
		else:
			front_profile_web_line_str = "HISTORY_WEB|POSITIVE|ADD_YN| Have you looked into and browsed "
			if elem in friends_id_dict:
				end_web_line_str = " today/this week in your friend " + friends_id_dict[elem] + "'s account ? (Y/N) "
				create_web_history_related_qs( front_profile_web_line_str, end_web_line_str, elem )
			else:
				end_web_line_str = " today/this week in " + process_string(elem) + "'s account ? (Y/N) "
				create_web_history_related_qs( front_profile_web_line_str, end_web_line_str, elem )

#print web_history_dict

#MCQs regarding have you visited Qs for web_history
work_item_not_done_list = []
work_item_list = [ "Photos", "All Photos", "Photo Albums", "Tagged Photos", "Friend list", "Total Friend list", "Mutual Friend list", "Recently Added Friend list", "Suggested Friend list (People you may know)", "Follower list", "Following list", "Events atended", "Music likings", "Radiostations likings", "Favorite Music related pages", "Favorite Music Playlists", "Places visited ( check ins )", "About Information", "Liked pages" ]

if web_history_read_flag == 1:
	web_history_dict_list = []
	for elem in web_history_dict:
		web_history_dict_list.append(elem)

	mcq_ans_str = ""
def make_mcq_for_web ( num, opt, w_l, w_n_l ):
	global mcq_ans_str
	
	if num == 1 :
		if len(w_l) > 0:
			mcq_ans_str = mcq_ans_str + opt + ";"
			return w_l[random.randint(0,len(w_l)-1)]
		else:
			return "agn"
	else:
		if len(w_n_l) > 0:
			return w_n_l[random.randint(0,len(w_n_l)-1)]
		else:
			return "agn"

def have_some_mcq_for_web ( end_str, front_str ):
	global mcq_ans_str
	mcq_ans_str = ""
	elem = web_history_dict_list[random.randint(0,len(web_history_dict_list)-1)]
	visitor_name = ""
	if elem in friends_id_dict:
		visitor_name = friends_id_dict[elem]
	else:
		visitor_name = process_string(elem)

	works_list = web_history_dict[elem][0].strip().split(';')
	for el in work_item_list:
		if el not in works_list:
			work_item_not_done_list.append(el)

	q_str =""
	ret_val = ""
	for i in [ "A", "B", "C", "D"]:
		while True:
			rand_num = random.randint(1,2)
			ret_val = make_mcq_for_web(rand_num, i, works_list, work_item_not_done_list).lower()

			if ret_val != "agn":
				break
			#print rand_num
		q_str = q_str + i + ".  " + process_string(ret_val) + ";"
	#print q_str
	line_str = front_str + visitor_name + end_str + q_str + "|" + mcq_ans_str + "\n"
	return line_str

#MCQ based Questions on Profile Visit History
if web_history_read_flag == 1:
	if len(web_history_dict_list) > 0:
		front_profile_str = "HISTORY_WEB|POS_NEG|UNI_MCQ|Please click the options of the things you visited in the profile of person named "
		for i in range(3): #No of MCQs
			qs_ans_out_file.write(have_some_mcq_for_web(" today/this week in comma separated way :|", front_profile_str))


#Create Questions Facebook
pages_created_dict = {}
secret_groups_dict ={}
message_comm_dict = {}

#Putting the parsed facebook data in appropriate data structures

for line in fb_page_read_content:
	l_str_lst = line.strip().split('|')
	try:
		if l_str_lst[0] not in pages_created_dict:
			pages_created_dict[ l_str_lst[0] ] = [ l_str_lst[1] ]
		else:
			pages_created_dict[ l_str_lst[0] ].append(l_str_lst[1])
	except IndexError:
		print "Page Index Error"

for line in fb_group_read_content:
	l_str_lst = line.strip().split('|')
	#print l_str_lst	
	try:
		if (l_str_lst[3] == "SECRET" or l_str_lst[3] == "CLOSED") :
			if l_str_lst[0] not in secret_groups_dict:
				secret_groups_dict[ l_str_lst[0] ] = [ l_str_lst[1] ]
			else:
				secret_groups_dict[ l_str_lst[0] ].append( l_str_lst[1] )	
	except IndexError:
		print "Group Index Error"

msg_comm_thread_time_dict = {}

len_list = len(fb_message_read_content)
inc_i = 0

for i in range( len_list ):
	if (i+inc_i) >= len_list :
		break
	i = i+inc_i
	line = fb_message_read_content[i]
	l_str_lst = line.strip().split('|')
	#g = 0
	while ( len( l_str_lst ) < 6 ):
		#g = 1
		inc_i = inc_i + 1
		i = i + 1
		line = line + fb_message_read_content[i]
		#print line
		l_str_lst = line.strip().replace("\n","").split('|')
	
	msg_thread_id = str( l_str_lst[4].strip().split("_")[0] )
	
	msg_comm_thread_time_dict[msg_thread_id] = l_str_lst[5]
	
	if msg_thread_id not in message_comm_dict:
		message_comm_dict [ msg_thread_id ] =	[ [ l_str_lst[0], l_str_lst[1] ] ]
	else:
		message_comm_dict [ msg_thread_id ].append([ l_str_lst[0], l_str_lst[1] ])
		
	#Code for unit testing
	#if g == 1:
		#print "HOHO : ", line, "******"
		#g = 0
	#if( not ( re.search(r"[0-9]+", l_str_lst[0] ) ) ):
	#	print l_str_lst
	#print l_str_lst

fb_user_message_dict = {}

for thread_id in message_comm_dict:

	people_in_message_exchange_dict = {}
	for el_list in message_comm_dict[thread_id]:

		msg_sender = el_list[1].strip().replace(' ', '').lower()
		#print el_list, "strip : ", msg_sender
		people_in_message_exchange_dict[msg_sender] = el_list[1]

	for fb_name in fb_profile_name_dict:
		if fb_name in people_in_message_exchange_dict:
			if fb_name not in fb_user_message_dict:
				fb_user_message_dict[fb_name] = [ [ people_in_message_exchange_dict, msg_comm_thread_time_dict[thread_id] ] ]
			else:
				fb_user_message_dict[fb_name].append( [ people_in_message_exchange_dict, msg_comm_thread_time_dict[thread_id] ] )

	#print other_people_in_conf		
	#print message_comm_dict[thread_id]

#print fb_user_message_dict
fb_user_message_history_dict = {}

for fb_name in fb_profile_name_dict:
	for msg_history_list in fb_user_message_dict[fb_name]:
		for send_name in msg_history_list[0]:
			if send_name != fb_name:
				if fb_name not in fb_user_message_history_dict:
					fb_user_message_history_dict[ fb_name ] = [ [ msg_history_list[0][send_name] , msg_history_list[1] ] ]
				else:
					fb_user_message_history_dict[ fb_name ].append( [ msg_history_list[0][send_name] , msg_history_list[1] ] )
#print fb_user_message_history_dict


#print secret_groups_dict
#Questions from secret/closed groups membership
front_profile_str = "FACEBOOK_DATA|POSITIVE|ADD_YN_G|Are you member of secret group named '"
if user_name in fb_profile_name_dict:
	if fb_profile_id_dict[ user_name ] in secret_groups_dict:
		groups_list = secret_groups_dict [ fb_profile_id_dict[ user_name ] ]
		for elem in groups_list:
			try:
				elem.decode('ascii')
			except UnicodeDecodeError:
				continue
			if ( not ( re.search(r"\\x", str(elem) ) ) ):
				line_str = front_profile_str + str(elem) + "' ? (Y/N) |Y" + "\n"
				qs_ans_out_file.write(line_str)

front_profile_str = "FACEBOOK_DATA|NEGATIVE|ADD_YN_G|Are you member of secret group named '"
other_groups_list = []

user_key = fb_profile_id_dict[ user_name ]
for key in secret_groups_dict:
	if key != user_key :
		for elem in secret_groups_dict[key] :
			if ( not ( re.search(r"\\x", str(elem) ) ) ):
				other_groups_list.append( elem )

for i in range(10):
	line_str = front_profile_str + other_groups_list[i] + "' ? (Y/N) |N\n"
	qs_ans_out_file.write(line_str)

#print other_groups_list
#print groups_list 
#for elem in message_comm_dict:
#	print  " Thread : ", elem, " People : ",  message_comm_dict[elem]
		
"""
print fb_profile_dict
print fb_profile_name_dict
print pages_created_dict
print secret_groups_dict
"""
#print pages_created_dict

#Questions from pages/groups/apps created
front_profile_str = "FACEBOOK_DATA|POSITIVE|ADD_YN_P|Have you created this page called '"

if user_name in fb_profile_name_dict:
	if fb_profile_id_dict[ user_name ] in pages_created_dict:
		pages_list = pages_created_dict [ fb_profile_id_dict[ user_name ] ]
		for elem in pages_list:
			print "Hiii  check: " + elem
			try:
                                elem.decode('ascii')
                        except UnicodeDecodeError:
                                continue
			if ( not ( re.search(r"\\x", str(elem) ) ) ):
				line_str = front_profile_str + str(elem) + "' ? (Y/N) |Y" + "\n"
				qs_ans_out_file.write(line_str)

front_profile_str = "FACEBOOK_DATA|NEGATIVE|ADD_YN_P|Have you created this page called '"
other_pages_list = []

user_key = fb_profile_id_dict[ user_name ]
for key in pages_created_dict:
	if key != user_key :
		for elem in pages_created_dict[key] :
			if ( not ( re.search(r"\\x", str(elem) ) ) ):
				other_pages_list.append( elem )

for i in range(10):
	try:
		line_str = front_profile_str + other_pages_list[i] + "' ? (Y/N) |N\n"
		qs_ans_out_file.write(line_str)
	except IndexError:
		x=1

#Questions from messages sent to particular user

front_profile_str = "FACEBOOK_DATA|POSITIVE|ADD_YN_M|Have you chatted or exchanged messages with "
chat_friend_list = []
if user_name in fb_user_message_history_dict:
	for chatter_lst in fb_user_message_history_dict[user_name]:
		chat_friend_list.append(chatter_lst[0])
		line_str = front_profile_str + chatter_lst[0] + " recently ? (Y/N) |Y\n"
		#print line_str
		qs_ans_out_file.write(line_str)


non_chat_all_friend_list = []
front_profile_str = "FACEBOOK_DATA|NEGATIVE|ADD_YN_M|Have you chatted or exchanged messages with "
for elem in friends_id_dict:
	if friends_id_dict[elem] not in chat_friend_list:
		non_chat_all_friend_list.append(friends_id_dict[elem])

for i in range(10):
        line_str = front_profile_str + non_chat_all_friend_list[i] + " recently ? (Y/N) |N\n"
	#print "Other ", line_str
        qs_ans_out_file.write(line_str)

#print fb_user_message_history_dict
mcq_ans_str = ""
def make_mcq_for_fb_message ( num, opt, w_l, w_n_l ):
	global mcq_ans_str
	
	if num == 1 :
		if len(w_l) > 0:
			mcq_ans_str = mcq_ans_str + opt + ";"
			return w_l[random.randint(0,len(w_l)-1)]
		else:
			return "agn"
	else:
		if len(w_n_l) > 0:
			return w_n_l[random.randint(0,len(w_n_l)-1)]
		else:
			return "agn"

def have_some_mcq_for_fb_message ( end_str, front_str ):
	global mcq_ans_str
	mcq_ans_str = ""
	
	q_str =""
	ret_val = ""
	for i in [ "A", "B", "C", "D"]:
		while True:
			rand_num = random.randint(1,2)
			ret_val = make_mcq_for_fb_message(rand_num, i, chat_friend_list, non_chat_all_friend_list)

			if ret_val != "agn":
				break
			#print rand_num
		q_str = q_str + i + ".  " + ret_val + ";"
	#print q_str
	line_str = front_str + end_str + q_str + "|" + mcq_ans_str + "\n"
	return line_str

#MCQ based Questions on Profile Visit History
front_profile_str = "FACEBOOK_DATA|POS_NEG|UNI_MCQ|Please click the options of recently chatted persons "
for i in range(6): #No of MCQs
	qs_ans_out_file.write(have_some_mcq_for_fb_message(" in comma separated way :|", front_profile_str))

qs_ans_out_file.close()

#encrypted_answer_file_name = "en_" + user_name + "_question_answer.log"
#command_str = "openssl des3 -salt -in " + qs_ans_out_file_name + " -out " + encrypted_answer_file_name + " -k swadhinpradhan"
#os.system(command_str)
#os.system("rm -rf " + qs_ans_out_file)
