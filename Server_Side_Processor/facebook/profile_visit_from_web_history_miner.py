#!/usr/bin/python
import sys
import random
import os
import re
from random import choice
from math import sqrt
from math import ceil


if len(sys.argv) != 3:
        print "\nUsage: <Program_name> <Input History File> <User Name>\n"
        exit(0)

history_read_handle               = open(sys.argv[1],'r')
history_read_content              = history_read_handle.readlines()
history_read_handle.close()

user_name = str(sys.argv[2])

history_out_file                  = open( "/home/cnerg/Activpass/Server/www/activPass_core/data_base/facebook_data/" + user_name + "_profile_visit_from_web_history.txt",'w');


history_dict={}
profile_history_count={}

line_count=1;
for line in history_read_content:
	if line_count != 1 :
		line_fields = line.strip().split(':')
		try:
			time_in_milliseconds_from_epoch = line_fields[2]
			full_url=line_fields[1].split('//')[1]
			important_part_of_url=full_url.split('/')
			if important_part_of_url[0] == "www.facebook.com" :
				if (not(re.match("events", important_part_of_url[1]) or re.match("photo", important_part_of_url[1]) or re.match("l.php", important_part_of_url[1]) or re.match("group", important_part_of_url[1]) or re.match("dialog", important_part_of_url[1]) or re.match("find", important_part_of_url[1]) or re.match("search", important_part_of_url[1]) or re.match("friend", important_part_of_url[1]) or re.match("message", important_part_of_url[1]) or re.match("sk=nf", important_part_of_url[1]) or re.match("appcenter", important_part_of_url[1]) or re.match("apps", important_part_of_url[1]) or re.match("page", important_part_of_url[1]) or re.match("addpage", important_part_of_url[1]) or re.match("ads", important_part_of_url[1]) or re.match("list", important_part_of_url[1]) or re.match("notes", important_part_of_url[1]) or re.match(r'^\s*$', important_part_of_url[1]))):
					profile_visited=important_part_of_url[1].split('?')[0]
	
					if re.search(r"\.php$", profile_visited):
						continue;
					else:	
						if profile_visited not in history_dict:
							history_dict[profile_visited]=time_in_milliseconds_from_epoch
							profile_history_count[profile_visited] = 1
						else:
							profile_history_count[profile_visited] = int(profile_history_count[profile_visited]) + 1
							history_dict[profile_visited]=time_in_milliseconds_from_epoch
		except IndexError:
			#print "
			Web_history_Index_Error=1
	line_count = line_count+1

#print history_dict
#print profile_history_count

for key in history_dict:
	line_str = str(key) + "|" + str(profile_history_count[key]) + "|" + str(history_dict[key]) + "\n";
	history_out_file.write(line_str);

history_out_file.close()
