#!/usr/bin/python
import sys
import random
import os
import re
from random import choice
from math import sqrt
from math import ceil


if len(sys.argv) != 5:
        print "\nUsage: <Program_name> <Web_History_File> <Facebook_Friend_List> <Profile_List_File> <User_Name>\n"
        exit(0)
print "web history facebook miner"
print "Parameters"
print "web history file = " + str(sys.argv[1]) 
print "Facebook friend list = " + str(sys.argv[2]) 
print "Profile list File= " + str(sys.argv[3]) 
print "User Name= " + str(sys.argv[4]) 
#print "********START*******"
#Read the content script history file and get the texts and urls
history_read_handle          = open(sys.argv[1],'r')
history_read_content         = history_read_handle.readlines()
history_read_handle.close()

fb_read_handle               = open(sys.argv[2],'r')
fb_read_content              = fb_read_handle.readlines()
fb_read_handle.close()

fb_read_handle               = open(sys.argv[3],'r')
fb_profile_read_content      = fb_read_handle.readlines()
fb_read_handle.close()

name_map_handle              = open("/home/cnerg/Activpass/Server/www/activPass_core/data_base/name_mapping.txt",'r')
name_map_handle_content      = name_map_handle.readlines()
name_map_handle.close()


user_name = str(sys.argv[4])

#Putting facebook friend list into a dictionary
friends_id_dict = {}

fb_profile_dict = {}
fb_profile_id_dict = {}
fb_profile_name_dict = {}

for line in fb_profile_read_content:
        l_str_lst = line.strip().split('|')
	try:
	        fb_profile_dict[ l_str_lst[0] ] = l_str_lst[1]
        	fb_profile_name_dict[ l_str_lst[1].strip().replace(' ', '').lower() ] = l_str_lst[1].strip()
        	fb_profile_id_dict[ l_str_lst[1].strip().replace(' ', '').lower() ] = l_str_lst[0].strip()
	except IndexError:
		#print "
		FB_profile_Index_Error=1
#print fb_profile_id_dict
for line in name_map_handle_content:
	if (line.find(user_name) != -1):
		imp_line = line
		break
#print imp_line
user_name_1 = imp_line.strip().split('|')[1]
user_key = fb_profile_id_dict[user_name_1]

for line in fb_read_content:
        line_m = line.strip().split('|')
	try:
		if user_key == str(line_m[0].strip()) :
        		friends_id_dict[line_m[2]] = line_m[3]
	except IndexError:
		#print "
		Friend_list_Index_Error=1
#print friends_id_dict

history_write_handle          = open( "/home/cnerg/Activpass/Server/www/activPass_core/data_base/facebook_data/"+ user_name + "_history_for_fb_question_feeder.txt",'w')

web_history_dict = {}
user_time_dict = {}

#Process URLS
        #1 Get different specific URL pattern to get the semantics of visiting pattern of any user
line_count=1;

def activity_recog( act_str ):
	if act_str == "photos":
		return str("Photos")
	elif act_str == "photos_all":
		return str("All Photos")
	elif act_str == "photos_albums":
		return str("Photo Albums")
	elif act_str == "photos_of":
		return str("Tagged Photos")
	elif act_str == "friends":
		return str("Friend list")
	elif act_str == "friends_all":
		return str("Total Friend list")
	elif act_str == "friends_mutual":
		return str("Mutual Friend list")
	elif act_str == "friends_recent":
		return str("Recently Added Friend list")
	elif act_str == "friends_suggested":
		return str("Suggested Friend list (People you may know)")
	elif act_str == "followers":
		return str("Followers list")
	elif act_str == "following":
		return str("Following list")
	elif act_str == "events":
		return str("Events attended")
	elif act_str == "music":
		return str("Music likings")
	elif act_str == "music_radio":
		return str("Radiostations likings")
	elif act_str == "music_favs":
		return str("Favorite Music related pages")
	elif act_str == "music_playlists":
		return str("Favorite Music Playlists")
	elif act_str == "events":
		return str("Events attended")
	elif act_str == "places":
		return str("Places visited ( check ins )")
	elif act_str == "about":
		return str("About Information")
	elif act_str == "likes":
		return str("Liked pages")
	else:
		if ( re.search(r"friends", act_str) ):
			frnd_str_list = act_str.strip().split('_')
			frnd_comm = ""
			for j in range( len(frnd_str_list) - 1 ):
				frnd_comm = frnd_comm + frnd_str_list[j+1]
			return str("Friends' Group named " + frnd_comm)

		return str(act_str)

for line in history_read_content:
        if line_count != 1 :
		try:
	                line_fields = line.strip().split(':')
	                time_in_milliseconds_from_epoch = line_fields[2]
	                full_url = line_fields[1].split('//')[1]
	                important_part_of_url = full_url.split('/')
	
	        	len_m = len(important_part_of_url)
	        	line_str = "http:/" + important_part_of_url[0] 
	
	        	for i in range( len_m - 1):
	                	line_str = line_str + "/" + important_part_of_url[i+1]
	
			#print line_str
	                if ( re.search(r"facebook.com",important_part_of_url[0]) ) :
	
				if (not(re.search(r"\.php", line_str) or re.search(r"dialog", line_str) or re.search(r"find", line_str) or re.search(r"search", line_str) or re.search(r"message", line_str) or re.search(r"sk=nf", line_str) or re.search(r"addpage", line_str) or re.search(r'^\s*$', line_str) or re.search(r"media", line_str) or re.search(r"article", line_str) or re.search(r"fref", line_str) or re.search(r"hc_location",line_str) or re.search(r"notif_t",line_str) or re.search(r"comment_id",line_str) or re.search(r"posts",line_str) or re.search(r"groups",line_str) or re.search(r"pages",line_str) or re.search(r"app",line_str) ) ):
				#print line_str
					line_str_list = line_str.strip().split('/')
					#print line_str_list
					if(re.search(r"photos", line_str)):				
						#print "photos : user ", line_str_list[2], " activity : ",line_str_list[3]
						if line_str_list[2] not in web_history_dict:
							web_history_dict[ str(line_str_list[2]) ] = [ activity_recog(line_str_list[3]) ]
						else:
							web_history_dict[ str(line_str_list[2]) ].append(activity_recog(line_str_list[3]))	
					elif(re.search(r"events", line_str)):
						#print "events : user ", line_str_list[2], " activity : ",line_str_list[3]
						if line_str_list[2] not in web_history_dict:
							web_history_dict[ str(line_str_list[2]) ] = [ activity_recog(line_str_list[3]) ]
						else:
							web_history_dict[ str(line_str_list[2]) ].append(activity_recog(line_str_list[3]))
					elif(re.search(r"friend", line_str)):
						#print "friend : user ", line_str_list[2], " activity : ",line_str_list[3]
						if line_str_list[2] not in web_history_dict:
							web_history_dict[ str(line_str_list[2]) ] = [ activity_recog(line_str_list[3]) ]
						else:
							web_history_dict[ str(line_str_list[2]) ].append(activity_recog(line_str_list[3]))
					elif(re.search("about", line_str)):
						#print "about : ", line_str
						#print "about : user ", line_str_list[2], " activity : ",line_str_list[3]
						if line_str_list[2] not in web_history_dict:
							web_history_dict[ str(line_str_list[2]) ] = [ activity_recog(line_str_list[3]) ]
						else:
							web_history_dict[ str(line_str_list[2]) ].append(activity_recog(line_str_list[3]))
					elif(re.search("follow", line_str)):
						if line_str_list[2] not in web_history_dict:
							web_history_dict[ str(line_str_list[2]) ] = [ activity_recog(line_str_list[3]) ]
						else:
							web_history_dict[ str(line_str_list[2]) ].append(activity_recog(line_str_list[3]))
					elif(re.search("map", line_str)):
						if line_str_list[2] not in web_history_dict:
							web_history_dict[ str(line_str_list[2]) ] = [ activity_recog(line_str_list[3]) ]
						else:
							web_history_dict[ str(line_str_list[2]) ].append(activity_recog(line_str_list[3]))
					elif(re.search("likes", line_str)):
						if line_str_list[2] not in web_history_dict:
							web_history_dict[ str(line_str_list[2]) ] = [ activity_recog(line_str_list[3]) ]
						else:
							web_history_dict[ str(line_str_list[2]) ].append(activity_recog(line_str_list[3]))
					elif(re.search("music", line_str)):
						if line_str_list[2] not in web_history_dict:
							web_history_dict[ str(line_str_list[2]) ] = [ activity_recog(line_str_list[3]) ]
						else:
							web_history_dict[ str(line_str_list[2]) ].append(activity_recog(line_str_list[3]))
					elif(re.search("notes", line_str)):
						if line_str_list[3] == "notes":
							if line_str_list[2] not in web_history_dict:
								web_history_dict[ str(line_str_list[2]) ] = [ activity_recog(line_str_list[3]) ]
							else:
								web_history_dict[ str(line_str_list[2]) ].append(activity_recog(line_str_list[3]))
						elif line_str_list[2] == "notes":
							if line_str_list[3] not in web_history_dict:
								web_history_dict[ str(line_str_list[3]) ] = [ "Note titled " + line_str_list[4].replace('-',' ') ]	
							else:
								web_history_dict[ str(line_str_list[3]) ].append("Note titled " + line_str_list[4].replace('-',' '))	
					else:
						continue
						#print "BLANK", line_str
					user_time_dict[ str(line_str_list[2]) ] = time_in_milliseconds_from_epoch
					
				else:	
					#continue
					user_time_dict[ str(user_name) ] = time_in_milliseconds_from_epoch
					line_str_list = line_str.strip().split('/')
					if(re.search("pages", line_str)):
						if user_name not in web_history_dict:
							web_history_dict[ user_name ] = [ "Page named " + line_str_list[3] ]
						else:
							web_history_dict[ user_name ].append("Page named " + line_str_list[3])
					elif(re.search("apps", line_str)):
						if user_name not in web_history_dict:
							web_history_dict[ user_name ] = [ "Used Installed App namespaced " + line_str_list[2] ]
						else:
							web_history_dict[ user_name ].append("Use Installed App namespaced " + line_str_list[2])
					elif(re.search("appcenter", line_str)):
						if(not(re.search("category", line_str))):
							if user_name not in web_history_dict:
								web_history_dict[ user_name ] = [ "Searched App named " + line_str_list[3].split('?')[0] ]	
							else:
								web_history_dict[ user_name ].append("Searched App named " + line_str_list[2].split('?')[0])	
					else:
						continue
					#print "_____DO Nothing_____"
			else:	
				continue
			#	print "OTHER : ", line_str			
		except IndexError:
			#print "
			Index_Error= 1
	line_count = line_count+1

#print web_history_dict
#print user_time_dict

#Now form a Question Feeder file from it

for elem in web_history_dict:
	line_str = elem + "|"
	try: 
		l_w = web_history_dict[elem][0]
		for k in range(len(web_history_dict[elem]) - 1):
			l_w = l_w + ";" + web_history_dict[elem][k+1]
		
		if elem in user_time_dict:
			line_str = line_str + l_w + "|" + user_time_dict[elem] + "\n"
		else:
			line_str = line_str + l_w + "|1\n"
		
		history_write_handle.write(line_str)
	except IndexError:
		#print "
		Index_Error= 2
history_write_handle.close()
#print "*******END*******"
