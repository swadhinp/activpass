#!/usr/bin/python

import sys
import random
import os
import re
from random import choice
from math import sqrt
from math import ceil
from random import randint


if len(sys.argv) != 2:
	print "\nUsage: <Program_name> <User_Name>\n"
	exit(0)
#print "master"

num_qs = int(5)
user_name = str(sys.argv[1])

user_response_file_name = "/home/cnerg/Activpass/Server/www/activPass_core/response_base/" + user_name + "_question_answer_response.log" 
friend_file = "/home/cnerg/Activpass/Server/www/activPass_core/data_base/friend_closeness.txt"
#user_question_file_type = "/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + user_name + "_other_all_question_type.log" 
asked_question_log = "/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + user_name  + "_asked_questions.log"
ask_handle = open(asked_question_log, 'a')

qs_barred_type		    =open("barred_type_guess.txt",'r')
qs_bar_content		    =qs_barred_type.readlines()
qs_barred_type.close()

barred_dict = {}
for line in qs_bar_content:
	barred_dict[line]=1

other_asked_question_dict = {}
if os.path.exists(user_response_file_name):
	b = os.path.getsize(user_response_file_name)
        if b != 0:
		qs_response_handle          = open(user_response_file_name, 'r')
		qs_response_content         = qs_response_handle.readlines()
		qs_response_handle.close()
		for line in qs_response_content:
			x = line.strip().split("|")
			if len(x) < int(5) :
				continue
			question= line.strip().split("|")[4].split("::")[0]
			if question != "other":
				continue
			else:
				frnd_nm = line.strip().split("|")[4].split("::")[1]
				question_type = line.strip().split("|")[1] + "|" + line.strip().split("|")[2] + "|" + line.strip().split("|")[3]
				actual_question = line.strip().split("|")[4].split("::")[4]
				if frnd_nm not in other_asked_question_dict:
					temp_dict = {}
					temp_dict[question_type] = [actual_question]
					other_asked_question_dict[frnd_nm] = temp_dict
				else:
					if question_type not in other_asked_question_dict[frnd_nm]:
						other_asked_question_dict[frnd_nm][question_type] = [actual_question]
					else:
						other_asked_question_dict[frnd_nm][question_type].append(actual_question)



friend_read_handle 	    = open(friend_file, 'r')
friend_content		    = friend_read_handle.readlines()
friend_read_handle.close()

#qs_ans_read_handle          = open(user_question_file_type, 'r')
#qs_type_content             = qs_ans_read_handle.readlines()
#qs_ans_read_handle.close()

frnd_lst = []
for line in friend_content:
	frnd_lst = line.strip().split('|')
	current_user = frnd_lst.pop(0)
	if current_user == user_name:
		break

number_of_available_friend = 0
for item in frnd_lst:
	frnd_nm = item.split(":")[1]
	friend_question_file_name = "/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + frnd_nm + "_all_question_answer.log"
	if not os.path.exists(friend_question_file_name):		
		continue
	else:
		b = os.path.getsize(friend_question_file_name)
		if b == 0:
			continue
		else:
			number_of_available_friend = number_of_available_friend + 1


number_of_questions_per_friend = 0
if number_of_available_friend == 0:
	sys.exit()
else:
	number_of_questions_per_friend = ceil(int(num_qs)/int(number_of_available_friend))

print "number_of_available_friend = " + str(number_of_available_friend)

print "number_of_questions_per_friend = " + str(number_of_questions_per_friend)

if (number_of_questions_per_friend * number_of_available_friend) < num_qs:
	number_of_questions_per_friend = number_of_questions_per_friend + 1

already_asked = 0

for item in frnd_lst:
	frnd_nm = item.split(":")[1]
	closeness = item.split(":")[0]
	friend_question_file_name = "/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + frnd_nm + "_all_question_answer.log"
	if not os.path.exists(friend_question_file_name):		
		continue
	else:
		b = os.path.getsize(friend_question_file_name)
		if b == 0:
			continue
		else:
			friend_question_read_handle          = open(friend_question_file_name, 'r')	
			friend_question_read_content         = friend_question_read_handle.readlines()
			friend_question_read_handle.close()
			friend_question_dict = {}
			for line in friend_question_read_content:
				key = line.strip().split("|")[0] + "|" + line.strip().split("|")[1] + "|" + line.strip().split("|")[2]
				if key in barred_dict:
					continue
				question_and_answer_list = line.strip().split("|")[3:]
				list_index = 0
				for item in question_and_answer_list:
					if list_index == 0:
						question_and_answer = item
						list_index = list_index + 1
					else:
						question_and_answer = question_and_answer + "|" + item
				if key not in friend_question_dict:
					friend_question_dict[key] = [question_and_answer]
				else:
					friend_question_dict[key].append(question_and_answer)	
				
				if frnd_nm not in other_asked_question_dict:
					temp_dict = {}
					temp_dict[key] = []
					other_asked_question_dict[frnd_nm] = temp_dict
				else:
					if key not in other_asked_question_dict[frnd_nm]:
						other_asked_question_dict[frnd_nm][key] = []

			questions_asked_about_this_friend = {}
			if frnd_nm in other_asked_question_dict:
				questions_asked_about_this_friend = other_asked_question_dict[frnd_nm]

			sorted_question_list_of_this_friend = sorted(questions_asked_about_this_friend.items(), key=lambda x: len(x[1]))	
	
			already_asked_for_this_friend = 0
			while already_asked_for_this_friend < number_of_questions_per_friend and already_asked < num_qs:
				for itemn in sorted_question_list_of_this_friend:
					if already_asked_for_this_friend == number_of_questions_per_friend:
						break
					if already_asked == num_qs:
						break
					new_key = itemn[0]
					questions_already_asked_for_this_new_key = itemn[1]
					if new_key in friend_question_dict:
						available_question_answer_list_for_new_key = friend_question_dict[new_key]
						for item_in_avaialable_question_answer_list in available_question_answer_list_for_new_key:
							new_question_and_answer = item_in_avaialable_question_answer_list.split("|")
							only_new_question = new_question_and_answer.pop(0)
							if only_new_question not in questions_already_asked_for_this_new_key:
								formatted_question = new_key+"|"+"other::"+ frnd_nm + "::(this question is about your friend " + frnd_nm + "::" + closeness + "::" +only_new_question 
							for item1 in new_question_and_answer:
								formatted_question = formatted_question + "|" + item1
							formatted_question = formatted_question + "\n"
							ask_handle.write(formatted_question)	
							already_asked = already_asked + 1
							already_asked_for_this_friend = already_asked_for_this_friend + 1
							break

ask_handle.close()

