#!/usr/bin/python

import sys
import random
import os
import re
from random import choice
from math import sqrt
from math import ceil
from random import randint


if len(sys.argv) != 2:
	print "\nUsage: <Program_name> <User_Name>\n"
	exit(0)
#print "master"

num_qs = int(5)
user_name = str(sys.argv[1])

user_question_file_name = "/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + user_name + "_all_question_answer.log" 
user_question_file_type = "/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + user_name + "_all_question_type.log" 

if os.path.exists(user_question_file_name):
	b = os.path.getsize(user_question_file_name)
	if b == 0:
		sys.exit()


qs_ans_read_handle          = open(user_question_file_name, 'r')
qs_ans_read_content         = qs_ans_read_handle.readlines()
qs_ans_read_handle.close()

qs_ans_read_handle          = open(user_question_file_type, 'r')
qs_type_content             = qs_ans_read_handle.readlines()
qs_ans_read_handle.close()

user_question_asked_stat_dict = {}
user_question_answer_log_dict = {}
loop_count = 1
for line in qs_type_content:
	key = line.strip().split('|')[0:3]
	key_str = key[0]+"|"+key[1]+"|"+key[2]
	question_list = line.strip().split('|')[3:]
	new_question_list = []
	if key_str.find("MCQ") == -1 :
		len_question_list = len(question_list)
		if len_question_list % 2 == 0:
			len_new_question_list = len_question_list/2
			count = 0
			index = 0
			while count < len_new_question_list:
				tuple1 = [question_list[index], question_list[index+1]]
				new_question_list.append(tuple1)
				index += 2
				count += 1
	else:
		len_question_list = len(question_list)
		if len_question_list % 3 == 0:
			len_new_question_list = len_question_list/3
			count = 0
                        index = 0
                        while count < len_new_question_list:
                                tuple1 = [question_list[index], question_list[index+1], question_list[index+2]]
                                new_question_list.append(tuple1)
                                index += 3
                                count += 1
				
	#print key_str
	#print question_list
	user_question_asked_stat_dict[key_str] = new_question_list

sorted_question_asked_stat_list = sorted(user_question_asked_stat_dict.items(), key=lambda x: len(x[1]))

for line in qs_ans_read_content:
	try:
		key = line.strip().split('|')[0:3]
        	key_str = key[0]+"|"+key[1]+"|"+key[2]
       		question_list = line.strip().split('|')[3:]
		if key_str not in user_question_answer_log_dict:
			user_question_answer_log_dict[key_str] = [question_list]
		else:
			user_question_answer_log_dict[key_str].append(question_list)
	except IndexError:
		x=1;
user_response_file_name = user_name + "_asked_questions.log"
##print user_response_file_name
command_str = "rm -rf /home/cnerg/Activpass/Server/www/activPass_core/question_base/" + user_response_file_name
os.system(command_str)
qs_ans_output_handle          = open("/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + user_response_file_name,'w')


command_str = "rm -rf /home/cnerg/Activpass/Server/www/activPass_core/question_base/" + user_name +"_temp.txt"
temp_handle          = open("/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + user_name +"_temp.txt",'w')


sorted_len = len(sorted_question_asked_stat_list)
question_asked = 0;
already_asked = 0;
number_of_available_question = 0

while question_asked < num_qs:
	print "question_asked = " + str(question_asked) +" sorted len = " + str(sorted_len) +" already asked = "+ str(already_asked)
	if question_asked == sorted_len :
		break
	if question_asked < sorted_len:
		key_str=sorted_question_asked_stat_list[question_asked][0]
		print "key_str ="+str(key_str)
		questions_already_asked = sorted_question_asked_stat_list[question_asked][1]
		number_of_key = len(user_question_answer_log_dict)
		if key_str in user_question_answer_log_dict:
			questions_available = user_question_answer_log_dict[key_str]
			number_of_available_question = len(questions_available)
			print "number_of_available_question = " + str(number_of_available_question)
			if number_of_available_question == 0:
				question_asked = question_asked + 1
				num_qs = num_qs +1
				if question_asked == number_of_key:
					question_asked = 0
					num_qs = 5 - already_asked
				continue
		else:
			question_asked = question_asked + 1
			num_qs = num_qs +1
			if question_asked == number_of_key:
				question_asked = 0
				num_qs = 5 - already_asked
			continue
		truecount = 0
		question = ""
		while True:
			print "while number_of_available_question= " + str(number_of_available_question)
			rand_index = randint(0, number_of_available_question -1)
			new_question = questions_available[rand_index]
			print "new_question =" + str(new_question)
			print "questions already asked" 
			#print questions_already_asked
			if new_question not in questions_already_asked:
				print "HHHHHHHHHHHHHH"
				sorted_question_asked_stat_list[question_asked][1].append(new_question)
				questions_already_asked.append(new_question)
				qu_len = len(new_question)
				item_count = 0
				question = key_str
				while item_count < qu_len:
					question = question + "|"+ str(new_question[item_count])
					item_count = item_count +1
				question = question + "\n"
				qs_ans_output_handle.write(question)
				print "question=" + str(question)
				already_asked = already_asked + 1
				question_asked = question_asked +1
				break
			else:
				truecount = truecount + 1
				if truecount == number_of_available_question:
					question_asked = question_asked + 1
					num_qs = num_qs +1
					if question_asked == number_of_key:
						question_asked = 0
						num_qs = 5 - already_asked
					break
	else:
		break

count = 0
number_of_item = len(sorted_question_asked_stat_list)

while count < number_of_item:
	key1 = sorted_question_asked_stat_list[count][0]
	question_tuple = key1
	count1 = 0
	question_item = sorted_question_asked_stat_list[count][1] 
	print "question_item" + str(question_item)
	question_item_len = len(question_item)
	print "question_item_len =" + str(question_item_len)
	while count1 < question_item_len:
		new_question_tuple = ""
		new_question_tuple_len = len(question_item[count1])
		count2 =0 
		print "abcd" + str(question_item[count1])
		while count2 < new_question_tuple_len:
			print "cdef "+question_item[count1][count2]
			new_question_tuple = new_question_tuple + "|" + str(question_item[count1][count2])
			count2 = count2 +1
		question_tuple = question_tuple + new_question_tuple
		count1 = count1 + 1
	question_tuple = question_tuple + "\n"
	temp_handle.write(question_tuple)
	count = count + 1
temp_handle.close()
qs_ans_output_handle.close()

command_str = "rm -rf " + user_question_file_type
os.system(command_str)

command_str = "cat /home/cnerg/Activpass/Server/www/activPass_core/question_base/" + user_name +"_temp.txt >" + user_question_file_type 
os.system(command_str)
