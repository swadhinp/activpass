#!/usr/bin/python

import sys
import random
import os
import re
from random import choice
from math import sqrt
from math import ceil
from random import randint


if len(sys.argv) != 2:
        print "\nUsage: <Program_name> <User_Name>\n"
        exit(0)
#print "master"

num_qs = int(20)
user_name = str(sys.argv[1])
user_question_file_name = "/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + user_name + "_all_question_answer.log"
user_response_file_name = "/home/cnerg/Activpass/Server/www/activPass_core/response_base/" + user_name + "_question_answer_response.log"

user_asked_question_file_name = user_name + "_asked_questions.log"
command_str 		= "rm -rf /home/cnerg/Activpass/Server/www/activPass_core/question_base/" + user_asked_question_file_name
os.system(command_str)

qs_ques_output_handle   = open("/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + user_asked_question_file_name,'w')

if os.path.exists(user_question_file_name):
        b = os.path.getsize(user_question_file_name)
        if b == 0:
		#print "exit"
                sys.exit()

qs_ans_read_handle          = open(user_question_file_name, 'r')
qs_ans_read_content         = qs_ans_read_handle.readlines()
qs_ans_read_handle.close()

qs_barred_type		    =open("barred_type.txt",'r')
qs_bar_content		    =qs_barred_type.readlines()
qs_barred_type.close()

barred_dict = {}
for line in qs_bar_content:
	barred_dict[line]=1

user_response_dict = {}
user_question_answer_log_dict = {}

number_of_response = 0
if os.path.exists(user_response_file_name):
	qs_response_handle  = open(user_response_file_name, 'r')
	qs_response_content = qs_response_handle.readlines()
	qs_response_handle.close()
	loop_count = 1
	for line in qs_response_content:
		key = line.strip().split('|')[1:4]
		#print key
		if len(key) == 0:
			continue
		key_str = key[0]+"|"+key[1]+"|"+key[2]
		question_list = line.strip().split('|')[4:]
		#print "question_list before deletion = " + str(question_list)
		del question_list[-1]
		del question_list[-1]
		number_of_response = number_of_response + 1
		#print "question_list after deletion = " + str(question_list)
		if key_str not in user_response_dict:
			user_response_dict[key_str] = [question_list]
		else:
			user_response_dict[key_str].append(question_list)
	#print sorted_response_dict_list

#print user_response_dict
number_of_total_available_question = 0
for line in qs_ans_read_content:
	try:
                key = line.strip().split('|')[0:3]
                key_str = key[0]+"|"+key[1]+"|"+key[2]
                question_list = line.strip().split('|')[3:]
		number_of_total_available_question = number_of_total_available_question +1
		if key_str not in user_response_dict:
			user_response_dict[key_str] = []
                if key_str not in user_question_answer_log_dict:
                        user_question_answer_log_dict[key_str] = [question_list]
                else:
                        user_question_answer_log_dict[key_str].append(question_list)
        except IndexError:
                x=1;

#print user_question_answer_log_dict
sorted_response_dict_list = sorted(user_response_dict.items(), key=lambda x: len(x[1]))
sorted_response_len = len(sorted_response_dict_list)
#print "hi " + str(sorted_response_len)



command_str 		= "rm -rf /home/cnerg/Activpass/Server/www/activPass_core/question_base/" + user_name +"_temp.txt"
temp_handle          	= open("/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + user_name +"_temp.txt",'w')

print "asker"

already_asked = 0;
#question_left = number_of_total_available_question - number_of_response 			
#if num_qs > question_left:
#	num_qs = question_left

print "num question =" + str(num_qs)

index = 0
questions_accessed = 0
while already_asked < num_qs:
	if index == sorted_response_len:
		index =0
	key = sorted_response_dict_list[index][0]
	if key in barred_dict:
		continue
	questions_already_asked_with_this_key = user_response_dict[key]
	#if questions_accessed == number_of_total_available_question:
	#	break
#	print questions_already_asked_with_this_key
	if key in user_question_answer_log_dict:
		available_questions_list = user_question_answer_log_dict[key]
		number_of_avail_question_with_this_key = len(available_questions_list)
		questions_accessed = questions_accessed + number_of_avail_question_with_this_key
		count = 0
		while count < number_of_avail_question_with_this_key:
			question = available_questions_list [count]
			if question not in questions_already_asked_with_this_key:
				print question
				question_len = len(question)
				list_index = 0
				while list_index < question_len:
					if list_index != 0:
						question_string = question_string + "|" + question[list_index]
					else:
						question_string = question[list_index]
					list_index = list_index + 1		
				final_question = key + "|" + question_string + "\n"
				user_response_dict[key].append(question)
				already_asked = already_asked + 1
				qs_ques_output_handle.write(final_question)	
				break
			else:
				count = count +1
		index = index + 1
				
	else:
		#print available_questions_list
		index +=1
