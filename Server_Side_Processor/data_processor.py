#!/usr/bin/python

import sys
import json
import random
import os
import re
from random import choice
from math import sqrt
from math import ceil
from os import listdir
from os.path import isfile, join
from random import sample
import urllib2
import httplib
import codecs
import time
import types
from datetime import datetime

if len(sys.argv) != 2:
        print "\nUsage: <Program_name> <IN: user_name> \n"
        exit(0)

usernameall   = str(sys.argv[1])
username = usernameall.strip().split('|')[2]
facebookname = usernameall.strip().split('|')[1]
actualname = usernameall.strip().split('|')[0]
#today_date   = str(sys.argv[2])
today_date = str(time.strftime("%d/%m/%Y"))
#today_date = '24/01/2014'

###################################### MOBILE DATA PROCESSING START#######################################################

parser_path = "/home/cnerg/Activpass/Server/www/activPass_core/data_base/mobile_data/"
home_path = os.path.dirname(os.path.abspath(__file__))

#os.chdir( str(parser_path) )
dir_path = str(parser_path) + str(username) 
if os.path.exists(dir_path) :

	onlyfiles = [ f for f in listdir(dir_path) if isfile(join(dir_path,f)) ]

	for f_name in onlyfiles:
		file_type = f_name.strip().split('-')[0]
        	f_path = dir_path + "/" + f_name
	
		if re.search("call", str(file_type) ):
			#print f_path
			call_path = f_path
	#print f_path

	#print call_path
	deleted_frequent_file = dir_path + "/del_frequent-" + username + "_call.txt"
	deleted_periodic_file = dir_path + "/del_periodic-" + username + "_call.txt"
	deleted_frequent_periodic_file = dir_path + "/del_periodic-" + username + "_call.txt"
	outlier_file = dir_path + "/outlier-" + username + "_call.txt"

	#cur_path = os.path.dirname(os.path.abspath(__file__))
	#print cur_path
	#print "Parser"
	cmd = "rm -rf /home/cnerg/Activpass/Server/www/activPass_core/data_base/mobile_data/"+username+"/del_frequent*";
	os.system(cmd);
	cmd = "rm -rf /home/cnerg/Activpass/Server/www/activPass_core/data_base/mobile_data/"+username+"/del_periodic*";
	os.system(cmd);
	cmd = "rm -rf /home/cnerg/Activpass/Server/www/activPass_core/data_base/mobile_data/"+username+"/outlier*";
	os.system(cmd);
	command_str = "java -cp /home/cnerg/Activpass/Server/www/activPass_core/code_base/mobile/ Activity_Parser " + str(call_path) + " 5 5 " + str(deleted_frequent_file) + " " +  str(deleted_periodic_file) + " " + str(deleted_frequent_periodic_file) + " " + str(outlier_file)
	os.system( command_str )

###################################### MOBILE DATA PROCESSING END#######################################################

###################################### WEB/CONTENT DATA PROCESSING START#######################################################
command_str = "python /home/cnerg/Activpass/Server/www/activPass_core/code_base/web_content/make_web_history_files.py " + username +" " +today_date
os.system( command_str )

#os.chdir( str(home_path) )
content_script_parser_path = "/home/cnerg/Activpass/Server/www/activPass_core/data_base/web_content_data/raw/"
#os.chdir( str(content_script_parser_path) )
content_script_file = content_script_parser_path + username + "_content_script_data.txt"
command_str = "python /home/cnerg/Activpass/Server/www/activPass_core/code_base/web_content/content_script_miner.py " +  str(content_script_file) + " " + str(username)
os.system( command_str )
command_str = "python /home/cnerg/Activpass/Server/www/activPass_core/code_base/web_content/content_script_mixer.py " +  str("/home/cnerg/Activpass/Server/www/activPass_core/data_base/web_content_data/processed/content_script/") + " " + str("/home/cnerg/Activpass/Server/www/activPass_core/data_base/web_content_data/processed/all_content_script.txt")
os.system( command_str )
print "here1"
web_data_file = content_script_parser_path + username + "_web_history_data.txt"
command_str = "python /home/cnerg/Activpass/Server/www/activPass_core/code_base/web_content/web_history_miner.py " +  str(web_data_file)
os.system( command_str )
print "here2"
command_str = "python /home/cnerg/Activpass/Server/www/activPass_core/code_base/web_content/web_history_mixer.py /home/cnerg/Activpass/Server/www/activPass_core/data_base/web_content_data/processed/"
os.system( command_str )
print "Web data processing ends here"
###################################### WEB/CONTENT DATA PROCESSING END#######################################################

###################################### FACEBOOK DATA PROCESSING START#######################################################
print "Facebook Data processing starts here"

#command_str = "/usr/bin/python /home/cnerg/Activpass/Server/www/activPass_core/code_base/facebook/fb_data_from_heroku_db_to_csv.py"
#os.system( command_str )

facebook_profile_list_file_path = "/home/cnerg/Activpass/Server/www/activPass_core/data_base/facebook_data/profile.csv"

profile_read_handle = open( facebook_profile_list_file_path, "r" )
profile_read_content = profile_read_handle.readlines()
profile_read_handle.close()

flag = 0
print "facebook name= " + str(facebookname)
print "profile_read_content"
print profile_read_content
for line in profile_read_content:
        line_list = line.strip().split('|')
        uname = str(line_list[-1]).replace(' ', '').lower()
	print "uname = " + str(uname)
	#print uname

        if str(uname) == str(facebookname):
		#print "User has Facebook Data"
                flag = 1
                break
#print flag

#Only for those who Have FB Data
if flag == 1:
        ##print "here"
        web_history_file = "/home/cnerg/Activpass/Server/www/activPass_core/data_base/web_content_data/raw/" + str(username) +"_web_history_data.txt"
        command_str = "python /home/cnerg/Activpass/Server/www/activPass_core/code_base/facebook/profile_visit_from_web_history_miner.py " + str(web_history_file) + " " + str(username)
        os.system( command_str )

        profile_visit_file = "/home/cnerg/Activpass/Server/www/activPass_core/data_base/facebook_data/" + str(username) + "_profile_visit_from_web_history.txt"

        facebook_friend_list_file_path = "/home/cnerg/Activpass/Server/www/activPass_core/data_base/facebook_data/friend.csv"


        command_str = "python /home/cnerg/Activpass/Server/www/activPass_core/code_base/facebook/web_history_facebook_miner.py " + str(web_history_file) + " " + str(facebook_friend_list_file_path) + " " + str(facebook_profile_list_file_path) + " " + str(username)
        os.system( command_str )

print "Facebook Data processing ends here"

###################################### FACEBOOK DATA PROCESSING END#######################################################




#######################################   Question   Generation Start  ###############################################################

#command_str = "python run_to_generate_questions_from_fb_and_callsmsbrowse1.py " + username + " friends_list_users_set1.txt " + str(today_date) + "  Question_Generator_Facebook_Related Question_Generator_Mobile_Related" 
#os.system( command_str )
#######################################   Mobile Question   Generation Start  ###############################################################

dir_path = "/home/cnerg/Activpass/Server/www/activPass_core/data_base/mobile_data/"+ str(username)
if os.path.exists(dir_path):
	command_str = "python /home/cnerg/Activpass/Server/www/activPass_core/code_base/mobile/question_answer_gen_for_mobile_data.py " + "/home/cnerg/Activpass/Server/www/activPass_core/data_base/mobile_data/"+ str(username) + "/ /home/cnerg/Activpass/Server/www/activPass_core/code_base/mobile/config_file.txt " + str(today_date) + " " + str(username)
	os.system( command_str )
##print command_str
print "Mobile question generation ends here"

#######################################   Mobile Question   Generation End  ###############################################################

#######################################   Facebook Question   Generation Start  ###############################################################
web_history_visit_file = "/home/cnerg/Activpass/Server/www/activPass_core/data_base/facebook_data/"+ str(username) + "_history_for_fb_question_feeder.txt"

profile_visit_file = "/home/cnerg/Activpass/Server/www/activPass_core/data_base/facebook_data/" + str(username) + "_profile_visit_from_web_history.txt"

command_str = "python /home/cnerg/Activpass/Server/www/activPass_core/code_base/facebook/question_answer_gen_for_facebook.py " + str(profile_visit_file) + " /home/cnerg/Activpass/Server/www/activPass_core/data_base/facebook_data/ " + str(web_history_visit_file) + " " + "\""+str(facebookname)+"\""
#print command_str
os.system( command_str )
print "Facebook question generation ends here"

#######################################   Facebook Question   Generation End  ###############################################################

#######################################   Web_Content Question   Generation Start  #############################################################
command_str = "python /home/cnerg/Activpass/Server/www/activPass_core/code_base/web_content/question_answer_gen_for_web_content_data.py " + "/home/cnerg/Activpass/Server/www/activPass_core/data_base/web_content_data/processed/all_content_script.txt" + " /home/cnerg/Activpass/Server/www/activPass_core/data_base/web_content_data/processed/web_data_mixer-question_feeder.txt " + str(username)
os.system( command_str )

print "Web question generation ends here"
########################################################   Marging Question  ###############################################################
command_str = "rm -rf /home/cnerg/Activpass/Server/www/activPass_core/question_base/" + str(username) + "_all_question_answer.log"
os.system(command_str)
command_str = "touch /home/cnerg/Activpass/Server/www/activPass_core/question_base/" + str(username) + "_all_question_answer.log"
os.system(command_str)
if os.path.exists("/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + str(username) + "_question_answer_mobile.log"):
	command_str = "cat /home/cnerg/Activpass/Server/www/activPass_core/question_base/" + str(username) + "_question_answer_mobile.log >> /home/cnerg/Activpass/Server/www/activPass_core/question_base/" + str(username) + "_all_question_answer.log"
	os.system(command_str)
if os.path.exists("/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + str(facebookname) + "_question_answer_facebook.log"):
	command_str = "cat /home/cnerg/Activpass/Server/www/activPass_core/question_base/" + str(facebookname) + "_question_answer_facebook.log >> /home/cnerg/Activpass/Server/www/activPass_core/question_base/" + str(username) + "_all_question_answer.log"
	os.system(command_str)
if os.path.exists("/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + str(username) + "_question_answer_web_content.log"):
	command_str = "cat /home/cnerg/Activpass/Server/www/activPass_core/question_base/" + str(username) + "_question_answer_web_content.log >> /home/cnerg/Activpass/Server/www/activPass_core/question_base/" + str(username) + "_all_question_answer.log"
	os.system(command_str)

#command_str = "python /home/cnerg/Activpass/Server/www/activPass_core/code_base/question_type.py " + str(username) 
#os.system(command_str)
#######################################   Question   Generation End  ###############################################################

#Uncomment to incorporate generalization
command_str = "python ./../svm_generalization/run.py " + username
os.system(command_str)

#Uncomment to incorporate personalization
#command_str = "python ./../svm/run.py " + username
#os.system(command_str)

#command_str = "python /home/cnerg/Activpass/Server/www/activPass_core/code_base/new_question_asker.py " + str(username) 
#os.system(command_str)
command_str = "python /home/cnerg/Activpass/Server/www/activPass_core/code_base/other_question_asker.py " + str(username) 
os.system(command_str)

#os.system("chmod -R 777 svm_dir")


			

