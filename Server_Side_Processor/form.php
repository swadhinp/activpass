<html>

<head>
</head>
<body>

<h1>ActivPass : Your Activity is Your Password</h1>
<hr/>

<p><b>Summary : </b><br/></p>
<p>
A user maintains 26 accounts on an average. It is quite unlikely that any user can maintain separate passwords for all these accounts. This boils down to bad habit of keeping similar passwords which are also most likely derivable from common passwords, their online social information. These facts make security of users vulnerable. Biometric, which is state of the art in security, is not always feasible. This motivates us to find some easy solution of this classical problem. Daily activities of users do have a unique signature and lots of those activities can be electronically logged. In this proposed scheme information regarding different activities is used as challenge for accessing any system or application.</p>
<hr />
<b>Primary Contacts</b>
<ol>
<li>Sourav Kumar Dandapat, sourav.dandapat@gmail.com, 9477092046. </li>
<li>Swadhin Pradhan, swadhinjeet88@gmail.com, 9051423664. </li>
</ol>
<b>Supervisors</b>
<ol>
<li> Prof. Niloy Ganguly, IIT Kharagpur</li>
<li> Prof. Bivas Mitra, IIT Kharagpur</li>
<li> Prof. Romit Roy Choudhury, UIUC</li>
</ol>
<hr />
<a href="https://docs.google.com/document/d/1ySAPPWgtjl-HuGaYbpRhAPJbw1ADZXKmtBhT3U4TBS4/pub"><b> FAQ : Frequently Asked Questions </b></a>
<br/>
<hr/>
<b>Enter username provided by the team ActivPass (Be Careful about the Spelling)</b> <br/>
<br />

<form name="form1" method="post" action="writetofile.php">
Registered Name : <input name="name" type="text" id="name" size="50">
<br />
<br />
<input type="submit" name="Submit" value="Submit">
</form>

<a href="http://10.5.18.202/login.php"><b> Quiz </b></a>
</body>

</html>
