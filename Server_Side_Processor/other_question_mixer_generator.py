#!/usr/bin/python

import sys
import random
import os
import re
from random import choice
from math import sqrt
from math import ceil


if len(sys.argv) != 3:
        print "\nUsage: <Program_name> <Friends_List_File> <User_Name>\n"
        exit(0)

num_qs = int(20)
user_name = str(sys.argv[2])


frnd_file_read_handle          = open(sys.argv[1],'r')
frnd_file_read_content         = frnd_file_read_handle.readlines()
frnd_file_read_handle.close()

frnd_user_file_name_list = []

for line in frnd_file_read_content:
	line_str_lst = line.strip().split('|')
	if str(line_str_lst[0]) == user_name:
		for elem in line_str_lst:
			if elem == user_name:
				continue
			else:
				file_name = str(elem) + "_all_question_answer.log"
				frnd_user_file_name_list.append(file_name)

#print frnd_user_file_name_list

other_file_name 	= "/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + str(user_name) + "_other_all_question_answer.log"
qs_ans_output_handle    = open( other_file_name,'w')

f_count = 1

for f_name in frnd_user_file_name_list:

	front_str = ""

	if f_count <= 2:
		front_str = "CLOSE"
	elif (f_count > 2) and (f_count <= 4 ):
		front_str = "NOT_CLOSE"
	else:
		front_str = "RANDOM"

	friend_name = f_name.strip().split('_')[0]

	qs_ans_read_handle          = open("/home/cnerg/Activpass/Server/www/activPass_core/question_base/" + str(f_name),'r')
	qs_ans_read_content         = qs_ans_read_handle.readlines()
	qs_ans_read_handle.close()

	qs_source_type_dict = {}

	#Question and Answer Dictionary Populate
	line_count = 0
	qs_source_type_list_dict = {}

	for line in qs_ans_read_content:
		new_line = line.strip().split('|')
		qs_ans = new_line[3:]
		new_line = new_line[0] + "|" +new_line[1] + new_line[2] + "|" + front_str + "|" + friend_name
		for item in qs_ans:
			new_line = new_line +"|" + str(item)
		new_line = new_line + "\n"
		qs_ans_output_handle.write(new_line)

qs_ans_output_handle.close()
		
